﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Reflection;

namespace Database_testing
{
    public partial class MainForm : Form
    {
        Database db;
        List<string> armourTypeList = new List<string>();
        List<string> oneHandTypeList = new List<string>();
        List<string> twoHandTypeList = new List<string>();
        List<string> rangedTypeList = new List<string>();
        List<string> otherTypeList = new List<string>();
        List<string> armourSlotList = new List<string>();
        List<string> weaponSlotList = new List<string>();

        private const string FILE_LOCATION = @"..\..\..\Database\";

        public MainForm()
        {
            db = new Database();
            //SqlConnection mySQLConnection = new SqlConnection("Data Source=.\\SQLEXPRESS;AttachDbFilename=|DataDirectory|\\Items.mdf;Integrated Security=True;User Instance=True");
            //SqlConnection mySQLConnection = new SqlConnection(Database_testing.Properties.Settings.Default.ItemsConnectionString);
            //try
            //{
            //    mySQLConnection.Open();
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e.Message);
            //}

            //try
            //{
            //    SqlDataReader myReader = null;

            //    SqlCommand myCommandRead = new SqlCommand("select * from Items", mySQLConnection);
            //    myReader = myCommandRead.ExecuteReader();

            //    Console.WriteLine("Items Database:");
            //    while (myReader.Read())
            //    {
            //        Console.WriteLine(myReader["Name"].ToString());
            //    }

            //    myReader.Close();
            //    myCommandRead = new SqlCommand("select * from Weapons", mySQLConnection);
            //    myReader = myCommandRead.ExecuteReader();

            //    Console.WriteLine("Weapons Database: ");
            //    while (myReader.Read())
            //    {
            //        Console.WriteLine(myReader["Name"].ToString());
            //    }

            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.ToString());
            //}

            //mySQLConnection.Close();

            InitializeComponent();

            List<string> itemsList = new List<string>();
            itemsList.Add("Select");
            itemsList.Add("Weapons");
            itemsList.Add("Armour");
            ItemsComboBox.DataSource = itemsList;

            armourTypeList.Add("Select");
            armourTypeList.Add("Cloth");
            armourTypeList.Add("Leather");
            armourTypeList.Add("Mail");
            armourTypeList.Add("Plate");
            armourTypeList.Add("Amulet");
            armourTypeList.Add("Ring");
            armourTypeList.Add("Trinket");
            armourTypeList.Add("Shield");
            armourTypeList.Add("Cloak");
            armourTypeList.Add("OffHandFrill");
            armourTypeList.Add("Relic");
            armourTypeList.Add("Miscellaneous");
            TypeComboBox.DataSource = armourTypeList;

            weaponSlotList.Add("Select");
            weaponSlotList.Add("One-Handed");
            weaponSlotList.Add("Two-Handed");
            weaponSlotList.Add("Ranged");
            weaponSlotList.Add("Other");

            oneHandTypeList.Add("Select");
            oneHandTypeList.Add("Daggers");
            oneHandTypeList.Add("Fist Weapons");
            oneHandTypeList.Add("One-Handed Axes");
            oneHandTypeList.Add("One-Handed Maces");
            oneHandTypeList.Add("One-Handed Swords");

            twoHandTypeList.Add("Select");
            twoHandTypeList.Add("Polearms");
            twoHandTypeList.Add("Staves");
            twoHandTypeList.Add("Two-Handed Axes");
            twoHandTypeList.Add("Two-Handed Maces");
            twoHandTypeList.Add("Two-Handed Swords");
            twoHandTypeList.Add("Fishing Poles");

            rangedTypeList.Add("Select");
            rangedTypeList.Add("Bows");
            rangedTypeList.Add("Crossbows");
            rangedTypeList.Add("Guns");
            rangedTypeList.Add("Thrown");
            rangedTypeList.Add("Wands");

            otherTypeList.Add("Select");
            otherTypeList.Add("Other");

            armourSlotList.Add("Select");
            armourSlotList.Add("Chest");
            armourSlotList.Add("Feet");
            armourSlotList.Add("Hands");
            armourSlotList.Add("Head");
            armourSlotList.Add("Legs");
            armourSlotList.Add("Shoulder");
            armourSlotList.Add("Waist");
            armourSlotList.Add("Wrist");
            slotComboBox.DataSource = armourSlotList;

            List<string> qualityList = new List<string>();
            qualityList.Add("Select");
            qualityList.Add("Legendary (Orange)");
            qualityList.Add("Epic (Purple)");
            qualityList.Add("Rare (Blue)");
            qualityList.Add("Uncommon (Green)");
            qualityList.Add("Common (White)");
            qualityList.Add("Poor (Gray)");
            qualityComboBox.DataSource = qualityList;

            //many lists needed since running multiple combo boxes off one list will change them all when one changes
            List<string> statList = new List<string>();
            PopulateStatList(statList);

            List<string> statList2 = new List<string>();
            PopulateStatList(statList2);

            List<string> statList3 = new List<string>();
            PopulateStatList(statList3);

            List<string> statList4 = new List<string>();
            PopulateStatList(statList4);

            List<string> statList5 = new List<string>();
            PopulateStatList(statList5);

            Filter1StatComboBox.DataSource = statList;
            Filter2StatComboBox.DataSource = statList2;
            Filter3StatComboBox.DataSource = statList3;
            Filter4StatComboBox.DataSource = statList4;

            List<string> operatorList = new List<string>();
            PopulateOperatorList(operatorList);
            List<string> operatorList2 = new List<string>();
            PopulateOperatorList(operatorList2);
            List<string> operatorList3 = new List<string>();
            PopulateOperatorList(operatorList3);
            List<string> operatorList4 = new List<string>();
            PopulateOperatorList(operatorList4);
            List<string> operatorList5 = new List<string>();
            PopulateOperatorList(operatorList5);

            Filter1OperatorComboBox.DataSource = operatorList;
            Filter2OperatorComboBox.DataSource = operatorList2;
            Filter3OperatorComboBox.DataSource = operatorList3;
            Filter4OperatorComboBox.DataSource = operatorList4;

            #region building database

            //Stopwatch elapsedTime = new Stopwatch();
            ////TextWriter tw = new StreamWriter("itemlist.txt");
            //TextWriter tw = File.AppendText("itemlist.txt");
            ////TextWriter tw2 = new StreamWriter("itemnamelist.txt");
            //TextWriter tw2 = File.AppendText("itemnamelist.txt");


            ////TextReader tr = new StreamReader("itemlist.txt");

            //StreamReader reader = new StreamReader("itemlist2.txt");

            //string itemNumberString;
            //int itemNumber;


            //while (!reader.EndOfStream)
            //{
            //    itemNumberString = reader.ReadLine();
            //    itemNumber = int.Parse(itemNumberString);

            //    Item test = new Item();
            //    Console.WriteLine("Getting Item " + itemNumber);
            //    elapsedTime.Start();
            //    test.GetHTML(itemNumber);
            //    Console.WriteLine("Page retrieved in: " + elapsedTime.ElapsedMilliseconds + " milliseconds");
            //    if (test.SaveHTML())
            //    {
            //        Console.WriteLine("Item saved");
            //    }
            //    else
            //    {
            //        Console.WriteLine("Item saving failed");
            //    }

            //    Console.WriteLine("Parsing content...");
            //    elapsedTime.Restart();
            //    //try
            //    if (test.ParseHTML())
            //    {
            //        Console.WriteLine("Page parsed in: " + elapsedTime.ElapsedMilliseconds + " milliseconds");
            //        elapsedTime.Reset();

            //        //tw.WriteLine(i);
            //        //tw.Flush();

            //        //tw2.WriteLine(i + " " + test.name);
            //        //tw2.Flush();

            //        test.PrintItem();
            //        Console.WriteLine();
            //        test.InsertIntoDatabase();
            //    }
            //    else
            //    {
            //        Console.WriteLine("Item " + itemNumber + " not found");
            //        Console.WriteLine();
            //        elapsedTime.Reset();
            //    }
            //}


            //SqlConnection myConnection = new SqlConnection("");

            /*
            for (int i = 19351; i < 19352; i++)
            {
                Item test = new Item();
                Console.WriteLine("Getting Item " + i);
                elapsedTime.Start();
                test.GetHTML(i);
                Console.WriteLine("Page retrieved in: " + elapsedTime.ElapsedMilliseconds + " milliseconds");
                Console.WriteLine("Parsing content...");
                elapsedTime.Restart();
                //try
                if (test.ParseHTML())
                {
                    Console.WriteLine("Page parsed in: " + elapsedTime.ElapsedMilliseconds + " milliseconds");
                    elapsedTime.Reset();

                    //tw.WriteLine(i);
                    //tw.Flush();

                    //tw2.WriteLine(i + " " + test.name);
                    //tw2.Flush();

                    test.PrintItem();
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine("Item " + i + " not found");
                    Console.WriteLine();
                    elapsedTime.Reset();
                }
                //catch
                
            }
            */
            #endregion
            Console.WriteLine();
            Console.WriteLine();
            //mp5, bop, trinket
            #region mindtap testing
            Item mindtap = new Item();
            if (mindtap.SetHTML(18371))
            {
                mindtap.ParseHTML();
                string mindtapErrors = "";

                if (mindtap.name != "Mindtap Talisman")
                {
                    mindtapErrors += "Name fail\n";
                }

                if (mindtap.boe)
                {
                    mindtapErrors += "BOP fail\n";
                }

                if (mindtap.armourSlot != Item.ArmourSlot.Trinket)
                {
                    mindtapErrors += "Trinket Slot fail";
                }

                if (mindtap.requiredLevel != 56)
                {
                    mindtapErrors += "Required level fail";
                }

                if (mindtap.mp5 != 11)
                {
                    mindtapErrors += "Mp5 fail";
                }

                if (mindtapErrors != "")
                {
                    Console.WriteLine(mindtapErrors);
                }
                else
                {
                    Console.WriteLine("Mindtap passed");
                }

                //mindtap.InsertIntoDatabase();
            }
            #endregion //name, mp5
            //hp5, use, boe
            #region lifestone testing
            Item lifestone = new Item();
            if (lifestone.SetHTML(833))
            {
                lifestone.ParseHTML();
                string lifestoneErrors = "";

                if (lifestone.name != "Lifestone")
                {
                    lifestoneErrors += "Name fail\n";
                }

                if (!lifestone.boe)
                {
                    lifestoneErrors += "BOE fail\n";
                }

                if (lifestone.armourSlot != Item.ArmourSlot.Trinket)
                {
                    lifestoneErrors += "Trinket Slot fail";
                }

                if (lifestone.requiredLevel != 51)
                {
                    lifestoneErrors += "Required level fail";
                }

                if (lifestone.hp5 != 10)
                {
                    lifestoneErrors += "Mp5 fail";
                }

                if (lifestone.use != "Restores 300 to 701 health.")
                {
                }

                if (lifestoneErrors != "")
                {
                    Console.WriteLine(lifestoneErrors);
                }
                else
                {
                    Console.WriteLine("Lifestone passed");
                }

                //lifestone.InsertIntoDatabase();
            }
            #endregion
            //melee hit + crit, AP, stam, offhand, fist, dps, damage low + high, durability, speed
            #region claw of the frost wyrm testing
            Item clawOfTheFrostWyrm = new Item();
            if (clawOfTheFrostWyrm.SetHTML(23242))
            {
                clawOfTheFrostWyrm.ParseHTML();

                string clawOfTheFrostWyrmErrors = "";

                if (clawOfTheFrostWyrm.hit != 1)
                {
                    clawOfTheFrostWyrmErrors += "Melee hit fail\n";
                }
                if (clawOfTheFrostWyrm.crit != 1)
                {
                    clawOfTheFrostWyrmErrors += "Melee crit fail\n";
                }
                if (clawOfTheFrostWyrm.attackPower != 22)
                {
                    clawOfTheFrostWyrmErrors += "AP fail\n";
                }
                if (clawOfTheFrostWyrm.stamina != 8)
                {
                    clawOfTheFrostWyrmErrors += "Stamina fail\n";
                }
                if (clawOfTheFrostWyrm.weaponSlot != Item.WeaponSlot.OffHand)
                {
                    clawOfTheFrostWyrmErrors += "Offhand fail\n";
                }
                if (clawOfTheFrostWyrm.weaponType != Item.WeaponType.Fist)
                {
                    clawOfTheFrostWyrmErrors += "Fist weapon fail\n";
                }
                if (clawOfTheFrostWyrm.speed != 1.5)
                {
                    clawOfTheFrostWyrmErrors += "Speed fail\n";
                }
                if (clawOfTheFrostWyrm.dps < 71.6 || clawOfTheFrostWyrm.dps > 71.8)
                {
                    clawOfTheFrostWyrmErrors += "Dps fail\n";
                }
                if (clawOfTheFrostWyrm.damageLow != 75)
                {
                    clawOfTheFrostWyrmErrors += "Damage low fail\n";
                }
                if (clawOfTheFrostWyrm.damageHigh != 140)
                {
                    clawOfTheFrostWyrmErrors += "Damage high fail\n";
                }
                if (clawOfTheFrostWyrm.durability != 75)
                {
                    clawOfTheFrostWyrmErrors += "Durability fail\n";
                }

                if (clawOfTheFrostWyrmErrors != "")
                {
                    Console.WriteLine(clawOfTheFrostWyrmErrors);
                }
                else
                {
                    Console.WriteLine("Claw of the Frost Wyrm Passed");
                }
            }
            #endregion
            //sword, two-hand, yellow text
            #region ashkandi testing
            Item ashkandi = new Item();
            if (ashkandi.SetHTML(19364))
            {
                ashkandi.ParseHTML();

                string ashkandiErrors = "";

                if (ashkandi.weaponType != Item.WeaponType.Sword)
                {
                    ashkandiErrors += "Two handed sword fail\n";
                }
                if (ashkandi.weaponSlot != Item.WeaponSlot.TwoHand)
                {
                    ashkandiErrors += "Two handed slot fail\n";
                }
                if (ashkandi.yellowText != "\"The initials A.L. are etched on the hilt.\"")
                {
                    ashkandiErrors += "Yellow text fail\n";
                }

                if (ashkandiErrors != "")
                {
                    Console.WriteLine(ashkandiErrors);
                }
            }
            #endregion
            //one hand, chance on hit, nature res, fire res, plus damage
            #region thunderfury testing
            Item thunderfury = new Item();
            if (thunderfury.SetHTML(19019))
            {
                thunderfury.ParseHTML();
                string thunderfuryErrors = "";

                if (thunderfury.chanceOnHit != "Blasts your enemy with lightning, dealing 300 Nature damage and then jumping to additional nearby enemies.  Each jump reduces that victim's Nature resistance by 25. Affects 5 targets. Your primary target is also consumed by a cyclone, slowing its attack speed by 20% for 12 sec.")
                {
                    thunderfuryErrors += "Cance on hit fail\n";
                }
                if (thunderfury.natureRes != 9)
                {
                    thunderfuryErrors += "Nature resistance fail\n";
                }
                if (thunderfury.fireRes != 8)
                {
                    thunderfuryErrors += "Fire resistance fail\n";
                }
                if (thunderfury.extraDamage != "+16 - 30 Nature Damage")
                {
                    thunderfuryErrors += "Extra damage fail";
                }

                if (thunderfuryErrors != "")
                {
                    Console.WriteLine(thunderfuryErrors);
                }
                else
                {
                    Console.WriteLine("Thunderfury passed");
                }
            }
            #endregion
            //int, spirit, spell damage, spell crit, staff
            #region shadowflame
            Item shadowflame = new Item();
            if (shadowflame.SetHTML(19356))
            {
                shadowflame.ParseHTML();
                string shadowflameErrors = "";

                if (shadowflame.intellect != 29)
                {
                    shadowflameErrors += "Intellect fail\n";
                }
                if (shadowflame.spirit != 18)
                {
                    shadowflameErrors += "Spirit fail\n";
                }
                if (shadowflame.spellDamage != 84)
                {
                    shadowflameErrors += "Spell damage fail\n";
                }
                if (shadowflame.spellCrit != 2)
                {
                    shadowflameErrors += "Spell crit fail\n";
                }
                if (shadowflame.weaponType != Item.WeaponType.Staff)
                {
                    shadowflameErrors += "Staff fail";
                }

                if (shadowflameErrors != "")
                {
                    Console.WriteLine(shadowflameErrors);
                }
                else
                {
                    Console.WriteLine("Staff of the Shadowflame passed");
                }
            }
            #endregion
            //finger, healing
            #region pure elementium band
            Item elementiumBand = new Item();
            if (elementiumBand.SetHTML(19382))
            {
                elementiumBand.ParseHTML();
                string elementiumBandErrors = "";

                if (elementiumBand.spellHealing != 53)
                {
                    elementiumBandErrors += "Healing fail\n";
                }
                if (elementiumBand.armourSlot != Item.ArmourSlot.Finger)
                {
                    elementiumBandErrors += "Finger slot fail\n";
                }

                if (elementiumBandErrors != "")
                {
                    Console.WriteLine(elementiumBandErrors);
                }
                else
                {
                    Console.WriteLine("Pure Elementium Band passed");
                }
            }
            #endregion
            //wand, frost damage
            #region cold snap
            Item coldSnap = new Item();
            if (coldSnap.SetHTML(19130))
            {
                coldSnap.ParseHTML();
                string coldSnapError = "";

                if (coldSnap.frostDamage != 20)
                {
                    coldSnapError += "Frost damage fail\n";
                }
                if (coldSnap.weaponSlot != Item.WeaponSlot.Ranged)
                {
                    coldSnapError += "Ranged weapon slot fail\n";
                }
                if (coldSnap.weaponType != Item.WeaponType.Wand)
                {
                    coldSnapError += "Wand weapon error\n";
                }

                if (coldSnapError != "")
                {
                    Console.WriteLine(coldSnapError);
                }
                else
                {
                    Console.WriteLine("Coldsnap passed");
                }
            }
            #endregion
        }

        private static void PopulateOperatorList(List<string> operatorList)
        {
            operatorList.Add(">");
            operatorList.Add("<");
            operatorList.Add("=");
        }

        private static void PopulateStatList(List<string> statList)
        {
            statList.Add("Agility");
            statList.Add("Intellect");
            statList.Add("Stamina");
            statList.Add("Spirit");
            statList.Add("Strength");
            statList.Add("Mana per 5");
            statList.Add("Health per 5");
            statList.Add("Armour");
            statList.Add("Dodge");
            statList.Add("Parry");
            statList.Add("Defence");
            statList.Add("Block Chance");
            statList.Add("Block Value");
            statList.Add("Increased Axes");
            statList.Add("Increased Daggers");
            statList.Add("Increased Swords");
            statList.Add("Hit");
            statList.Add("Critical");
            statList.Add("Attack Power");
            statList.Add("Ranged Attack Power");
            statList.Add("Feral Attack Power");
            statList.Add("Spell Hit");
            statList.Add("Spell Critical");
            statList.Add("Spell Damage");
            statList.Add("Spell Healing");
            statList.Add("Spell Hit");
            statList.Add("Resistance Decrease");
            statList.Add("Arcane Damage");
            statList.Add("Fire Damage");
            statList.Add("Frost Damage");
            statList.Add("Nature Damage");
            statList.Add("Shadow Damage");
            statList.Add("Arcane Resistance");
            statList.Add("Fire Resistance");
            statList.Add("Frost Resistance");
            statList.Add("Nature Resistance");
            statList.Add("Shadow Resistance");
        }

        private void DebugSearchButton_Click(object sender, EventArgs e)
        {
            string searchName = ItemNameTextBox.Text;
            searchName = searchName.Replace("'", "''"); //sort apostrophes for SQL

            SQLiteConnection mySQLConnection = new SQLiteConnection(Database_testing.Properties.Settings.Default.ItemsConnectionString);
            try
            {
                mySQLConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            SQLiteDataReader myReader = null;

            int itemNumber;
            SQLiteCommand myCommandRead;
            if (!int.TryParse(searchName, out itemNumber))
            {
                myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE Name = '" + searchName + "'", mySQLConnection); //get all columns where item = name passed in
            }
            else
            {
                myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ItemID = " + itemNumber, mySQLConnection); //get all columns where item = name passed in
            }
            myReader = myCommandRead.ExecuteReader(); //set the reader
            bool isWeapon = false;

            if (myReader.HasRows == false) //if first search didn't return anything
            {
                myReader.Close();
                //weapon search
                if (!int.TryParse(searchName, out itemNumber))
                {
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, AttackPower, RangedAttackPower, FeralAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, WeaponType, Slot, DPS, DamageLow, DamageHigh, Speed, ExtraDamage, ChanceOnHit, WandDamage FROM Weapons WHERE Name = '" + searchName + "'", mySQLConnection); //get all columns where item = name passed in
                }
                else
                {
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, AttackPower, RangedAttackPower, FeralAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, WeaponType, Slot, DPS, DamageLow, DamageHigh, Speed, ExtraDamage, ChanceOnHit, WandDamage FROM Weapons WHERE ItemID = " + itemNumber, mySQLConnection); //get all columns where item = name passed in
                }
                myReader = myCommandRead.ExecuteReader(); //set the reader
                isWeapon = true;
            }

            Item newItem = new Item();

            newItem.BuildFromDatabase(myReader, isWeapon); //pass reader to the item so it can pull data from it

            mySQLConnection.Close();

            newItem.PrintItem();

        }

        private void AddItemIDButton_Click(object sender, EventArgs e)
        {
            Item newItem = new Item();

            //newItem.GetHTML(int.Parse(ItemNameTextBox.Text));
            if (newItem.SetHTML(int.Parse(ItemNameTextBox.Text)))
            {
                newItem.ParseHTML();
                newItem.InsertIntoDatabase();
                newItem.PrintItem();
            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            SQLiteConnection mySQLConnection = new SQLiteConnection(Database_testing.Properties.Settings.Default.ItemsConnectionString);
            try
            {
                mySQLConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            string name = ItemNameTextBox.Text;
            name = name.Replace("'", "''");

            int itemNumber;
            SQLiteCommand mycommand;
            if (!int.TryParse(name, out itemNumber))
            {
                mycommand = new SQLiteCommand("DELETE FROM Items WHERE Name = '" + name + "'", mySQLConnection);
            }
            else
            {
                mycommand = new SQLiteCommand("DELETE FROM Items WHERE ItemID = " + itemNumber, mySQLConnection);
            }

            mycommand.ExecuteNonQuery();

            mycommand = new SQLiteCommand("DELETE FROM Weapons WHERE Name = '" + name + "'", mySQLConnection);
            mycommand.ExecuteNonQuery();

            mySQLConnection.Close();

            Console.WriteLine("Deleted item with name: " + name);
        }

        private void PrintDBButton_Click(object sender, EventArgs e)
        {
            SQLiteConnection mySQLConnection = new SQLiteConnection(Database_testing.Properties.Settings.Default.ItemsConnectionString);
            try
            {
                mySQLConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                Console.WriteLine("Armour Database:");
                SQLiteDataReader myReader = null;
                SQLiteCommand myCommandRead = new SQLiteCommand("select * from Items", mySQLConnection);
                myReader = myCommandRead.ExecuteReader();
                while (myReader.Read())
                {
                    Console.WriteLine(myReader["Name"].ToString());
                }
                myReader.Close();
                myCommandRead = new SQLiteCommand("select * from Weapons", mySQLConnection);
                myReader = myCommandRead.ExecuteReader();
                Console.WriteLine("Weapons Database: ");
                while (myReader.Read())
                {
                    Console.WriteLine(myReader["Name"].ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            mySQLConnection.Close();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            string searchName = SearchTextBox.Text;

            #region comment out
            /*
            SqlConnection mySQLConnection = new SqlConnection(Database_testing.Properties.Settings.Default.ItemsConnectionString);
            try
            {
                mySQLConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            SqlDataReader myReader = null;

            SqlCommand myCommandRead = new SqlCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE Name LIKE '%" + searchName + "%'", mySQLConnection); //get all columns where item = name passed in
            myReader = myCommandRead.ExecuteReader(); //set the reader

            List<Item> items = new List<Item>(); //list of items found

            if (myReader.HasRows) //item search found something
            {
                while (myReader.Read()) //for each row
                {
                    //cerate an item
                    Item theItem = new Item
                        (
                        false,
                        myReader.GetInt32(0),//ItemID
                        myReader.GetString(1),//Name
                        myReader.GetInt32(2),//RequiredLevel
                        myReader.GetString(3),//OtherRequirements
                        myReader.GetString(4),//OtherRequirements2
                        myReader.GetBoolean(5),//BoE
                        myReader.GetBoolean(6),//BoP
                        myReader.GetBoolean(7),//QuestItem
                        myReader.GetBoolean(8),//IsUnique
                        myReader.GetBoolean(9),//BeginQuest
                        myReader.GetString(10),//Quality
                        myReader.GetInt32(11),//Armour
                        myReader.GetInt32(12),//Durability
                        myReader.GetBoolean(13),//RandomBonuses
                        myReader.GetInt32(14),//Agility
                        myReader.GetInt32(15),//Intellect
                        myReader.GetInt32(16),//Spirit
                        myReader.GetInt32(17),//Stamina
                        myReader.GetInt32(18),//Strength
                        myReader.GetInt32(19),//IncreacedDaggers
                        myReader.GetInt32(20),//IncreacedSwords
                        myReader.GetInt32(21),//IncreacedAxes
                        myReader.GetInt32(22),//ManaPer5
                        myReader.GetInt32(23),//HealthPer5
                        myReader.GetInt32(24),//Hit
                        myReader.GetInt32(25),//SpellHit
                        myReader.GetInt32(26),//Critical
                        myReader.GetInt32(27),//SpellCritical
                        myReader.GetInt32(28),//Dodge
                        myReader.GetInt32(29),//Parry
                        myReader.GetInt32(30),//Defence
                        myReader.GetInt32(31),//BaseBlock
                        myReader.GetInt32(32),//BlockValue
                        myReader.GetInt32(33),//BlockPercent
                        myReader.GetInt32(34),//AttackPower
                        myReader.GetInt32(35),//RangedAttackPower
                        myReader.GetInt32(36),//SpellDamage
                        myReader.GetInt32(37),//SpellHealing
                        myReader.GetInt32(38),//ResistanceDecreace
                        myReader.GetInt32(39),//ArcaneDamage
                        myReader.GetInt32(40),//ShadowDamage
                        myReader.GetInt32(41),//FireDamage
                        myReader.GetInt32(42),//NatureDamage
                        myReader.GetInt32(43),//FrostDamage
                        myReader.GetInt32(44),//NatureResistance
                        myReader.GetInt32(45),//FireResistance
                        myReader.GetInt32(46),//ShadowResistance
                        myReader.GetInt32(47),//FrostResistance
                        myReader.GetInt32(48),//ArcaneResistance
                        myReader.GetString(49),//MiscEquip
                        myReader.GetString(50),//OnUse
                        myReader.GetString(51),//YellowText
                        myReader.GetString(52),//ArmourType
                        myReader.GetString(53)//Slot]"
                        );
                    items.Add(theItem); //add to list
                }
            }

            myReader.Close(); //close reader ready for next search

            //weapon search
            myCommandRead = new SqlCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, AttackPower, RangedAttackPower, FeralAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, WeaponType, Slot, DPS, DamageLow, DamageHigh, Speed, ExtraDamage, ChanceOnHit, WandDamage FROM Weapons WHERE Name LIKE '%" + searchName + "%'", mySQLConnection); //get all columns where item = name passed in
            myReader = myCommandRead.ExecuteReader(); //set the reader

            if (myReader.HasRows)
            {
                while (myReader.Read()) //for each row
                {
                    //int itemID = myReader.GetInt32(0);//ItemID
                    //string name = myReader.GetString(1);//Name
                    //int requiredLevel = myReader.GetInt32(2);//RequiredLevel
                    //string otherRequirements = myReader.GetString(3);//OtherRequirements
                    //string otherRequirements2 = myReader.GetString(4);//OtherRequirements2
                    //bool boe = myReader.GetBoolean(5);//BoE
                    //bool bop = myReader.GetBoolean(6);//BoP
                    //bool questItem = myReader.GetBoolean(7);//QuestItem
                    //bool unique = myReader.GetBoolean(8);//IsUnique
                    //string quality = myReader.GetString(9);//Quality
                    //int armour = myReader.GetInt32(10);//Armour
                    //int durability = myReader.GetInt32(11);//Durability
                    //bool randomBonuses = myReader.GetBoolean(12);//RandomBonuses
                    //int agility = myReader.GetInt32(13);//Agility
                    //int intellect = myReader.GetInt32(14);//Intellect
                    //int spirit = myReader.GetInt32(15);//Spirit
                    //int stamina = myReader.GetInt32(16);//Stamina
                    //int strength = myReader.GetInt32(17);//Strength
                    //int increasedDaggers = myReader.GetInt32(18);//IncreacedDaggers
                    //int increasedSwords = myReader.GetInt32(19);//IncreacedSwords
                    //int increasedAxes = myReader.GetInt32(20);//IncreacedAxes
                    //int mp5 = myReader.GetInt32(21);//ManaPer5
                    //int hp5 = myReader.GetInt32(22);//HealthPer5
                    //int hit = myReader.GetInt32(23);//Hit
                    //int spellhit = myReader.GetInt32(24);//SpellHit
                    //int crit = myReader.GetInt32(25);//Critical
                    //int spellCrit = myReader.GetInt32(26);//SpellCritical
                    //int dodge = myReader.GetInt32(27);//Dodge
                    //int parry = myReader.GetInt32(28);//Parry
                    //int defence = myReader.GetInt32(29);//Defence
                    //int blockValue = myReader.GetInt32(30);//BlockValue
                    //int blockPercent = myReader.GetInt32(31);//BlockPercent
                    //int attackPower = myReader.GetInt32(32);//AttackPower
                    //int rangedAttackPower = myReader.GetInt32(33);//RangedAttackPower
                    //int feralAttackPower = myReader.GetInt32(34);//FeralAttackPower
                    //int spellDamage = myReader.GetInt32(35);//SpellDamage
                    //int spellHealing = myReader.GetInt32(36);//SpellHealing
                    //int resistanceDecrease = myReader.GetInt32(37);//ResistanceDecreace
                    //int arcaneDamage = myReader.GetInt32(38);//ArcaneDamage
                    //int shadowDamage = myReader.GetInt32(39);//ShadowDamage
                    //int fireDamage = myReader.GetInt32(40);//FireDamage
                    //int natureDamage = myReader.GetInt32(41);//NatureDamage
                    //int frostDamage = myReader.GetInt32(42);//FrostDamage
                    //int natureRes = myReader.GetInt32(43);//NatureResistance
                    //int fireRes = myReader.GetInt32(44);//FireResistance
                    //int shadowRes = myReader.GetInt32(45);//ShadowResistance
                    //string temp = myReader[46].ToString();
                    //int frostRes = myReader.GetInt32(46);//FrostResistance
                    //int arcaneRes = myReader.GetInt32(47);//ArcaneResistance
                    //string miscEquip = myReader.GetString(48);//MiscEquip
                    //string use = myReader.GetString(49);//OnUse
                    //string yellowText = myReader.GetString(50);//YellowText
                    //string weaponType = myReader.GetString(51);//WeaponType
                    //string slot = myReader.GetString(52);//Slot
                    //float DPS = (float)myReader.GetDouble(53);//DPS
                    //float damageLow = (float)myReader.GetDouble(54);//DamageLow
                    //float damageHigh = (float)myReader.GetDouble(55);//DamageHigh
                    //float speed = (float)myReader.GetDouble(56);//Speed
                    //string extraDamage = myReader.GetString(57);//ExtraDamage
                    //string chanceOnHit = myReader.GetString(58);//ChanceOnHit
                    //string wandDamage = myReader.GetString(59);//WandDamage
                    //create item
                    Item theItem = new Item
                        (
                        true,
                        myReader.GetInt32(0),//ItemID
                        myReader.GetString(1),//Name
                        myReader.GetInt32(2),//RequiredLevel
                        myReader.GetString(3),//OtherRequirements
                        myReader.GetString(4),//OtherRequirements2
                        myReader.GetBoolean(5),//BoE
                        myReader.GetBoolean(6),//BoP
                        myReader.GetBoolean(7),//QuestItem
                        myReader.GetBoolean(8),//IsUnique
                        myReader.GetString(9),//Quality
                        myReader.GetInt32(10),//Armour
                        myReader.GetInt32(11),//Durability
                        myReader.GetBoolean(12),//RandomBonuses
                        myReader.GetInt32(13),//Agility
                        myReader.GetInt32(14),//Intellect
                        myReader.GetInt32(15),//Spirit
                        myReader.GetInt32(16),//Stamina
                        myReader.GetInt32(17),//Strength
                        myReader.GetInt32(18),//IncreacedDaggers
                        myReader.GetInt32(19),//IncreacedSwords
                        myReader.GetInt32(20),//IncreacedAxes
                        myReader.GetInt32(21),//ManaPer5
                        myReader.GetInt32(22),//HealthPer5
                        myReader.GetInt32(23),//Hit
                        myReader.GetInt32(24),//SpellHit
                        myReader.GetInt32(25),//Critical
                        myReader.GetInt32(26),//SpellCritical
                        myReader.GetInt32(27),//Dodge
                        myReader.GetInt32(28),//Parry
                        myReader.GetInt32(29),//Defence
                        myReader.GetInt32(30),//AttackPower
                        myReader.GetInt32(31),//RangedAttackPower
                        myReader.GetInt32(32),//FeralAttackPower
                        myReader.GetInt32(33),//SpellDamage
                        myReader.GetInt32(34),//SpellHealing
                        myReader.GetInt32(35),//ResistanceDecreace
                        myReader.GetInt32(36),//ArcaneDamage
                        myReader.GetInt32(37),//ShadowDamage
                        myReader.GetInt32(38),//FireDamage
                        myReader.GetInt32(39),//NatureDamage
                        myReader.GetInt32(40),//FrostDamage
                        myReader.GetInt32(41),//NatureResistance
                        myReader.GetInt32(42),//FireResistance
                        myReader.GetInt32(43),//ShadowResistance
                        myReader.GetInt32(44),//FrostResistance
                        myReader.GetInt32(45),//ArcaneResistance
                        myReader.GetString(46),//MiscEquip
                        myReader.GetString(47),//OnUse
                        myReader.GetString(48),//YellowText
                        myReader.GetString(49),//WeaponType
                        myReader.GetString(50),//Slot
                        (float)myReader.GetDouble(51),//DPS
                        (float)myReader.GetDouble(52),//DamageLow
                        (float)myReader.GetDouble(53),//DamageHigh
                        (float)myReader.GetDouble(54),//Speed
                        myReader.GetString(55),//ExtraDamage
                        myReader.GetString(56),//ChanceOnHit
                        myReader.GetString(57)//WandDamage
                        );
                    items.Add(theItem);
                }
                myReader.Close();
            }
            */
            #endregion
            List<Item> items = db.SearchAllItems(searchName);

            if (items.Count == 0)
            {
                MessageBox.Show("Your search returned no results", "No Results Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Console.WriteLine("\nSearch Results:");
                foreach (Item iItem in items)
                {
                    Console.WriteLine(iItem.name);
                }
            }
            //mySQLConnection.Close();
        }

        private void DisplayButton_Click(object sender, EventArgs e)
        {
            string filter = "";

            if (Filter1CheckBox.Checked)
            {
                string column = Filter1StatComboBox.SelectedItem.ToString().Replace(" ", "");
                filter += " AND " + column + " " + Filter1OperatorComboBox.SelectedItem + " " + Filter1TextBox.Text;
            }
            if (Filter2CheckBox.Checked)
            {
                string column = Filter2StatComboBox.SelectedItem.ToString().Replace(" ", "");
                filter += " AND " + column + " " + Filter2OperatorComboBox.SelectedItem + " " + Filter2TextBox.Text;
            }
            if (Filter3CheckBox.Checked)
            {
                string column = Filter3StatComboBox.SelectedItem.ToString().Replace(" ", "");
                filter += " AND " + column + " " + Filter3OperatorComboBox.SelectedItem + " " + Filter3TextBox.Text;
            }
            if (Filter4CheckBox.Checked)
            {
                string column = Filter4StatComboBox.SelectedItem.ToString().Replace(" ", "");
                filter += " AND " + column + " " + Filter4OperatorComboBox.SelectedItem + " " + Filter4TextBox.Text;
            }

            if (qualityComboBox.SelectedItem.ToString() != "Select")
            {
                //check if there is any selection before this, if not append where instead of and
                //tho make select from items WHERE instead of select items FROM
                //if (filter != string.Empty)
                //{
                //    filter += " AND";
                //}
                //else
                //{
                //    filter += " WHERE";
                //}
                switch (qualityComboBox.SelectedItem.ToString())
                {
                    case "Legendary (Orange)":
                        filter += " AND Quality = 'Legendary'";
                        break;
                    case "Epic (Purple)":
                        filter += " AND Quality = 'Epic'";
                        break;
                    case "Rare (Blue)":
                        filter += " AND Quality = 'Rare'";
                        break;
                    case "Uncommon (Green)":
                        filter += " AND Quality = 'Uncommon'";
                        break;
                    case "Common (White)":
                        filter += " AND Quality = 'Common'";
                        break;
                    case "Poor (Gray)":
                        filter += " AND Quality = 'Poor'";
                        break;
                }
            }

            if (maxLevelTextBox.Text != string.Empty && minLevelTextBox.Text != string.Empty)
            {
                int minLevel, maxLevel;
                try
                {
                    minLevel = int.Parse(minLevelTextBox.Text);
                    maxLevel = int.Parse(maxLevelTextBox.Text);

                    filter += " AND RequiredLevel >= " + minLevel;
                    filter += " AND RequiredLevel <= " + maxLevel;
                }
                catch
                {
                    MessageBox.Show("Text in required level boxes must be numbers");
                }
            }

            List<Item> items = null;

            if ((string)ItemsComboBox.SelectedItem == "Weapons") //weapon list
            {
                switch ((string)TypeComboBox.SelectedItem)
                {
                    case "One-Handed":
                        switch ((string)slotComboBox.SelectedItem)
                        {
                            case "Daggers":
                                items = db.ListDaggers(filter);
                                break;
                            case "Fist Weapons":
                                items = db.ListFistWeapons(filter);
                                break;
                            case "One-Handed Axes":
                                items = db.ListOneHandedAxes(filter);
                                break;
                            case "One-Handed Maces":
                                items = db.ListOneHandedMaces(filter);
                                break;
                            case "One-Handed Swords":
                                items = db.ListOneHandedSwords(filter);
                                break;
                            default:
                                items = db.ListOneHandedWeapons(filter);
                                break;
                        }
                        break;
                    case "Two-Handed":
                        switch ((string)slotComboBox.SelectedItem)
                        {
                            case "Polearms":
                                items = db.ListPolearms(filter);
                                break;
                            case "Staves":
                                items = db.ListStaffs(filter);
                                break;
                            case "Two-Handed Axes":
                                items = db.ListTwoHandedAxes(filter);
                                break;
                            case "Two-Handed Maces":
                                items = db.ListTwoHandedMaces(filter);
                                break;
                            case "Two-Handed Swords":
                                items = db.ListTwoHandedSwords(filter);
                                break;
                            case "Fishing Poles":
                                items = db.ListFishingPoles(filter);
                                break;
                            default:
                                items = db.ListTwoHandedWeapons(filter);
                                break;
                        }
                        break;
                    case "Ranged":
                        switch ((string)slotComboBox.SelectedItem)
                        {
                            case "Bows":
                                items = db.ListBows(filter);
                                break;
                            case "Crossbows":
                                items = db.ListCrossbows(filter);
                                break;
                            case "Guns":
                                items = db.ListGuns(filter);
                                break;
                            case "Thrown":
                                items = db.ListThrown(filter);
                                break;
                            case "Wands":
                                items = db.ListWands(filter);
                                break;
                            default:
                                items = db.ListRangedWeapons(filter);
                                break;
                        }
                        break;
                    case "Other":
                        switch ((string)slotComboBox.SelectedItem)
                        {
                            default:
                                items = db.ListMiscellaneous(filter);
                                break;
                        }
                        break;
                    default:
                        items = db.ListAllWeapons(filter);
                        break;
                }
            }
            else if ((string)ItemsComboBox.SelectedItem == "Armour") //armour list
            {

                if ((string)TypeComboBox.SelectedItem == "Cloth")
                {
                    if ((string)slotComboBox.SelectedItem == "Chest")
                    {
                        items = db.ListClothChest(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Feet")
                    {
                        items = db.ListClothFeet(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Hands")
                    {
                        items = db.ListClothHands(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Head")
                    {
                        items = db.ListClothHead(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Legs")
                    {
                        items = db.ListClothLeg(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Shoulder")
                    {
                        items = db.ListClothShoulder(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Waist")
                    {
                        items = db.ListClothWaist(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Wrist")
                    {
                        items = db.ListClothWrist(filter);
                    }
                    else //all cloth armour
                    {
                        items = db.ListClothArmour(filter);
                    }
                }
                else if ((string)TypeComboBox.SelectedItem == "Leather")
                {
                    if ((string)slotComboBox.SelectedItem == "Chest")
                    {
                        items = db.ListLeatherChest(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Feet")
                    {
                        items = db.ListLeatherFeet(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Hands")
                    {
                        items = db.ListLeatherHands(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Head")
                    {
                        items = db.ListLeatherHead(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Legs")
                    {
                        items = db.ListLeatherLeg(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Shoulder")
                    {
                        items = db.ListLeatherShoulder(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Waist")
                    {
                        items = db.ListLeatherWaist(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Wrist")
                    {
                        items = db.ListLeatherWrist(filter);
                    }
                    else
                    {
                        items = db.ListLeatherArmour(filter);
                    }
                }
                else if ((string)TypeComboBox.SelectedItem == "Mail")
                {
                    if ((string)slotComboBox.SelectedItem == "Chest")
                    {
                        items = db.ListMailChest(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Feet")
                    {
                        items = db.ListMailFeet(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Hands")
                    {
                        items = db.ListMailHands(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Head")
                    {
                        items = db.ListMailHead(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Legs")
                    {
                        items = db.ListMailLeg(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Shoulder")
                    {
                        items = db.ListMailShoulder(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Waist")
                    {
                        items = db.ListMailWaist(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Wrist")
                    {
                        items = db.ListMailWrist(filter);
                    }
                    else
                    {
                        items = db.ListMailArmour(filter);
                    }
                }
                else if ((string)TypeComboBox.SelectedItem == "Plate")
                {
                    if ((string)slotComboBox.SelectedItem == "Chest")
                    {
                        items = db.ListPlateChest(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Feet")
                    {
                        items = db.ListPlateFeet(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Hands")
                    {
                        items = db.ListPlateHands(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Head")
                    {
                        items = db.ListPlateHead(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Legs")
                    {
                        items = db.ListPlateLeg(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Shoulder")
                    {
                        items = db.ListPlateShoulder(filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Waist")
                    {
                        items = db.ListPlateWaist(filter);
                        //items = theDatabase.ListArmour3(" WHERE ArmourType = 'Plate' AND Slot = 'Waist'", filter);
                    }
                    else if ((string)slotComboBox.SelectedItem == "Wrist")
                    {
                        items = db.ListPlateWrist(filter);
                    }
                    else
                    {
                        items = db.ListPlateArmour(filter);
                    }
                }
                else if ((string)TypeComboBox.SelectedItem == "Amulet")
                {
                    items = db.ListAmuletArmour(filter);
                }
                else if ((string)TypeComboBox.SelectedItem == "Ring")
                {
                    items = db.ListRingArmour(filter);
                }
                else if ((string)TypeComboBox.SelectedItem == "Trinket")
                {
                    items = db.ListTrinketArmour(filter);
                }
                else if ((string)TypeComboBox.SelectedItem == "Cloak")
                {
                    items = db.ListCloakArmour(filter);
                }
                else if ((string)TypeComboBox.SelectedItem == "OffHandFrill")
                {
                    items = db.ListOffHandFrillArmour(filter);
                }
                else if ((string)TypeComboBox.SelectedItem == "Shield")
                {
                    items = db.ListShieldArmour(filter);
                }
                else if ((string)TypeComboBox.SelectedItem == "Miscellaneous")
                {
                    items = db.ListMiscellaneousArmour(filter);
                }
                else //nothing selectec, just select all armour
                {
                    items = db.ListAllArmour(filter);
                }
            }
            else //neither checked, do nothing
            {
                MessageBox.Show("Please select some catagories to display", "No filter selected", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (items == null || items.Count == 0) //check null first to prevent null reference exception
            {
                MessageBox.Show("Your search returned no results", "No Results Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                Console.WriteLine("\nSearch Results:");
                foreach (Item iItem in items)
                {
                    Console.WriteLine();
                    Console.WriteLine();
                    iItem.PrintItem();
                    //Console.WriteLine(iItem.name);
                }
            }
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            //ChestRadioButton.Checked = false;
            //FeetRadioButton.Checked = false;
            //HandsRadioButton.Checked = false;
            //HeadRadioButton.Checked = false;
            //LegsRadioButton.Checked = false;
            //ShoulderRadioButton.Checked = false;
            //WaistRadioButton.Checked = false;
            //WristRadioButton.Checked = false;

            //ClothRadioButton.Checked = false;
            //LeatherRadioButton.Checked = false;
            //MailRadioButton.Checked = false;
            //PlateRadioButton.Checked = false;
            //AmuletRadioButton.Checked = false;
            //RingRadioButton.Checked = false;
            //TrinketRadioButton.Checked = false;
            //CloakRadioButton.Checked = false;
            //OffHandFrillRadioButton.Checked = false;
            //ShieldRadioButton.Checked = false;
            //MiscellaneousRadioButton.Checked = false;

            //WeaponsRadioButton.Checked = false;
            //ArmourRadioButton.Checked = false;
            Console.Clear();
        }

        private void FillDBButton_Click(object sender, EventArgs e)
        {
            Stopwatch elapsedTime = new Stopwatch();
            //TextWriter tw = new StreamWriter("itemlist.txt");
            //TextWriter tw = File.AppendText("itemlist.txt");
            ////TextWriter tw2 = new StreamWriter("itemnamelist.txt");
            //TextWriter tw2 = File.AppendText("itemnamelist.txt");

            TextWriter errorLog = File.AppendText(FILE_LOCATION + "errorLog.txt");


            //TextReader tr = new StreamReader("itemlist.txt");

            StreamReader reader = new StreamReader(FILE_LOCATION + "itemlist.txt");

            string itemNumberString;
            int itemNumber;


            while (!reader.EndOfStream)
            {
                itemNumberString = reader.ReadLine();
                itemNumber = int.Parse(itemNumberString);

                Item test = new Item();
                //Console.WriteLine("Getting Item " + itemNumber);

                //elapsedTime.Start();
                //test.GetHTML(itemNumber);
                //Console.WriteLine("Page retrieved in: " + elapsedTime.ElapsedMilliseconds + " milliseconds");
                //if (test.SaveHTML())
                //{
                //    Console.WriteLine("Item saved");
                //}
                //else
                //{
                //    Console.WriteLine("Item saving failed");
                //}

                //Console.WriteLine("Parsing content...");
                elapsedTime.Restart();
                try
                {
                    if (test.SetHTML(itemNumber))
                    {
                        if (test.ParseHTML())
                        {
                            //Console.WriteLine("Page parsed in: " + elapsedTime.ElapsedMilliseconds + " milliseconds");
                            elapsedTime.Reset();

                            //tw.WriteLine(i);
                            //tw.Flush();

                            //tw2.WriteLine(i + " " + test.name);
                            //tw2.Flush();

                            //test.PrintItem();
                            //Console.WriteLine();
                            test.InsertIntoDatabase();
                        }
                        else
                        {
                            Console.WriteLine("Item " + itemNumber + " not found");
                            Console.WriteLine();
                            elapsedTime.Reset();
                        }
                    }
                }
                catch
                {
                    Console.WriteLine("Error with item number " + itemNumber);
                    errorLog.WriteLine("Error with item number " + itemNumber);
                    errorLog.Flush();
                }
            }

            Console.WriteLine("Database imported");

            //for (int i = 15168; i < 24000; i++)
            //{
            //    Item test = new Item();
            //    Console.WriteLine("Getting Item " + i);
            //    elapsedTime.Start();
            //    test.GetHTML(i);
            //    Console.WriteLine("Page retrieved in: " + elapsedTime.ElapsedMilliseconds + " milliseconds");

            //    try
            //    {
            //        if (test.ParseHTML())
            //        {
            //            Console.WriteLine("Page parsed in: " + elapsedTime.ElapsedMilliseconds + " milliseconds");
            //            elapsedTime.Reset();

            //            tw.WriteLine(i);
            //            tw.Flush();

            //            tw2.WriteLine(i + " " + test.name);
            //            tw2.Flush();

            //            test.PrintItem();
            //            Console.WriteLine();
            //            test.InsertIntoDatabase();

            //            if (test.SaveHTML())
            //            {
            //                Console.WriteLine("Item saved");
            //            }
            //            else
            //            {
            //                Console.WriteLine("Item saving failed");
            //            }

            //            Console.WriteLine("Parsing content...");
            //            elapsedTime.Restart();
            //        }
            //        else
            //        {
            //            Console.WriteLine("Item " + i + " not found");
            //            Console.WriteLine();
            //            elapsedTime.Reset();
            //        }
            //    }
            //    catch
            //    {
            //        Console.WriteLine("Error with item number " + i);
            //        errorLog.WriteLine("Error with item number " + i);
            //        errorLog.Flush();
            //    }

            //}

        }

        private void DeleteAllButton_Click(object sender, EventArgs e)
        {
            SQLiteConnection mySQLConnection = new SQLiteConnection(Database_testing.Properties.Settings.Default.ItemsConnectionString);
            try
            {
                mySQLConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            string name = ItemNameTextBox.Text;
            name = name.Replace("'", "''");
            SQLiteCommand mycommand = new SQLiteCommand("DELETE FROM Items", mySQLConnection);

            mycommand.ExecuteNonQuery();

            mycommand = new SQLiteCommand("DELETE FROM Weapons", mySQLConnection);
            mycommand.ExecuteNonQuery();

            mySQLConnection.Close();
        }

        private void DebugDisplayButton_Click(object sender, EventArgs e)
        {

        }

        private void TypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ItemsComboBox.SelectedItem.ToString() == "Armour")
            {
                switch ((string)TypeComboBox.SelectedItem)
                {
                    case "Amulet":
                    case "Ring":
                    case "Cloak":
                    case "OffHandFrill":
                    case "Shield":
                    case "Miscellaneous":
                    case "Relic":
                        slotComboBox.Enabled = false;
                        break;
                    default:
                        slotComboBox.Enabled = true;
                        break;
                }
            }
            else if (ItemsComboBox.SelectedItem.ToString() == "Weapons")
            {
                switch ((string)TypeComboBox.SelectedItem)
                {
                    case "One-Handed":
                        slotComboBox.DataSource = oneHandTypeList;
                        slotComboBox.Enabled = true;
                        break;
                    case "Two-Handed":
                        slotComboBox.DataSource = twoHandTypeList;
                        slotComboBox.Enabled = true;
                        break;
                    case "Ranged":
                        slotComboBox.DataSource = rangedTypeList;
                        slotComboBox.Enabled = true;
                        break;
                    case "Other":
                        slotComboBox.DataSource = otherTypeList;
                        slotComboBox.Enabled = false;
                        break;
                }
            }
        }

        private void Filter1CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (Filter1CheckBox.Checked)
            {
                Filter1StatComboBox.Enabled = true;
                Filter1OperatorComboBox.Enabled = true;
                Filter1TextBox.Enabled = true;
            }
            else
            {
                Filter1StatComboBox.Enabled = false;
                Filter1OperatorComboBox.Enabled = false;
                Filter1TextBox.Enabled = false;
            }
        }

        private void Filter2CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (Filter2CheckBox.Checked)
            {
                Filter2StatComboBox.Enabled = true;
                Filter2OperatorComboBox.Enabled = true;
                Filter2TextBox.Enabled = true;
            }
            else
            {
                Filter2StatComboBox.Enabled = false;
                Filter2OperatorComboBox.Enabled = false;
                Filter2TextBox.Enabled = false;
            }
        }

        private void Filter3CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (Filter3CheckBox.Checked)
            {
                Filter3StatComboBox.Enabled = true;
                Filter3OperatorComboBox.Enabled = true;
                Filter3TextBox.Enabled = true;
            }
            else
            {
                Filter3StatComboBox.Enabled = false;
                Filter3OperatorComboBox.Enabled = false;
                Filter3TextBox.Enabled = false;
            }
        }

        private void Filter4CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (Filter4CheckBox.Checked)
            {
                Filter4StatComboBox.Enabled = true;
                Filter4OperatorComboBox.Enabled = true;
                Filter4TextBox.Enabled = true;
            }
            else
            {
                Filter4StatComboBox.Enabled = false;
                Filter4OperatorComboBox.Enabled = false;
                Filter4TextBox.Enabled = false;
            }
        }

        private void maxButton_Click(object sender, EventArgs e)
        {
            string filter = "";

            if (Filter1CheckBox.Checked)
            {
                string column = Filter1StatComboBox.SelectedItem.ToString().Replace(" ", "");
                //filter += " AND " + column + " " + Filter1OperatorComboBox.SelectedItem + " " + Filter1TextBox.Text;
                filter += " ORDER BY " + column + " DESC";

                List<List<Item>> items = new List<List<Item>>();

                List<Item> head = db.MaxStat(1, " WHERE ArmourType = 'Plate' AND Slot = 'Head'", filter);
                List<Item> neck = db.MaxStat(1, "  WHERE Slot = 'Neck'", filter);
                List<Item> shoulder = db.MaxStat(1, " WHERE ArmourType = 'Plate' AND Slot = 'Shoulder'", filter);
                List<Item> back = db.MaxStat(1, " WHERE Slot = 'Back'", filter);
                List<Item> chest = db.MaxStat(1, " WHERE ArmourType = 'Plate' AND Slot = 'Chest'", filter);
                List<Item> wrists = db.MaxStat(1, " WHERE ArmourType = 'Plate' AND Slot = 'Wrist'", filter);
                List<Item> hands = db.MaxStat(1, " WHERE ArmourType = 'Plate' AND Slot = 'Hands'", filter);
                List<Item> waist = db.MaxStat(1, " WHERE ArmourType = 'Plate' AND Slot = 'Waist'", filter);
                List<Item> legs = db.MaxStat(1, " WHERE ArmourType = 'Plate' AND Slot = 'Legs'", filter);
                List<Item> feet = db.MaxStat(1, " WHERE ArmourType = 'Plate' AND Slot = 'Feet'", filter);
                List<Item> finger = db.MaxStat(2, " WHERE Slot = 'Finger'", filter);
                List<Item> trinket = db.MaxStat(2, " WHERE Slot = 'Trinket'", filter);

                items.Add(head);
                items.Add(neck);
                items.Add(shoulder);
                items.Add(back);
                items.Add(chest);
                items.Add(wrists);
                items.Add(waist);
                items.Add(legs);
                items.Add(feet);
                items.Add(finger);
                items.Add(trinket);

                int total = 0;

                column = Char.ToLower(column[0]) + column.Substring(1); //tostring just the first char, since we're using camal case

                foreach (List<Item> listOfItems in items)
                {
                    foreach (Item iItem in listOfItems)
                    {
                        PropertyInfo prop = iItem.GetType().GetProperty(column);
                        total += (int)prop.GetValue(iItem, null);

                        Console.WriteLine(iItem.name);
                    }
                }
                Console.WriteLine("Total " + Filter1StatComboBox.SelectedItem.ToString().ToLower() + " is " + total);
            }
        }

        private void ItemsComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ItemsComboBox.SelectedItem.ToString() == "Armour")
            {
                TypeComboBox.DataSource = armourTypeList;
                slotComboBox.DataSource = armourSlotList;
            }
            else if (ItemsComboBox.SelectedItem.ToString() == "Weapons")
            {
                TypeComboBox.DataSource = weaponSlotList;
                slotComboBox.DataSource = oneHandTypeList;
            }
        }

        private void DebugButton_Click(object sender, EventArgs e)
        {
            List<Item> items = null;

            items = db.Debug();

            //Console.WriteLine("\nSearch Results:");
            //foreach (Item iItem in items)
            //{
            //    Console.WriteLine();
            //    Console.WriteLine();
            //    iItem.PrintItem();
            //    //Console.WriteLine(iItem.name);
            //}
        }
    }
}
