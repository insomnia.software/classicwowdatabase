﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Data.SQLite;

namespace Database_testing
{
    class Database
    {
        public List<Item> ListWeapons()
        {
            List<Item> items = new List<Item>();

            SQLiteConnection mySQLConnection = new SQLiteConnection(Database_testing.Properties.Settings.Default.ItemsConnectionString);
            try
            {
                mySQLConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            SQLiteCommand mycommand = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, AttackPower, RangedAttackPower, FeralAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, WeaponType, Slot, DPS, DamageLow, DamageHigh, Speed, ExtraDamage, ChanceOnHit, WandDamage FROM Weapons", mySQLConnection);

            SQLiteDataReader myReader = mycommand.ExecuteReader();

            if (myReader.HasRows)
            {
                while (myReader.Read()) //for each row
                {
                    //create item
                    Item theItem = new Item
                        (
                        true,
                        myReader.GetInt32(0),//ItemID
                        myReader.GetString(1),//Name
                        myReader.GetInt32(2),//RequiredLevel
                        myReader.GetString(3),//OtherRequirements
                        myReader.GetString(4),//OtherRequirements2
                        myReader.GetBoolean(5),//BoE
                        myReader.GetBoolean(6),//BoP
                        myReader.GetBoolean(7),//QuestItem
                        myReader.GetBoolean(8),//IsUnique
                        myReader.GetString(9),//Quality
                        myReader.GetInt32(10),//Armour
                        myReader.GetInt32(11),//Durability
                        myReader.GetBoolean(12),//RandomBonuses
                        myReader.GetInt32(13),//Agility
                        myReader.GetInt32(14),//Intellect
                        myReader.GetInt32(15),//Spirit
                        myReader.GetInt32(16),//Stamina
                        myReader.GetInt32(17),//Strength
                        myReader.GetInt32(18),//IncreacedDaggers
                        myReader.GetInt32(19),//IncreacedSwords
                        myReader.GetInt32(20),//IncreacedAxes
                        myReader.GetInt32(21),//ManaPer5
                        myReader.GetInt32(22),//HealthPer5
                        myReader.GetInt32(23),//Hit
                        myReader.GetInt32(24),//SpellHit
                        myReader.GetInt32(25),//Critical
                        myReader.GetInt32(26),//SpellCritical
                        myReader.GetInt32(27),//Dodge
                        myReader.GetInt32(28),//Parry
                        myReader.GetInt32(29),//Defence
                        myReader.GetInt32(30),//AttackPower
                        myReader.GetInt32(31),//RangedAttackPower
                        myReader.GetInt32(32),//FeralAttackPower
                        myReader.GetInt32(33),//SpellDamage
                        myReader.GetInt32(34),//SpellHealing
                        myReader.GetInt32(35),//ResistanceDecreace
                        myReader.GetInt32(36),//ArcaneDamage
                        myReader.GetInt32(37),//ShadowDamage
                        myReader.GetInt32(38),//FireDamage
                        myReader.GetInt32(39),//NatureDamage
                        myReader.GetInt32(40),//FrostDamage
                        myReader.GetInt32(41),//NatureResistance
                        myReader.GetInt32(42),//FireResistance
                        myReader.GetInt32(43),//ShadowResistance
                        myReader.GetInt32(44),//FrostResistance
                        myReader.GetInt32(45),//ArcaneResistance
                        myReader.GetString(46),//MiscEquip
                        myReader.GetString(47),//OnUse
                        myReader.GetString(48),//YellowText
                        myReader.GetString(49),//WeaponType
                        myReader.GetString(50),//Slot
                        (float)myReader.GetDouble(51),//DPS
                        (float)myReader.GetDouble(52),//DamageLow
                        (float)myReader.GetDouble(53),//DamageHigh
                        (float)myReader.GetDouble(54),//Speed
                        myReader.GetString(55),//ExtraDamage
                        myReader.GetString(56),//ChanceOnHit
                        myReader.GetString(57)//WandDamage
                        );
                    items.Add(theItem);
                }
                myReader.Close();
            }

            mySQLConnection.Close();

            return items;
        }

        //remember to include a leading space before the WHERe or the command won't work
        public List<Item> ListAllArmour(string filter)
        {
            //no where clause selecting types so we need to say FROM items WHERE ... not FROM items AND ...
            //string newFilter = filter.Replace("AND", "WHERE");
            return ListArmour3("", filter);
        }

        public List<Item> ListClothArmour(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Cloth' AND Slot != 'Back'", filter);
        }

        public List<Item> ListLeatherArmour(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Leather'", filter);
        }

        public List<Item> ListMailArmour(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Mail'", filter);
        }

        public List<Item> ListPlateArmour(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Plate'", filter);
        }

        public List<Item> ListAmuletArmour(string filter)
        {
            return ListArmour3(" WHERE Slot = 'Neck'", filter);
        }

        public List<Item> ListRingArmour(string filter)
        {
            return ListArmour3(" WHERE Slot = 'Finger'", filter);
        }

        public List<Item> ListTrinketArmour(string filter)
        {
            return ListArmour3(" WHERE Slot = 'Trinket'", filter);
        }

        public List<Item> ListCloakArmour(string filter)
        {
            return ListArmour3(" WHERE Slot = 'Back'", filter);
        }

        public List<Item> ListOffHandFrillArmour(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'OffHandFrill'", filter);
        }

        public List<Item> ListShieldArmour(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Shield'", filter);
        }

        public List<Item> ListMiscellaneousArmour(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Miscellaneous'", filter);
        }

        #region cloth
        public List<Item> ListClothChest(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Cloth' AND Slot = 'Chest'", filter);
        }

        public List<Item> ListClothFeet(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Cloth' AND Slot = 'Feet'", filter);
        }

        public List<Item> ListClothHands(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Cloth' AND Slot = 'Hands'", filter);
        }

        public List<Item> ListClothHead(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Cloth' AND Slot = 'Head'", filter);
        }

        public List<Item> ListClothLeg(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Cloth' AND Slot = 'Legs'", filter);
        }

        public List<Item> ListClothShoulder(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Cloth' AND Slot = 'Shoulder'", filter);
        }

        public List<Item> ListClothWaist(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Cloth' AND Slot = 'Waist'", filter);
        }

        public List<Item> ListClothWrist(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Cloth' AND Slot = 'Wrist'", filter);
        }
        #endregion

        #region leather
        public List<Item> ListLeatherChest(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Leather' AND Slot = 'Chest'", filter);
        }

        public List<Item> ListLeatherFeet(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Leather' AND Slot = 'Feet'", filter);
        }

        public List<Item> ListLeatherHands(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Leather' AND Slot = 'Hands'", filter);
        }

        public List<Item> ListLeatherHead(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Leather' AND Slot = 'Head'", filter);
        }

        public List<Item> ListLeatherLeg(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Leather' AND Slot = 'Legs'", filter);
        }

        public List<Item> ListLeatherShoulder(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Leather' AND Slot = 'Shoulder'", filter);
        }

        public List<Item> ListLeatherWaist(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Leather' AND Slot = 'Waist'", filter);
        }

        public List<Item> ListLeatherWrist(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Leather' AND Slot = 'Wrist'", filter);
        }
        #endregion

        #region mail
        public List<Item> ListMailChest(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Mail' AND Slot = 'Chest'", filter);
        }

        public List<Item> ListMailFeet(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Mail' AND Slot = 'Feet'", filter);
        }

        public List<Item> ListMailHands(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Mail' AND Slot = 'Hands'", filter);
        }

        public List<Item> ListMailHead(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Mail' AND Slot = 'Head'", filter);
        }

        public List<Item> ListMailLeg(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Mail' AND Slot = 'Legs'", filter);
        }

        public List<Item> ListMailShoulder(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Mail' AND Slot = 'Shoulder'", filter);
        }

        public List<Item> ListMailWaist(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Mail' AND Slot = 'Waist'", filter);
        }

        public List<Item> ListMailWrist(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Mail' AND Slot = 'Wrist'", filter);
        }
        #endregion

        #region plate
        public List<Item> ListPlateChest(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Plate' AND Slot = 'Chest'", filter);
        }

        public List<Item> ListPlateFeet(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Plate' AND Slot = 'Feet'", filter);
        }

        public List<Item> ListPlateHands(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Plate' AND Slot = 'Hands'", filter);
        }

        public List<Item> ListPlateHead(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Plate' AND Slot = 'Head'", filter);
        }

        public List<Item> ListPlateLeg(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Plate' AND Slot = 'Legs'", filter);
        }

        public List<Item> ListPlateShoulder(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Plate' AND Slot = 'Shoulder'", filter);
        }

        public List<Item> ListPlateWaist(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Plate' AND Slot = 'Waist'", filter);
        }

        public List<Item> ListPlateWrist(string filter)
        {
            return ListArmour3(" WHERE ArmourType = 'Plate' AND Slot = 'Wrist'", filter);
        }
        #endregion

        #region weapons
        public List<Item> ListAllWeapons(string filter)
        {
            return ListWeapons("", filter);
        }

        public List<Item> ListOneHandedWeapons(string filter)
        {
            return ListWeapons(" WHERE Slot = 'OneHand'", filter);
            //return Debug("", filter);
        }
        public List<Item> ListTwoHandedWeapons(string filter)
        {
            return ListWeapons(" WHERE Slot = 'TwoHand'", filter);
        }
        public List<Item> ListRangedWeapons(string filter)
        {
            return ListWeapons(" WHERE Slot = 'Ranged'", filter);
        }

        public List<Item> ListOneHandedSwords(string filter)
        {
            return ListWeapons(" WHERE Slot = 'OneHand' AND WeaponType='Sword'", filter);
        }
        public List<Item> ListOneHandedAxes(string filter)
        {
            return ListWeapons(" WHERE Slot = 'OneHand' AND WeaponType='Axe'", filter);
        }
        public List<Item> ListOneHandedMaces(string filter)
        {
            return ListWeapons(" WHERE Slot = 'OneHand' AND WeaponType='Mace'", filter);
        }
        public List<Item> ListDaggers(string filter)
        {
            return ListWeapons(" WHERE Slot = 'OneHand' AND WeaponType='Dagger'", filter);
        }
        public List<Item> ListFistWeapons(string filter)
        {
            return ListWeapons(" WHERE Slot = 'OneHand' AND WeaponType='Fist'", filter);
        }
        public List<Item> ListPolearms(string filter)
        {
            return ListWeapons(" WHERE Slot = 'TwoHand' AND WeaponType='Polearm'", filter);
        }
        public List<Item> ListStaffs(string filter)
        {
            return ListWeapons(" WHERE Slot = 'TwoHand' AND WeaponType='Staff'", filter);
        }
        public List<Item> ListTwoHandedSwords(string filter)
        {
            return ListWeapons(" WHERE Slot = 'TwoHand' AND WeaponType='Sword'", filter);
        }
        public List<Item> ListTwoHandedAxes(string filter)
        {
            return ListWeapons(" WHERE Slot = 'TwoHand' AND WeaponType='Axe'", filter);
        }
        public List<Item> ListTwoHandedMaces(string filter)
        {
            return ListWeapons(" WHERE Slot = 'TwoHand' AND WeaponType='Mace'", filter);
        }
        public List<Item> ListFishingPoles(string filter)
        {
            return ListWeapons(" WHERE Slot = 'TwoHand' AND WeaponType='FishingPole'", filter);
        }

        public List<Item> ListCrossbows(string filter)
        {
            return ListWeapons(" WHERE Slot = 'Ranged' AND WeaponType='Crossbow'", filter);
        }
        public List<Item> ListGuns(string filter)
        {
            return ListWeapons(" WHERE Slot = 'Ranged' AND WeaponType='Gun'", filter);
        }
        public List<Item> ListBows(string filter)
        {
            return ListWeapons(" WHERE Slot = 'Ranged' AND WeaponType='Bow'", filter);
        }
        public List<Item> ListWands(string filter)
        {
            return ListWeapons(" WHERE Slot = 'Ranged' AND WeaponType='Wand'", filter);
        }
        public List<Item> ListThrown(string filter)
        {
            return ListWeapons(" WHERE Slot = 'Ranged' AND WeaponType='Thrown'", filter);
        }

        public List<Item> ListMiscellaneous(string filter)
        {
            return ListWeapons(" WHERE WeaponType = 'Miscellaneous'", filter);
        }
        #endregion

        /// <summary>
        /// This is private so that someone cannot pass false string values into it
        /// The other methods must be called which are public and in turn call this method
        /// </summary>
        /// <param name="searchSet"></param>
        /// <returns></returns>
        private List<Item> ListArmour(string searchSet)
        {
            SQLiteConnection mySQLConnection = new SQLiteConnection(Database_testing.Properties.Settings.Default.ItemsConnectionString);
            try
            {
                mySQLConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            SQLiteDataReader myReader = null;
            SQLiteCommand myCommandRead = null;

            switch (searchSet)
            {
                case "":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items", mySQLConnection); //get all columns
                    break;
                case "Cloth":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Cloth' AND Slot != 'Back'", mySQLConnection); //get all columns
                    break;
                case "Leather":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Leather'", mySQLConnection); //get all columns
                    break;
                case "Mail":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Mail'", mySQLConnection); //get all columns
                    break;
                case "Plate":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Plate'", mySQLConnection); //get all columns
                    break;
                case "Amulet":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE Slot = 'Neck'", mySQLConnection); //get all columns
                    break;
                case "Ring":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE Slot = 'Finger'", mySQLConnection); //get all columns
                    break;
                case "Trinket":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE Slot = 'Trinket'", mySQLConnection); //get all columns
                    break;
                case "Cloak":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE Slot = 'Back'", mySQLConnection); //get all columns
                    break;
                case "OffHandFrill":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'OffHandFrill'", mySQLConnection); //get all columns
                    break;
                case "Shield":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Shield'", mySQLConnection); //get all columns
                    break;
                case "Miscellaneous":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Miscellaneous'", mySQLConnection); //get all columns
                    break;
                //cloth
                case "Cloth Chest":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Cloth' AND Slot = 'Chest'", mySQLConnection); //get all columns
                    break;
                case "Cloth Feet":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Cloth' AND Slot = 'Feet'", mySQLConnection); //get all columns
                    break;
                case "Cloth Hands":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Cloth' AND Slot = 'Hands'", mySQLConnection); //get all columns
                    break;
                case "Cloth Head":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Cloth' AND Slot = 'Head'", mySQLConnection); //get all columns
                    break;
                case "Cloth Legs":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Cloth' AND Slot = 'Legs'", mySQLConnection); //get all columns
                    break;
                case "Cloth Shoulder":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Cloth' AND Slot = 'Shoulder'", mySQLConnection); //get all columns
                    break;
                case "Cloth Waist":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Cloth' AND Slot = 'Waist'", mySQLConnection); //get all columns
                    break;
                case "Cloth Wrist":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Cloth' AND Slot = 'Wrist'", mySQLConnection); //get all columns
                    break;
                //leather
                case "Leather Chest":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Leather' AND Slot = 'Chest'", mySQLConnection); //get all columns
                    break;
                case "Leather Feet":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Leather' AND Slot = 'Feet'", mySQLConnection); //get all columns
                    break;
                case "Leather Hands":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Leather' AND Slot = 'Hands'", mySQLConnection); //get all columns
                    break;
                case "Leather Head":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Leather' AND Slot = 'Head'", mySQLConnection); //get all columns
                    break;
                case "Leather Legs":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Leather' AND Slot = 'Legs'", mySQLConnection); //get all columns
                    break;
                case "Leather Shoulder":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Leather' AND Slot = 'Shoulder'", mySQLConnection); //get all columns
                    break;
                case "Leather Waist":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Leather' AND Slot = 'Waist'", mySQLConnection); //get all columns
                    break;
                case "Leather Wrist":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Leather' AND Slot = 'Wrist'", mySQLConnection); //get all columns
                    break;
                //mail
                case "Mail Chest":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Mail' AND Slot = 'Chest'", mySQLConnection); //get all columns
                    break;
                case "Mail Feet":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Mail' AND Slot = 'Feet'", mySQLConnection); //get all columns
                    break;
                case "Mail Hands":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Mail' AND Slot = 'Hands'", mySQLConnection); //get all columns
                    break;
                case "Mail Head":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Mail' AND Slot = 'Head'", mySQLConnection); //get all columns
                    break;
                case "Mail Legs":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Mail' AND Slot = 'Legs'", mySQLConnection); //get all columns
                    break;
                case "Mail Shoulder":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Mail' AND Slot = 'Shoulder'", mySQLConnection); //get all columns
                    break;
                case "Mail Waist":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Mail' AND Slot = 'Waist'", mySQLConnection); //get all columns
                    break;
                case "Mail Wrist":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Mail' AND Slot = 'Wrist'", mySQLConnection); //get all columns
                    break;
                //plate
                case "Plate Chest":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Plate' AND Slot = 'Chest'", mySQLConnection); //get all columns
                    break;
                case "Plate Feet":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Plate' AND Slot = 'Feet'", mySQLConnection); //get all columns
                    break;
                case "Plate Hands":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Plate' AND Slot = 'Hands'", mySQLConnection); //get all columns
                    break;
                case "Plate Head":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Plate' AND Slot = 'Head'", mySQLConnection); //get all columns
                    break;
                case "Plate Legs":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Plate' AND Slot = 'Legs'", mySQLConnection); //get all columns
                    break;
                case "Plate Shoulder":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Plate' AND Slot = 'Shoulder'", mySQLConnection); //get all columns
                    break;
                case "Plate Waist":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Plate' AND Slot = 'Waist' AND Defence > 0", mySQLConnection); //get all columns
                    break;
                case "Plate Wrist":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Plate' AND Slot = 'Wrist'", mySQLConnection); //get all columns
                    break;
            }

            myReader = myCommandRead.ExecuteReader(); //set the reader //error here if invalid item passed in (nul refrence)

            List<Item> items = new List<Item>(); //list of items found

            if (myReader.HasRows) //item search found something
            {
                while (myReader.Read()) //for each row
                {
                    //cerate an item
                    Item theItem = new Item
                        (
                        false,
                        myReader.GetInt32(0),//ItemID
                        myReader.GetString(1),//Name
                        myReader.GetInt32(2),//RequiredLevel
                        myReader.GetString(3),//OtherRequirements
                        myReader.GetString(4),//OtherRequirements2
                        myReader.GetBoolean(5),//BoE
                        myReader.GetBoolean(6),//BoP
                        myReader.GetBoolean(7),//QuestItem
                        myReader.GetBoolean(8),//IsUnique
                        myReader.GetBoolean(9),//BeginQuest
                        myReader.GetString(10),//Quality
                        myReader.GetInt32(11),//Armour
                        myReader.GetInt32(12),//Durability
                        myReader.GetBoolean(13),//RandomBonuses
                        myReader.GetInt32(14),//Agility
                        myReader.GetInt32(15),//Intellect
                        myReader.GetInt32(16),//Spirit
                        myReader.GetInt32(17),//Stamina
                        myReader.GetInt32(18),//Strength
                        myReader.GetInt32(19),//IncreacedDaggers
                        myReader.GetInt32(20),//IncreacedSwords
                        myReader.GetInt32(21),//IncreacedAxes
                        myReader.GetInt32(22),//ManaPer5
                        myReader.GetInt32(23),//HealthPer5
                        myReader.GetInt32(24),//Hit
                        myReader.GetInt32(25),//SpellHit
                        myReader.GetInt32(26),//Critical
                        myReader.GetInt32(27),//SpellCritical
                        myReader.GetInt32(28),//Dodge
                        myReader.GetInt32(29),//Parry
                        myReader.GetInt32(30),//Defence
                        myReader.GetInt32(31),//BaseBlock
                        myReader.GetInt32(32),//BlockValue
                        myReader.GetInt32(33),//BlockPercent
                        myReader.GetInt32(34),//AttackPower
                        myReader.GetInt32(35),//RangedAttackPower
                        myReader.GetInt32(36),//SpellDamage
                        myReader.GetInt32(37),//SpellHealing
                        myReader.GetInt32(38),//ResistanceDecreace
                        myReader.GetInt32(39),//ArcaneDamage
                        myReader.GetInt32(40),//ShadowDamage
                        myReader.GetInt32(41),//FireDamage
                        myReader.GetInt32(42),//NatureDamage
                        myReader.GetInt32(43),//FrostDamage
                        myReader.GetInt32(44),//NatureResistance
                        myReader.GetInt32(45),//FireResistance
                        myReader.GetInt32(46),//ShadowResistance
                        myReader.GetInt32(47),//FrostResistance
                        myReader.GetInt32(48),//ArcaneResistance
                        myReader.GetString(49),//MiscEquip
                        myReader.GetString(50),//OnUse
                        myReader.GetString(51),//YellowText
                        myReader.GetString(52),//ArmourType
                        myReader.GetString(53)//Slot
                        );
                    //Console.WriteLine("\"" + myReader[52].ToString() + "\"");
                    items.Add(theItem); //add to list
                }
            }

            myReader.Close();
            mySQLConnection.Close();

            return items;
        }

        private List<Item> ListArmour2(string searchSet, string filter)
        {
            SQLiteConnection mySQLConnection = new SQLiteConnection(Database_testing.Properties.Settings.Default.ItemsConnectionString);
            try
            {
                mySQLConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            SQLiteDataReader myReader = null;
            SQLiteCommand myCommandRead = null;

            switch (searchSet)
            {
                case "":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items", mySQLConnection); //get all columns
                    break;
                case "Cloth":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Cloth' AND Slot != 'Back'", mySQLConnection); //get all columns
                    break;
                case "Leather":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Leather'", mySQLConnection); //get all columns
                    break;
                case "Mail":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Mail'", mySQLConnection); //get all columns
                    break;
                case "Plate":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Plate'", mySQLConnection); //get all columns
                    break;
                case "Amulet":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE Slot = 'Neck'" + filter, mySQLConnection); //get all columns
                    break;
                case "Ring":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE Slot = 'Finger'" + filter, mySQLConnection); //get all columns
                    break;
                case "Trinket":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE Slot = 'Trinket'", mySQLConnection); //get all columns
                    break;
                case "Cloak":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE Slot = 'Back'", mySQLConnection); //get all columns
                    break;
                case "OffHandFrill":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'OffHandFrill'", mySQLConnection); //get all columns
                    break;
                case "Shield":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Shield'", mySQLConnection); //get all columns
                    break;
                case "Miscellaneous":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Miscellaneous'", mySQLConnection); //get all columns
                    break;
                //cloth
                case "Cloth Chest":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Cloth' AND Slot = 'Chest'", mySQLConnection); //get all columns
                    break;
                case "Cloth Feet":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Cloth' AND Slot = 'Feet'", mySQLConnection); //get all columns
                    break;
                case "Cloth Hands":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Cloth' AND Slot = 'Hands'", mySQLConnection); //get all columns
                    break;
                case "Cloth Head":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Cloth' AND Slot = 'Head'", mySQLConnection); //get all columns
                    break;
                case "Cloth Legs":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Cloth' AND Slot = 'Legs'", mySQLConnection); //get all columns
                    break;
                case "Cloth Shoulder":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Cloth' AND Slot = 'Shoulder'", mySQLConnection); //get all columns
                    break;
                case "Cloth Waist":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Cloth' AND Slot = 'Waist'", mySQLConnection); //get all columns
                    break;
                case "Cloth Wrist":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Cloth' AND Slot = 'Wrist'", mySQLConnection); //get all columns
                    break;
                //leather
                case "Leather Chest":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Leather' AND Slot = 'Chest'", mySQLConnection); //get all columns
                    break;
                case "Leather Feet":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Leather' AND Slot = 'Feet'", mySQLConnection); //get all columns
                    break;
                case "Leather Hands":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Leather' AND Slot = 'Hands'", mySQLConnection); //get all columns
                    break;
                case "Leather Head":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Leather' AND Slot = 'Head'", mySQLConnection); //get all columns
                    break;
                case "Leather Legs":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Leather' AND Slot = 'Legs'", mySQLConnection); //get all columns
                    break;
                case "Leather Shoulder":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Leather' AND Slot = 'Shoulder'", mySQLConnection); //get all columns
                    break;
                case "Leather Waist":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Leather' AND Slot = 'Waist'", mySQLConnection); //get all columns
                    break;
                case "Leather Wrist":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Leather' AND Slot = 'Wrist'", mySQLConnection); //get all columns
                    break;
                //mail
                case "Mail Chest":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Mail' AND Slot = 'Chest'", mySQLConnection); //get all columns
                    break;
                case "Mail Feet":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Mail' AND Slot = 'Feet'", mySQLConnection); //get all columns
                    break;
                case "Mail Hands":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Mail' AND Slot = 'Hands'", mySQLConnection); //get all columns
                    break;
                case "Mail Head":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Mail' AND Slot = 'Head'", mySQLConnection); //get all columns
                    break;
                case "Mail Legs":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Mail' AND Slot = 'Legs'", mySQLConnection); //get all columns
                    break;
                case "Mail Shoulder":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Mail' AND Slot = 'Shoulder'", mySQLConnection); //get all columns
                    break;
                case "Mail Waist":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Mail' AND Slot = 'Waist'", mySQLConnection); //get all columns
                    break;
                case "Mail Wrist":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Mail' AND Slot = 'Wrist'", mySQLConnection); //get all columns
                    break;
                //plate
                case "Plate Chest":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Plate' AND Slot = 'Chest'", mySQLConnection); //get all columns
                    break;
                case "Plate Feet":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Plate' AND Slot = 'Feet'", mySQLConnection); //get all columns
                    break;
                case "Plate Hands":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Plate' AND Slot = 'Hands'", mySQLConnection); //get all columns
                    break;
                case "Plate Head":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Plate' AND Slot = 'Head'", mySQLConnection); //get all columns
                    break;
                case "Plate Legs":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Plate' AND Slot = 'Legs'", mySQLConnection); //get all columns
                    break;
                case "Plate Shoulder":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Plate' AND Slot = 'Shoulder'", mySQLConnection); //get all columns
                    break;
                case "Plate Waist":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Plate' AND Slot = 'Waist'", mySQLConnection); //get all columns
                    break;
                case "Plate Wrist":
                    myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE ArmourType = 'Plate' AND Slot = 'Wrist'", mySQLConnection); //get all columns
                    break;
            }

            myReader = myCommandRead.ExecuteReader(); //set the reader //error here if invalid item passed in (nul refrence)

            List<Item> items = new List<Item>(); //list of items found

            if (myReader.HasRows) //item search found something
            {
                while (myReader.Read()) //for each row
                {
                    //cerate an item
                    Item theItem = new Item
                        (
                        false,
                        myReader.GetInt32(0),//ItemID
                        myReader.GetString(1),//Name
                        myReader.GetInt32(2),//RequiredLevel
                        myReader.GetString(3),//OtherRequirements
                        myReader.GetString(4),//OtherRequirements2
                        myReader.GetBoolean(5),//BoE
                        myReader.GetBoolean(6),//BoP
                        myReader.GetBoolean(7),//QuestItem
                        myReader.GetBoolean(8),//IsUnique
                        myReader.GetBoolean(9),//BeginQuest
                        myReader.GetString(10),//Quality
                        myReader.GetInt32(11),//Armour
                        myReader.GetInt32(12),//Durability
                        myReader.GetBoolean(13),//RandomBonuses
                        myReader.GetInt32(14),//Agility
                        myReader.GetInt32(15),//Intellect
                        myReader.GetInt32(16),//Spirit
                        myReader.GetInt32(17),//Stamina
                        myReader.GetInt32(18),//Strength
                        myReader.GetInt32(19),//IncreacedDaggers
                        myReader.GetInt32(20),//IncreacedSwords
                        myReader.GetInt32(21),//IncreacedAxes
                        myReader.GetInt32(22),//ManaPer5
                        myReader.GetInt32(23),//HealthPer5
                        myReader.GetInt32(24),//Hit
                        myReader.GetInt32(25),//SpellHit
                        myReader.GetInt32(26),//Critical
                        myReader.GetInt32(27),//SpellCritical
                        myReader.GetInt32(28),//Dodge
                        myReader.GetInt32(29),//Parry
                        myReader.GetInt32(30),//Defence
                        myReader.GetInt32(31),//BaseBlock
                        myReader.GetInt32(32),//BlockValue
                        myReader.GetInt32(33),//BlockPercent
                        myReader.GetInt32(34),//AttackPower
                        myReader.GetInt32(35),//RangedAttackPower
                        myReader.GetInt32(36),//SpellDamage
                        myReader.GetInt32(37),//SpellHealing
                        myReader.GetInt32(38),//ResistanceDecreace
                        myReader.GetInt32(39),//ArcaneDamage
                        myReader.GetInt32(40),//ShadowDamage
                        myReader.GetInt32(41),//FireDamage
                        myReader.GetInt32(42),//NatureDamage
                        myReader.GetInt32(43),//FrostDamage
                        myReader.GetInt32(44),//NatureResistance
                        myReader.GetInt32(45),//FireResistance
                        myReader.GetInt32(46),//ShadowResistance
                        myReader.GetInt32(47),//FrostResistance
                        myReader.GetInt32(48),//ArcaneResistance
                        myReader.GetString(49),//MiscEquip
                        myReader.GetString(50),//OnUse
                        myReader.GetString(51),//YellowText
                        myReader.GetString(52),//ArmourType
                        myReader.GetString(53)//Slot
                        );
                    //Console.WriteLine("\"" + myReader[52].ToString() + "\"");
                    items.Add(theItem); //add to list
                }
            }

            myReader.Close();
            mySQLConnection.Close();

            return items;
        }

        public List<Item> ListArmour3(string search, string filter)
        {
            //check to see if we are trying to do an AND without a WHERE, if so replace
            if (search == string.Empty && Regex.IsMatch(filter, " AND ."))
            {
                filter = filter.Remove(0, 4);
                filter = " WHERE" + filter;
            }

            SQLiteConnection mySQLConnection = new SQLiteConnection(Database_testing.Properties.Settings.Default.ItemsConnectionString);
            try
            {
                mySQLConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            SQLiteDataReader myReader = null;
            SQLiteCommand myCommandRead = null;

            myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items" + search + filter, mySQLConnection); //get all columns

            myReader = myCommandRead.ExecuteReader(); //set the reader //error here if invalid item passed in (nul refrence)

            List<Item> items = new List<Item>(); //list of items found

            if (myReader.HasRows) //item search found something
            {
                while (myReader.Read()) //for each row
                {
                    //cerate an item
                    Item theItem = new Item
                        (
                        false,
                        myReader.GetInt32(0),//ItemID
                        myReader.GetString(1),//Name
                        myReader.GetInt32(2),//RequiredLevel
                        myReader.GetString(3),//OtherRequirements
                        myReader.GetString(4),//OtherRequirements2
                        myReader.GetBoolean(5),//BoE
                        myReader.GetBoolean(6),//BoP
                        myReader.GetBoolean(7),//QuestItem
                        myReader.GetBoolean(8),//IsUnique
                        myReader.GetBoolean(9),//BeginQuest
                        myReader.GetString(10),//Quality
                        myReader.GetInt32(11),//Armour
                        myReader.GetInt32(12),//Durability
                        myReader.GetBoolean(13),//RandomBonuses
                        myReader.GetInt32(14),//Agility
                        myReader.GetInt32(15),//Intellect
                        myReader.GetInt32(16),//Spirit
                        myReader.GetInt32(17),//Stamina
                        myReader.GetInt32(18),//Strength
                        myReader.GetInt32(19),//IncreacedDaggers
                        myReader.GetInt32(20),//IncreacedSwords
                        myReader.GetInt32(21),//IncreacedAxes
                        myReader.GetInt32(22),//ManaPer5
                        myReader.GetInt32(23),//HealthPer5
                        myReader.GetInt32(24),//Hit
                        myReader.GetInt32(25),//SpellHit
                        myReader.GetInt32(26),//Critical
                        myReader.GetInt32(27),//SpellCritical
                        myReader.GetInt32(28),//Dodge
                        myReader.GetInt32(29),//Parry
                        myReader.GetInt32(30),//Defence
                        myReader.GetInt32(31),//BaseBlock
                        myReader.GetInt32(32),//BlockValue
                        myReader.GetInt32(33),//BlockPercent
                        myReader.GetInt32(34),//AttackPower
                        myReader.GetInt32(35),//RangedAttackPower
                        myReader.GetInt32(36),//SpellDamage
                        myReader.GetInt32(37),//SpellHealing
                        myReader.GetInt32(38),//ResistanceDecreace
                        myReader.GetInt32(39),//ArcaneDamage
                        myReader.GetInt32(40),//ShadowDamage
                        myReader.GetInt32(41),//FireDamage
                        myReader.GetInt32(42),//NatureDamage
                        myReader.GetInt32(43),//FrostDamage
                        myReader.GetInt32(44),//NatureResistance
                        myReader.GetInt32(45),//FireResistance
                        myReader.GetInt32(46),//ShadowResistance
                        myReader.GetInt32(47),//FrostResistance
                        myReader.GetInt32(48),//ArcaneResistance
                        myReader.GetString(49),//MiscEquip
                        myReader.GetString(50),//OnUse
                        myReader.GetString(51),//YellowText
                        myReader.GetString(52),//ArmourType
                        myReader.GetString(53)//Slot
                        );
                    //Console.WriteLine("\"" + myReader[52].ToString() + "\"");
                    items.Add(theItem); //add to list
                }
            }

            myReader.Close();
            mySQLConnection.Close();

            return items;
        }

        public List<Item> ListWeapons(string search, string filter)
        {
            //check to see if we are trying to do an AND without a WHERE, if so replace
            if (search == string.Empty && Regex.IsMatch(filter, " AND ."))
            {
                filter = filter.Remove(0, 4);
                filter = " WHERE" + filter;
            }

            SQLiteConnection mySQLConnection = new SQLiteConnection(Database_testing.Properties.Settings.Default.ItemsConnectionString);
            try
            {
                mySQLConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            SQLiteDataReader myReader = null;
            SQLiteCommand myCommandRead = null;

            myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, AttackPower, RangedAttackPower, FeralAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, WeaponType, Slot, DPS, DamageLow, DamageHigh, Speed, ExtraDamage, ChanceOnHit, WandDamage FROM Weapons" + search + filter, mySQLConnection); //get all columns

            myReader = myCommandRead.ExecuteReader(); //set the reader //error here if invalid item passed in (nul refrence)

            List<Item> items = new List<Item>(); //list of items found

            if (myReader.HasRows) //item search found something
            {
                while (myReader.Read()) //for each row
                {
                    //cerate an item
                    Item theItem = new Item
                        (
                        true,
                        myReader.GetInt32(0),//ItemID
                        myReader.GetString(1),//Name
                        myReader.GetInt32(2),//RequiredLevel
                        myReader.GetString(3),//OtherRequirements
                        myReader.GetString(4),//OtherRequirements2
                        myReader.GetBoolean(5),//BoE
                        myReader.GetBoolean(6),//BoP
                        myReader.GetBoolean(7),//QuestItem
                        myReader.GetBoolean(8),//IsUnique
                        myReader.GetString(9),//Quality
                        myReader.GetInt32(10),//Armour
                        myReader.GetInt32(11),//Durability
                        myReader.GetBoolean(12),//RandomBonuses
                        myReader.GetInt32(13),//Agility
                        myReader.GetInt32(14),//Intellect
                        myReader.GetInt32(15),//Spirit
                        myReader.GetInt32(16),//Stamina
                        myReader.GetInt32(17),//Strength
                        myReader.GetInt32(18),//IncreacedDaggers
                        myReader.GetInt32(19),//IncreacedSwords
                        myReader.GetInt32(20),//IncreacedAxes
                        myReader.GetInt32(21),//ManaPer5
                        myReader.GetInt32(22),//HealthPer5
                        myReader.GetInt32(23),//Hit
                        myReader.GetInt32(24),//SpellHit
                        myReader.GetInt32(25),//Critical
                        myReader.GetInt32(26),//SpellCritical
                        myReader.GetInt32(27),//Dodge
                        myReader.GetInt32(28),//Parry
                        myReader.GetInt32(29),//Defence
                        myReader.GetInt32(30),//AttackPower
                        myReader.GetInt32(31),//RangedAttackPower
                        myReader.GetInt32(32),//FeralAttackPower
                        myReader.GetInt32(33),//SpellDamage
                        myReader.GetInt32(34),//SpellHealing
                        myReader.GetInt32(35),//ResistanceDecreace
                        myReader.GetInt32(36),//ArcaneDamage
                        myReader.GetInt32(37),//ShadowDamage
                        myReader.GetInt32(38),//FireDamage
                        myReader.GetInt32(39),//NatureDamage
                        myReader.GetInt32(40),//FrostDamage
                        myReader.GetInt32(41),//NatureResistance
                        myReader.GetInt32(42),//FireResistance
                        myReader.GetInt32(43),//ShadowResistance
                        myReader.GetInt32(44),//FrostResistance
                        myReader.GetInt32(45),//ArcaneResistance
                        myReader.GetString(46),//MiscEquip
                        myReader.GetString(47),//OnUse
                        myReader.GetString(48),//YellowText
                        myReader.GetString(49),//WeaponType
                        myReader.GetString(50),//Slot
                        (float)myReader.GetDouble(51),//DPS
                        (float)myReader.GetDouble(52),//DamageLow
                        (float)myReader.GetDouble(53),//DamageHigh
                        (float)myReader.GetDouble(54),//Speed
                        myReader.GetString(55),//ExtraDamage
                        myReader.GetString(56),//ChanceOnHit
                        myReader.GetString(57)//WandDamage
                        );
                    items.Add(theItem);
                }
            }

            myReader.Close();
            mySQLConnection.Close();

            return items;
        }

        public List<Item> Debug()
        {
            SQLiteConnection mySQLConnection = new SQLiteConnection(Database_testing.Properties.Settings.Default.ItemsConnectionString);
            try
            {
                mySQLConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            SQLiteDataReader myReader = null;
            SQLiteCommand myCommandRead = null;

            myCommandRead = new SQLiteCommand("SELECT Name FROM Weapons WHERE WeaponType='Null'", mySQLConnection); //get all columns

            myReader = myCommandRead.ExecuteReader(); //set the reader //error here if invalid item passed in (nul refrence)

            List<Item> items = new List<Item>(); //list of items found

            if (myReader.HasRows) //item search found something
            {
                while (myReader.Read()) //for each row
                {
                    Console.WriteLine(myReader[0].ToString());
                }
            }

            myReader.Close();
            mySQLConnection.Close();

            return null;
        }

        /// <summary>
        /// Searches both the weapons and items tables for a given name, name does not need to be specific, partials are ok
        /// </summary>
        /// <param name="searchTerm">partial name to search for</param>
        /// <returns>returns a list of item objects that were found</returns>
        public List<Item> SearchAllItems(string searchTerm)
        {
            searchTerm = searchTerm.Replace("'", "''"); //sort apostrophes for SQL

            SQLiteConnection mySQLConnection = new SQLiteConnection(Database_testing.Properties.Settings.Default.ItemsConnectionString);
            try
            {
                mySQLConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            SQLiteDataReader myReader = null;

            SQLiteCommand myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE Name LIKE '%" + searchTerm + "%'", mySQLConnection); //get all columns where item = name passed in
            myReader = myCommandRead.ExecuteReader(); //set the reader

            List<Item> items = new List<Item>(); //list of items found

            if (myReader.HasRows) //item search found something
            {
                while (myReader.Read()) //for each row
                {
                    //cerate an item
                    Item theItem = new Item
                        (
                        false,
                        myReader.GetInt32(0),//ItemID
                        myReader.GetString(1),//Name
                        myReader.GetInt32(2),//RequiredLevel
                        myReader.GetString(3),//OtherRequirements
                        myReader.GetString(4),//OtherRequirements2
                        myReader.GetBoolean(5),//BoE
                        myReader.GetBoolean(6),//BoP
                        myReader.GetBoolean(7),//QuestItem
                        myReader.GetBoolean(8),//IsUnique
                        myReader.GetBoolean(9),//BeginQuest
                        myReader.GetString(10),//Quality
                        myReader.GetInt32(11),//Armour
                        myReader.GetInt32(12),//Durability
                        myReader.GetBoolean(13),//RandomBonuses
                        myReader.GetInt32(14),//Agility
                        myReader.GetInt32(15),//Intellect
                        myReader.GetInt32(16),//Spirit
                        myReader.GetInt32(17),//Stamina
                        myReader.GetInt32(18),//Strength
                        myReader.GetInt32(19),//IncreacedDaggers
                        myReader.GetInt32(20),//IncreacedSwords
                        myReader.GetInt32(21),//IncreacedAxes
                        myReader.GetInt32(22),//ManaPer5
                        myReader.GetInt32(23),//HealthPer5
                        myReader.GetInt32(24),//Hit
                        myReader.GetInt32(25),//SpellHit
                        myReader.GetInt32(26),//Critical
                        myReader.GetInt32(27),//SpellCritical
                        myReader.GetInt32(28),//Dodge
                        myReader.GetInt32(29),//Parry
                        myReader.GetInt32(30),//Defence
                        myReader.GetInt32(31),//BaseBlock
                        myReader.GetInt32(32),//BlockValue
                        myReader.GetInt32(33),//BlockPercent
                        myReader.GetInt32(34),//AttackPower
                        myReader.GetInt32(35),//RangedAttackPower
                        myReader.GetInt32(36),//SpellDamage
                        myReader.GetInt32(37),//SpellHealing
                        myReader.GetInt32(38),//ResistanceDecreace
                        myReader.GetInt32(39),//ArcaneDamage
                        myReader.GetInt32(40),//ShadowDamage
                        myReader.GetInt32(41),//FireDamage
                        myReader.GetInt32(42),//NatureDamage
                        myReader.GetInt32(43),//FrostDamage
                        myReader.GetInt32(44),//NatureResistance
                        myReader.GetInt32(45),//FireResistance
                        myReader.GetInt32(46),//ShadowResistance
                        myReader.GetInt32(47),//FrostResistance
                        myReader.GetInt32(48),//ArcaneResistance
                        myReader.GetString(49),//MiscEquip
                        myReader.GetString(50),//OnUse
                        myReader.GetString(51),//YellowText
                        myReader.GetString(52),//ArmourType
                        myReader.GetString(53)//Slot]"
                        );
                    items.Add(theItem); //add to list
                }
            }

            myReader.Close(); //close reader ready for next search

            //weapon search
            myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, AttackPower, RangedAttackPower, FeralAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, WeaponType, Slot, DPS, DamageLow, DamageHigh, Speed, ExtraDamage, ChanceOnHit, WandDamage FROM Weapons WHERE Name LIKE '%" + searchTerm + "%'", mySQLConnection); //get all columns where item = name passed in
            myReader = myCommandRead.ExecuteReader(); //set the reader

            if (myReader.HasRows)
            {
                while (myReader.Read()) //for each row
                {
                    //int itemID = myReader.GetInt32(0);//ItemID
                    //string name = myReader.GetString(1);//Name
                    //int requiredLevel = myReader.GetInt32(2);//RequiredLevel
                    //string otherRequirements = myReader.GetString(3);//OtherRequirements
                    //string otherRequirements2 = myReader.GetString(4);//OtherRequirements2
                    //bool boe = myReader.GetBoolean(5);//BoE
                    //bool bop = myReader.GetBoolean(6);//BoP
                    //bool questItem = myReader.GetBoolean(7);//QuestItem
                    //bool unique = myReader.GetBoolean(8);//IsUnique
                    //string quality = myReader.GetString(9);//Quality
                    //int armour = myReader.GetInt32(10);//Armour
                    //int durability = myReader.GetInt32(11);//Durability
                    //bool randomBonuses = myReader.GetBoolean(12);//RandomBonuses
                    //int agility = myReader.GetInt32(13);//Agility
                    //int intellect = myReader.GetInt32(14);//Intellect
                    //int spirit = myReader.GetInt32(15);//Spirit
                    //int stamina = myReader.GetInt32(16);//Stamina
                    //int strength = myReader.GetInt32(17);//Strength
                    //int increasedDaggers = myReader.GetInt32(18);//IncreacedDaggers
                    //int increasedSwords = myReader.GetInt32(19);//IncreacedSwords
                    //int increasedAxes = myReader.GetInt32(20);//IncreacedAxes
                    //int mp5 = myReader.GetInt32(21);//ManaPer5
                    //int hp5 = myReader.GetInt32(22);//HealthPer5
                    //int hit = myReader.GetInt32(23);//Hit
                    //int spellhit = myReader.GetInt32(24);//SpellHit
                    //int crit = myReader.GetInt32(25);//Critical
                    //int spellCrit = myReader.GetInt32(26);//SpellCritical
                    //int dodge = myReader.GetInt32(27);//Dodge
                    //int parry = myReader.GetInt32(28);//Parry
                    //int defence = myReader.GetInt32(29);//Defence
                    //int blockValue = myReader.GetInt32(30);//BlockValue
                    //int blockPercent = myReader.GetInt32(31);//BlockPercent
                    //int attackPower = myReader.GetInt32(32);//AttackPower
                    //int rangedAttackPower = myReader.GetInt32(33);//RangedAttackPower
                    //int feralAttackPower = myReader.GetInt32(34);//FeralAttackPower
                    //int spellDamage = myReader.GetInt32(35);//SpellDamage
                    //int spellHealing = myReader.GetInt32(36);//SpellHealing
                    //int resistanceDecrease = myReader.GetInt32(37);//ResistanceDecreace
                    //int arcaneDamage = myReader.GetInt32(38);//ArcaneDamage
                    //int shadowDamage = myReader.GetInt32(39);//ShadowDamage
                    //int fireDamage = myReader.GetInt32(40);//FireDamage
                    //int natureDamage = myReader.GetInt32(41);//NatureDamage
                    //int frostDamage = myReader.GetInt32(42);//FrostDamage
                    //int natureRes = myReader.GetInt32(43);//NatureResistance
                    //int fireRes = myReader.GetInt32(44);//FireResistance
                    //int shadowRes = myReader.GetInt32(45);//ShadowResistance
                    //string temp = myReader[46].ToString();
                    //int frostRes = myReader.GetInt32(46);//FrostResistance
                    //int arcaneRes = myReader.GetInt32(47);//ArcaneResistance
                    //string miscEquip = myReader.GetString(48);//MiscEquip
                    //string use = myReader.GetString(49);//OnUse
                    //string yellowText = myReader.GetString(50);//YellowText
                    //string weaponType = myReader.GetString(51);//WeaponType
                    //string slot = myReader.GetString(52);//Slot
                    //float DPS = (float)myReader.GetDouble(53);//DPS
                    //float damageLow = (float)myReader.GetDouble(54);//DamageLow
                    //float damageHigh = (float)myReader.GetDouble(55);//DamageHigh
                    //float speed = (float)myReader.GetDouble(56);//Speed
                    //string extraDamage = myReader.GetString(57);//ExtraDamage
                    //string chanceOnHit = myReader.GetString(58);//ChanceOnHit
                    //string wandDamage = myReader.GetString(59);//WandDamage
                    //create item
                    Item theItem = new Item
                        (
                        true,
                        myReader.GetInt32(0),//ItemID
                        myReader.GetString(1),//Name
                        myReader.GetInt32(2),//RequiredLevel
                        myReader.GetString(3),//OtherRequirements
                        myReader.GetString(4),//OtherRequirements2
                        myReader.GetBoolean(5),//BoE
                        myReader.GetBoolean(6),//BoP
                        myReader.GetBoolean(7),//QuestItem
                        myReader.GetBoolean(8),//IsUnique
                        myReader.GetString(9),//Quality
                        myReader.GetInt32(10),//Armour
                        myReader.GetInt32(11),//Durability
                        myReader.GetBoolean(12),//RandomBonuses
                        myReader.GetInt32(13),//Agility
                        myReader.GetInt32(14),//Intellect
                        myReader.GetInt32(15),//Spirit
                        myReader.GetInt32(16),//Stamina
                        myReader.GetInt32(17),//Strength
                        myReader.GetInt32(18),//IncreacedDaggers
                        myReader.GetInt32(19),//IncreacedSwords
                        myReader.GetInt32(20),//IncreacedAxes
                        myReader.GetInt32(21),//ManaPer5
                        myReader.GetInt32(22),//HealthPer5
                        myReader.GetInt32(23),//Hit
                        myReader.GetInt32(24),//SpellHit
                        myReader.GetInt32(25),//Critical
                        myReader.GetInt32(26),//SpellCritical
                        myReader.GetInt32(27),//Dodge
                        myReader.GetInt32(28),//Parry
                        myReader.GetInt32(29),//Defence
                        myReader.GetInt32(30),//AttackPower
                        myReader.GetInt32(31),//RangedAttackPower
                        myReader.GetInt32(32),//FeralAttackPower
                        myReader.GetInt32(33),//SpellDamage
                        myReader.GetInt32(34),//SpellHealing
                        myReader.GetInt32(35),//ResistanceDecreace
                        myReader.GetInt32(36),//ArcaneDamage
                        myReader.GetInt32(37),//ShadowDamage
                        myReader.GetInt32(38),//FireDamage
                        myReader.GetInt32(39),//NatureDamage
                        myReader.GetInt32(40),//FrostDamage
                        myReader.GetInt32(41),//NatureResistance
                        myReader.GetInt32(42),//FireResistance
                        myReader.GetInt32(43),//ShadowResistance
                        myReader.GetInt32(44),//FrostResistance
                        myReader.GetInt32(45),//ArcaneResistance
                        myReader.GetString(46),//MiscEquip
                        myReader.GetString(47),//OnUse
                        myReader.GetString(48),//YellowText
                        myReader.GetString(49),//WeaponType
                        myReader.GetString(50),//Slot
                        (float)myReader.GetDouble(51),//DPS
                        (float)myReader.GetDouble(52),//DamageLow
                        (float)myReader.GetDouble(53),//DamageHigh
                        (float)myReader.GetDouble(54),//Speed
                        myReader.GetString(55),//ExtraDamage
                        myReader.GetString(56),//ChanceOnHit
                        myReader.GetString(57)//WandDamage
                        );
                    items.Add(theItem);
                }
                myReader.Close();
            } //end

            mySQLConnection.Close();

            return items;

        }


        public List<Item> MaxStat(int number, string search, string filter)
        {
            SQLiteConnection mySQLConnection = new SQLiteConnection(Database_testing.Properties.Settings.Default.ItemsConnectionString);
            try
            {
                mySQLConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            SQLiteDataReader myReader = null;
            SQLiteCommand myCommandRead = null;
            /*
            myCommandRead = new SqlCommand("SELECT TOP 2 ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE Slot = 'Trinket'" + filter +
                " SELECT TOP 1 ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE Slot = 'Back'" + filter +
                " SELECT TOP 1 ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE Slot = 'Neck'" + filter +
                " SELECT TOP 2 ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE Slot = 'Finger'" + filter, mySQLConnection); //get all columns
            */
            myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items " + search + filter + " LIMIT " + number, mySQLConnection);

            myReader = myCommandRead.ExecuteReader(); //set the reader //error here if invalid item passed in (nul refrence)

            List<Item> items = new List<Item>(); //list of items found

            if (myReader.HasRows) //item search found something
            {
                while (myReader.Read()) //for each row
                {
                    //cerate an item
                    Item theItem = new Item
                        (
                        false,
                        myReader.GetInt32(0),//ItemID
                        myReader.GetString(1),//Name
                        myReader.GetInt32(2),//RequiredLevel
                        myReader.GetString(3),//OtherRequirements
                        myReader.GetString(4),//OtherRequirements2
                        myReader.GetBoolean(5),//BoE
                        myReader.GetBoolean(6),//BoP
                        myReader.GetBoolean(7),//QuestItem
                        myReader.GetBoolean(8),//IsUnique
                        myReader.GetBoolean(9),//BeginQuest
                        myReader.GetString(10),//Quality
                        myReader.GetInt32(11),//Armour
                        myReader.GetInt32(12),//Durability
                        myReader.GetBoolean(13),//RandomBonuses
                        myReader.GetInt32(14),//Agility
                        myReader.GetInt32(15),//Intellect
                        myReader.GetInt32(16),//Spirit
                        myReader.GetInt32(17),//Stamina
                        myReader.GetInt32(18),//Strength
                        myReader.GetInt32(19),//IncreacedDaggers
                        myReader.GetInt32(20),//IncreacedSwords
                        myReader.GetInt32(21),//IncreacedAxes
                        myReader.GetInt32(22),//ManaPer5
                        myReader.GetInt32(23),//HealthPer5
                        myReader.GetInt32(24),//Hit
                        myReader.GetInt32(25),//SpellHit
                        myReader.GetInt32(26),//Critical
                        myReader.GetInt32(27),//SpellCritical
                        myReader.GetInt32(28),//Dodge
                        myReader.GetInt32(29),//Parry
                        myReader.GetInt32(30),//Defence
                        myReader.GetInt32(31),//BaseBlock
                        myReader.GetInt32(32),//BlockValue
                        myReader.GetInt32(33),//BlockPercent
                        myReader.GetInt32(34),//AttackPower
                        myReader.GetInt32(35),//RangedAttackPower
                        myReader.GetInt32(36),//SpellDamage
                        myReader.GetInt32(37),//SpellHealing
                        myReader.GetInt32(38),//ResistanceDecreace
                        myReader.GetInt32(39),//ArcaneDamage
                        myReader.GetInt32(40),//ShadowDamage
                        myReader.GetInt32(41),//FireDamage
                        myReader.GetInt32(42),//NatureDamage
                        myReader.GetInt32(43),//FrostDamage
                        myReader.GetInt32(44),//NatureResistance
                        myReader.GetInt32(45),//FireResistance
                        myReader.GetInt32(46),//ShadowResistance
                        myReader.GetInt32(47),//FrostResistance
                        myReader.GetInt32(48),//ArcaneResistance
                        myReader.GetString(49),//MiscEquip
                        myReader.GetString(50),//OnUse
                        myReader.GetString(51),//YellowText
                        myReader.GetString(52),//ArmourType
                        myReader.GetString(53)//Slot
                        );
                    //Console.WriteLine("\"" + myReader[52].ToString() + "\"");
                    items.Add(theItem); //add to list
                }
            }

            myReader.Close();
            mySQLConnection.Close();

            return items;
        }
    }
}
