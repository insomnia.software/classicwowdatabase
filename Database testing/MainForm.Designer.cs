﻿namespace Database_testing
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DebugSearchButton = new System.Windows.Forms.Button();
            this.ItemNameTextBox = new System.Windows.Forms.TextBox();
            this.AddItemIDButton = new System.Windows.Forms.Button();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.PrintDBButton = new System.Windows.Forms.Button();
            this.SearchTextBox = new System.Windows.Forms.TextBox();
            this.SearchButton = new System.Windows.Forms.Button();
            this.DisplayButton = new System.Windows.Forms.Button();
            this.ResetButton = new System.Windows.Forms.Button();
            this.ItemsComboBox = new System.Windows.Forms.ComboBox();
            this.TypeComboBox = new System.Windows.Forms.ComboBox();
            this.slotComboBox = new System.Windows.Forms.ComboBox();
            this.FillDBButton = new System.Windows.Forms.Button();
            this.DeleteAllButton = new System.Windows.Forms.Button();
            this.Filter1CheckBox = new System.Windows.Forms.CheckBox();
            this.Filter1StatComboBox = new System.Windows.Forms.ComboBox();
            this.Filter1OperatorComboBox = new System.Windows.Forms.ComboBox();
            this.Filter1TextBox = new System.Windows.Forms.TextBox();
            this.Filter2TextBox = new System.Windows.Forms.TextBox();
            this.Filter2OperatorComboBox = new System.Windows.Forms.ComboBox();
            this.Filter2StatComboBox = new System.Windows.Forms.ComboBox();
            this.Filter2CheckBox = new System.Windows.Forms.CheckBox();
            this.Filter4TextBox = new System.Windows.Forms.TextBox();
            this.Filter4OperatorComboBox = new System.Windows.Forms.ComboBox();
            this.Filter4StatComboBox = new System.Windows.Forms.ComboBox();
            this.Filter4CheckBox = new System.Windows.Forms.CheckBox();
            this.Filter3TextBox = new System.Windows.Forms.TextBox();
            this.Filter3OperatorComboBox = new System.Windows.Forms.ComboBox();
            this.Filter3StatComboBox = new System.Windows.Forms.ComboBox();
            this.Filter3CheckBox = new System.Windows.Forms.CheckBox();
            this.qualityLabel = new System.Windows.Forms.Label();
            this.qualityComboBox = new System.Windows.Forms.ComboBox();
            this.maxButton = new System.Windows.Forms.Button();
            this.requiredLevelLabel = new System.Windows.Forms.Label();
            this.hyphenLabel = new System.Windows.Forms.Label();
            this.minLevelTextBox = new System.Windows.Forms.TextBox();
            this.maxLevelTextBox = new System.Windows.Forms.TextBox();
            this.DebugButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // DebugSearchButton
            // 
            this.DebugSearchButton.Location = new System.Drawing.Point(182, 408);
            this.DebugSearchButton.Name = "DebugSearchButton";
            this.DebugSearchButton.Size = new System.Drawing.Size(75, 23);
            this.DebugSearchButton.TabIndex = 0;
            this.DebugSearchButton.Text = "Search";
            this.DebugSearchButton.UseVisualStyleBackColor = true;
            this.DebugSearchButton.Click += new System.EventHandler(this.DebugSearchButton_Click);
            // 
            // ItemNameTextBox
            // 
            this.ItemNameTextBox.Location = new System.Drawing.Point(12, 382);
            this.ItemNameTextBox.Name = "ItemNameTextBox";
            this.ItemNameTextBox.Size = new System.Drawing.Size(419, 20);
            this.ItemNameTextBox.TabIndex = 1;
            // 
            // AddItemIDButton
            // 
            this.AddItemIDButton.Location = new System.Drawing.Point(356, 408);
            this.AddItemIDButton.Name = "AddItemIDButton";
            this.AddItemIDButton.Size = new System.Drawing.Size(75, 23);
            this.AddItemIDButton.TabIndex = 2;
            this.AddItemIDButton.Text = "Add ItemID";
            this.AddItemIDButton.UseVisualStyleBackColor = true;
            this.AddItemIDButton.Click += new System.EventHandler(this.AddItemIDButton_Click);
            // 
            // DeleteButton
            // 
            this.DeleteButton.Location = new System.Drawing.Point(12, 408);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(75, 23);
            this.DeleteButton.TabIndex = 3;
            this.DeleteButton.Text = "Delete";
            this.DeleteButton.UseVisualStyleBackColor = true;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // PrintDBButton
            // 
            this.PrintDBButton.Location = new System.Drawing.Point(96, 408);
            this.PrintDBButton.Name = "PrintDBButton";
            this.PrintDBButton.Size = new System.Drawing.Size(75, 23);
            this.PrintDBButton.TabIndex = 4;
            this.PrintDBButton.Text = "Print DB";
            this.PrintDBButton.UseVisualStyleBackColor = true;
            this.PrintDBButton.Click += new System.EventHandler(this.PrintDBButton_Click);
            // 
            // SearchTextBox
            // 
            this.SearchTextBox.Location = new System.Drawing.Point(12, 12);
            this.SearchTextBox.Name = "SearchTextBox";
            this.SearchTextBox.Size = new System.Drawing.Size(338, 20);
            this.SearchTextBox.TabIndex = 5;
            // 
            // SearchButton
            // 
            this.SearchButton.Location = new System.Drawing.Point(356, 10);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(75, 23);
            this.SearchButton.TabIndex = 8;
            this.SearchButton.Text = "Search";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // DisplayButton
            // 
            this.DisplayButton.Location = new System.Drawing.Point(356, 305);
            this.DisplayButton.Name = "DisplayButton";
            this.DisplayButton.Size = new System.Drawing.Size(75, 23);
            this.DisplayButton.TabIndex = 22;
            this.DisplayButton.Text = "Display";
            this.DisplayButton.UseVisualStyleBackColor = true;
            this.DisplayButton.Click += new System.EventHandler(this.DisplayButton_Click);
            // 
            // ResetButton
            // 
            this.ResetButton.Location = new System.Drawing.Point(356, 276);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(75, 23);
            this.ResetButton.TabIndex = 23;
            this.ResetButton.Text = "Reset";
            this.ResetButton.UseVisualStyleBackColor = true;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // ItemsComboBox
            // 
            this.ItemsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ItemsComboBox.FormattingEnabled = true;
            this.ItemsComboBox.Location = new System.Drawing.Point(12, 72);
            this.ItemsComboBox.Name = "ItemsComboBox";
            this.ItemsComboBox.Size = new System.Drawing.Size(121, 21);
            this.ItemsComboBox.TabIndex = 24;
            this.ItemsComboBox.SelectedIndexChanged += new System.EventHandler(this.ItemsComboBox_SelectedIndexChanged);
            // 
            // TypeComboBox
            // 
            this.TypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TypeComboBox.FormattingEnabled = true;
            this.TypeComboBox.Location = new System.Drawing.Point(139, 72);
            this.TypeComboBox.Name = "TypeComboBox";
            this.TypeComboBox.Size = new System.Drawing.Size(121, 21);
            this.TypeComboBox.TabIndex = 25;
            this.TypeComboBox.SelectedIndexChanged += new System.EventHandler(this.TypeComboBox_SelectedIndexChanged);
            // 
            // slotComboBox
            // 
            this.slotComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.slotComboBox.FormattingEnabled = true;
            this.slotComboBox.Location = new System.Drawing.Point(266, 72);
            this.slotComboBox.Name = "slotComboBox";
            this.slotComboBox.Size = new System.Drawing.Size(121, 21);
            this.slotComboBox.TabIndex = 26;
            // 
            // FillDBButton
            // 
            this.FillDBButton.Location = new System.Drawing.Point(12, 353);
            this.FillDBButton.Name = "FillDBButton";
            this.FillDBButton.Size = new System.Drawing.Size(75, 23);
            this.FillDBButton.TabIndex = 27;
            this.FillDBButton.Text = "Fill DB";
            this.FillDBButton.UseVisualStyleBackColor = true;
            this.FillDBButton.Click += new System.EventHandler(this.FillDBButton_Click);
            // 
            // DeleteAllButton
            // 
            this.DeleteAllButton.Location = new System.Drawing.Point(263, 408);
            this.DeleteAllButton.Name = "DeleteAllButton";
            this.DeleteAllButton.Size = new System.Drawing.Size(75, 23);
            this.DeleteAllButton.TabIndex = 28;
            this.DeleteAllButton.Text = "Delete All";
            this.DeleteAllButton.UseVisualStyleBackColor = true;
            this.DeleteAllButton.Click += new System.EventHandler(this.DeleteAllButton_Click);
            // 
            // Filter1CheckBox
            // 
            this.Filter1CheckBox.AutoSize = true;
            this.Filter1CheckBox.Location = new System.Drawing.Point(12, 120);
            this.Filter1CheckBox.Name = "Filter1CheckBox";
            this.Filter1CheckBox.Size = new System.Drawing.Size(48, 17);
            this.Filter1CheckBox.TabIndex = 29;
            this.Filter1CheckBox.Text = "Filter";
            this.Filter1CheckBox.UseVisualStyleBackColor = true;
            this.Filter1CheckBox.CheckedChanged += new System.EventHandler(this.Filter1CheckBox_CheckedChanged);
            // 
            // Filter1StatComboBox
            // 
            this.Filter1StatComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Filter1StatComboBox.Enabled = false;
            this.Filter1StatComboBox.FormattingEnabled = true;
            this.Filter1StatComboBox.Location = new System.Drawing.Point(66, 118);
            this.Filter1StatComboBox.Name = "Filter1StatComboBox";
            this.Filter1StatComboBox.Size = new System.Drawing.Size(157, 21);
            this.Filter1StatComboBox.TabIndex = 30;
            // 
            // Filter1OperatorComboBox
            // 
            this.Filter1OperatorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Filter1OperatorComboBox.Enabled = false;
            this.Filter1OperatorComboBox.FormattingEnabled = true;
            this.Filter1OperatorComboBox.Location = new System.Drawing.Point(229, 118);
            this.Filter1OperatorComboBox.Name = "Filter1OperatorComboBox";
            this.Filter1OperatorComboBox.Size = new System.Drawing.Size(52, 21);
            this.Filter1OperatorComboBox.TabIndex = 31;
            // 
            // Filter1TextBox
            // 
            this.Filter1TextBox.Enabled = false;
            this.Filter1TextBox.Location = new System.Drawing.Point(287, 118);
            this.Filter1TextBox.Name = "Filter1TextBox";
            this.Filter1TextBox.Size = new System.Drawing.Size(100, 20);
            this.Filter1TextBox.TabIndex = 32;
            // 
            // Filter2TextBox
            // 
            this.Filter2TextBox.Enabled = false;
            this.Filter2TextBox.Location = new System.Drawing.Point(287, 145);
            this.Filter2TextBox.Name = "Filter2TextBox";
            this.Filter2TextBox.Size = new System.Drawing.Size(100, 20);
            this.Filter2TextBox.TabIndex = 36;
            // 
            // Filter2OperatorComboBox
            // 
            this.Filter2OperatorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Filter2OperatorComboBox.Enabled = false;
            this.Filter2OperatorComboBox.FormattingEnabled = true;
            this.Filter2OperatorComboBox.Location = new System.Drawing.Point(229, 145);
            this.Filter2OperatorComboBox.Name = "Filter2OperatorComboBox";
            this.Filter2OperatorComboBox.Size = new System.Drawing.Size(52, 21);
            this.Filter2OperatorComboBox.TabIndex = 35;
            // 
            // Filter2StatComboBox
            // 
            this.Filter2StatComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Filter2StatComboBox.Enabled = false;
            this.Filter2StatComboBox.FormattingEnabled = true;
            this.Filter2StatComboBox.Location = new System.Drawing.Point(66, 145);
            this.Filter2StatComboBox.Name = "Filter2StatComboBox";
            this.Filter2StatComboBox.Size = new System.Drawing.Size(157, 21);
            this.Filter2StatComboBox.TabIndex = 34;
            // 
            // Filter2CheckBox
            // 
            this.Filter2CheckBox.AutoSize = true;
            this.Filter2CheckBox.Location = new System.Drawing.Point(12, 147);
            this.Filter2CheckBox.Name = "Filter2CheckBox";
            this.Filter2CheckBox.Size = new System.Drawing.Size(48, 17);
            this.Filter2CheckBox.TabIndex = 33;
            this.Filter2CheckBox.Text = "Filter";
            this.Filter2CheckBox.UseVisualStyleBackColor = true;
            this.Filter2CheckBox.CheckedChanged += new System.EventHandler(this.Filter2CheckBox_CheckedChanged);
            // 
            // Filter4TextBox
            // 
            this.Filter4TextBox.Enabled = false;
            this.Filter4TextBox.Location = new System.Drawing.Point(287, 199);
            this.Filter4TextBox.Name = "Filter4TextBox";
            this.Filter4TextBox.Size = new System.Drawing.Size(100, 20);
            this.Filter4TextBox.TabIndex = 44;
            // 
            // Filter4OperatorComboBox
            // 
            this.Filter4OperatorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Filter4OperatorComboBox.Enabled = false;
            this.Filter4OperatorComboBox.FormattingEnabled = true;
            this.Filter4OperatorComboBox.Location = new System.Drawing.Point(229, 199);
            this.Filter4OperatorComboBox.Name = "Filter4OperatorComboBox";
            this.Filter4OperatorComboBox.Size = new System.Drawing.Size(52, 21);
            this.Filter4OperatorComboBox.TabIndex = 43;
            // 
            // Filter4StatComboBox
            // 
            this.Filter4StatComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Filter4StatComboBox.Enabled = false;
            this.Filter4StatComboBox.FormattingEnabled = true;
            this.Filter4StatComboBox.Location = new System.Drawing.Point(66, 199);
            this.Filter4StatComboBox.Name = "Filter4StatComboBox";
            this.Filter4StatComboBox.Size = new System.Drawing.Size(157, 21);
            this.Filter4StatComboBox.TabIndex = 42;
            // 
            // Filter4CheckBox
            // 
            this.Filter4CheckBox.AutoSize = true;
            this.Filter4CheckBox.Location = new System.Drawing.Point(12, 201);
            this.Filter4CheckBox.Name = "Filter4CheckBox";
            this.Filter4CheckBox.Size = new System.Drawing.Size(48, 17);
            this.Filter4CheckBox.TabIndex = 41;
            this.Filter4CheckBox.Text = "Filter";
            this.Filter4CheckBox.UseVisualStyleBackColor = true;
            this.Filter4CheckBox.CheckedChanged += new System.EventHandler(this.Filter4CheckBox_CheckedChanged);
            // 
            // Filter3TextBox
            // 
            this.Filter3TextBox.Enabled = false;
            this.Filter3TextBox.Location = new System.Drawing.Point(287, 172);
            this.Filter3TextBox.Name = "Filter3TextBox";
            this.Filter3TextBox.Size = new System.Drawing.Size(100, 20);
            this.Filter3TextBox.TabIndex = 40;
            // 
            // Filter3OperatorComboBox
            // 
            this.Filter3OperatorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Filter3OperatorComboBox.Enabled = false;
            this.Filter3OperatorComboBox.FormattingEnabled = true;
            this.Filter3OperatorComboBox.Location = new System.Drawing.Point(229, 172);
            this.Filter3OperatorComboBox.Name = "Filter3OperatorComboBox";
            this.Filter3OperatorComboBox.Size = new System.Drawing.Size(52, 21);
            this.Filter3OperatorComboBox.TabIndex = 39;
            // 
            // Filter3StatComboBox
            // 
            this.Filter3StatComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Filter3StatComboBox.Enabled = false;
            this.Filter3StatComboBox.FormattingEnabled = true;
            this.Filter3StatComboBox.Location = new System.Drawing.Point(66, 172);
            this.Filter3StatComboBox.Name = "Filter3StatComboBox";
            this.Filter3StatComboBox.Size = new System.Drawing.Size(157, 21);
            this.Filter3StatComboBox.TabIndex = 38;
            // 
            // Filter3CheckBox
            // 
            this.Filter3CheckBox.AutoSize = true;
            this.Filter3CheckBox.Location = new System.Drawing.Point(12, 174);
            this.Filter3CheckBox.Name = "Filter3CheckBox";
            this.Filter3CheckBox.Size = new System.Drawing.Size(48, 17);
            this.Filter3CheckBox.TabIndex = 37;
            this.Filter3CheckBox.Text = "Filter";
            this.Filter3CheckBox.UseVisualStyleBackColor = true;
            this.Filter3CheckBox.CheckedChanged += new System.EventHandler(this.Filter3CheckBox_CheckedChanged);
            // 
            // qualityLabel
            // 
            this.qualityLabel.AutoSize = true;
            this.qualityLabel.Location = new System.Drawing.Point(21, 239);
            this.qualityLabel.Name = "qualityLabel";
            this.qualityLabel.Size = new System.Drawing.Size(39, 13);
            this.qualityLabel.TabIndex = 45;
            this.qualityLabel.Text = "Quality";
            // 
            // qualityComboBox
            // 
            this.qualityComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.qualityComboBox.FormattingEnabled = true;
            this.qualityComboBox.Location = new System.Drawing.Point(66, 236);
            this.qualityComboBox.Name = "qualityComboBox";
            this.qualityComboBox.Size = new System.Drawing.Size(146, 21);
            this.qualityComboBox.TabIndex = 46;
            // 
            // maxButton
            // 
            this.maxButton.Location = new System.Drawing.Point(393, 116);
            this.maxButton.Name = "maxButton";
            this.maxButton.Size = new System.Drawing.Size(38, 23);
            this.maxButton.TabIndex = 47;
            this.maxButton.Text = "Max";
            this.maxButton.UseVisualStyleBackColor = true;
            this.maxButton.Click += new System.EventHandler(this.maxButton_Click);
            // 
            // requiredLevelLabel
            // 
            this.requiredLevelLabel.AutoSize = true;
            this.requiredLevelLabel.Location = new System.Drawing.Point(218, 239);
            this.requiredLevelLabel.Name = "requiredLevelLabel";
            this.requiredLevelLabel.Size = new System.Drawing.Size(79, 13);
            this.requiredLevelLabel.TabIndex = 48;
            this.requiredLevelLabel.Text = "Required Level";
            // 
            // hyphenLabel
            // 
            this.hyphenLabel.AutoSize = true;
            this.hyphenLabel.Location = new System.Drawing.Point(340, 239);
            this.hyphenLabel.Name = "hyphenLabel";
            this.hyphenLabel.Size = new System.Drawing.Size(10, 13);
            this.hyphenLabel.TabIndex = 49;
            this.hyphenLabel.Text = "-";
            // 
            // minLevelTextBox
            // 
            this.minLevelTextBox.Location = new System.Drawing.Point(303, 236);
            this.minLevelTextBox.Name = "minLevelTextBox";
            this.minLevelTextBox.Size = new System.Drawing.Size(31, 20);
            this.minLevelTextBox.TabIndex = 50;
            // 
            // maxLevelTextBox
            // 
            this.maxLevelTextBox.Location = new System.Drawing.Point(356, 237);
            this.maxLevelTextBox.Name = "maxLevelTextBox";
            this.maxLevelTextBox.Size = new System.Drawing.Size(31, 20);
            this.maxLevelTextBox.TabIndex = 51;
            // 
            // DebugButton
            // 
            this.DebugButton.Location = new System.Drawing.Point(221, 305);
            this.DebugButton.Name = "DebugButton";
            this.DebugButton.Size = new System.Drawing.Size(75, 23);
            this.DebugButton.TabIndex = 52;
            this.DebugButton.Text = "Debug";
            this.DebugButton.UseVisualStyleBackColor = true;
            this.DebugButton.Click += new System.EventHandler(this.DebugButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 442);
            this.Controls.Add(this.DebugButton);
            this.Controls.Add(this.maxLevelTextBox);
            this.Controls.Add(this.minLevelTextBox);
            this.Controls.Add(this.hyphenLabel);
            this.Controls.Add(this.requiredLevelLabel);
            this.Controls.Add(this.maxButton);
            this.Controls.Add(this.qualityComboBox);
            this.Controls.Add(this.qualityLabel);
            this.Controls.Add(this.Filter4TextBox);
            this.Controls.Add(this.Filter4OperatorComboBox);
            this.Controls.Add(this.Filter4StatComboBox);
            this.Controls.Add(this.Filter4CheckBox);
            this.Controls.Add(this.Filter3TextBox);
            this.Controls.Add(this.Filter3OperatorComboBox);
            this.Controls.Add(this.Filter3StatComboBox);
            this.Controls.Add(this.Filter3CheckBox);
            this.Controls.Add(this.Filter2TextBox);
            this.Controls.Add(this.Filter2OperatorComboBox);
            this.Controls.Add(this.Filter2StatComboBox);
            this.Controls.Add(this.Filter2CheckBox);
            this.Controls.Add(this.Filter1TextBox);
            this.Controls.Add(this.Filter1OperatorComboBox);
            this.Controls.Add(this.Filter1StatComboBox);
            this.Controls.Add(this.Filter1CheckBox);
            this.Controls.Add(this.DeleteAllButton);
            this.Controls.Add(this.FillDBButton);
            this.Controls.Add(this.slotComboBox);
            this.Controls.Add(this.TypeComboBox);
            this.Controls.Add(this.ItemsComboBox);
            this.Controls.Add(this.ResetButton);
            this.Controls.Add(this.DisplayButton);
            this.Controls.Add(this.SearchButton);
            this.Controls.Add(this.SearchTextBox);
            this.Controls.Add(this.PrintDBButton);
            this.Controls.Add(this.DeleteButton);
            this.Controls.Add(this.AddItemIDButton);
            this.Controls.Add(this.ItemNameTextBox);
            this.Controls.Add(this.DebugSearchButton);
            this.Name = "MainForm";
            this.Text = "WoW Item Database 1.12";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button DebugSearchButton;
        private System.Windows.Forms.TextBox ItemNameTextBox;
        private System.Windows.Forms.Button AddItemIDButton;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Button PrintDBButton;
        private System.Windows.Forms.TextBox SearchTextBox;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.Button DisplayButton;
        private System.Windows.Forms.Button ResetButton;
        private System.Windows.Forms.ComboBox ItemsComboBox;
        private System.Windows.Forms.ComboBox TypeComboBox;
        private System.Windows.Forms.ComboBox slotComboBox;
        private System.Windows.Forms.Button FillDBButton;
        private System.Windows.Forms.Button DeleteAllButton;
        private System.Windows.Forms.CheckBox Filter1CheckBox;
        private System.Windows.Forms.ComboBox Filter1StatComboBox;
        private System.Windows.Forms.ComboBox Filter1OperatorComboBox;
        private System.Windows.Forms.TextBox Filter1TextBox;
        private System.Windows.Forms.TextBox Filter2TextBox;
        private System.Windows.Forms.ComboBox Filter2OperatorComboBox;
        private System.Windows.Forms.ComboBox Filter2StatComboBox;
        private System.Windows.Forms.CheckBox Filter2CheckBox;
        private System.Windows.Forms.TextBox Filter4TextBox;
        private System.Windows.Forms.ComboBox Filter4OperatorComboBox;
        private System.Windows.Forms.ComboBox Filter4StatComboBox;
        private System.Windows.Forms.CheckBox Filter4CheckBox;
        private System.Windows.Forms.TextBox Filter3TextBox;
        private System.Windows.Forms.ComboBox Filter3OperatorComboBox;
        private System.Windows.Forms.ComboBox Filter3StatComboBox;
        private System.Windows.Forms.CheckBox Filter3CheckBox;
        private System.Windows.Forms.Label qualityLabel;
        private System.Windows.Forms.ComboBox qualityComboBox;
        private System.Windows.Forms.Button maxButton;
        private System.Windows.Forms.Label requiredLevelLabel;
        private System.Windows.Forms.Label hyphenLabel;
        private System.Windows.Forms.TextBox minLevelTextBox;
        private System.Windows.Forms.TextBox maxLevelTextBox;
        private System.Windows.Forms.Button DebugButton;
    }
}

