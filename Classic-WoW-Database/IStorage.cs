﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Classic_WoW_Database
{
    public interface IStorage
    {
        ObservableCollection<Item> SearchAllItemsByName(string _searchTerm);
        ObservableCollection<Item> SearchAllItemsById(int _id);
        ObservableCollection<Item> SearchByFilter(string _name, List<Filter> _filters, List<StatFilter> _stats, string _orderBy);
        ObservableCollection<Item> MaxStat(List<Filter> _filter, StatFilter _stat);
        List<Item> Debug();
    }
}
