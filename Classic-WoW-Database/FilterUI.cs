﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Classic_WoW_Database
{
    public class FilterUI : INotifyPropertyChanged
    {
        public event SelectedItemTypeChanged OnSelectedItemType;

        #region filter 1
        //filter 1, choose weapons or armour
        private List<string> itemsList = new List<string>();
        public List<string> ItemsList
        {
            get { return itemsList; }
            set
            {
                itemsList = value;
                //OnPropertyChanged("ItemsList");
            }
        }
        private string selectedItemList;
        public string SelectedItemList
        {
            get { return selectedItemList; }
            set
            {
                selectedItemList = value;
                OnPropertyChanged("SelectedItemList");
                SelectedItemTypeArgs e = new SelectedItemTypeArgs();
                switch (selectedItemList)
                {
                    case "Weapons":
                        ItemTypeList = weaponTypeList;
                        SelectedItemType = ItemTypeList[0];
                        ItemTypeListEnabled = true;
                        e.weaponSelected = true;
                        if (OnSelectedItemType != null)
                            OnSelectedItemType(this, e);
                        break;
                    case "Armour":
                        ItemTypeList = armourTypeList;
                        SelectedItemType = ItemTypeList[0];
                        ItemTypeListEnabled = true;
                        e.weaponSelected = false;
                        if (OnSelectedItemType != null)
                            OnSelectedItemType(this, e);
                        break;
                    default:
                        ItemTypeListEnabled = false;
                        ItemSlotListEnabled = false;
                        //ItemSlotList = new List<string>() { "Select" };
                        SelectedItemSlot = "Select";
                        SelectedItemType = "Select";
                        e.weaponSelected = null;
                        if(OnSelectedItemType != null)
                            OnSelectedItemType(this, e);
                        break;
                }
            }
        }
        #endregion

        #region filter 2
        private bool itemTypeListEnabled;
        public bool ItemTypeListEnabled
        {
            get { return itemTypeListEnabled; }
            set
            {
                itemTypeListEnabled = value;
                OnPropertyChanged("ItemTypeListEnabled");
            }
        }
        //dirrerent lists
        private List<string> armourTypeList = new List<string>();
        private List<string> weaponTypeList = new List<string>();

        //meta list
        private List<string> itemTypeList = new List<string>();
        public List<string> ItemTypeList
        {
            get { return itemTypeList; }
            set
            {
                itemTypeList = value;
                OnPropertyChanged("ItemTypeList");
            }
        }
        private string selectedItemType; //selected item data binding in combo box
        public string SelectedItemType
        {
            get { return selectedItemType; }
            set
            {
                selectedItemType = value;
                OnPropertyChanged("SelectedItemType");

                switch (selectedItemType)
                {
                    case "Cloth":
                    case "Leather":
                    case "Mail":
                    case "Plate":
                        ItemSlotList = armourSlotList;
                        SelectedItemSlot = ItemSlotList[0];
                        ItemSlotListEnabled = true;
                        break;
                    case "One-Handed":
                        ItemSlotList = oneHandTypeList;
                        SelectedItemSlot = ItemSlotList[0];
                        ItemSlotListEnabled = true;
                        break;
                    case "Two-Handed":
                        ItemSlotList = twoHandTypeList;
                        SelectedItemSlot = ItemSlotList[0];
                        ItemSlotListEnabled = true;
                        break;
                    case "Ranged":
                        ItemSlotList = rangedTypeList;
                        SelectedItemSlot = ItemSlotList[0];
                        ItemSlotListEnabled = true;
                        break;
                    default:
                        ItemSlotList = new List<string>();
                        ItemSlotListEnabled = false;
                        ItemSlotList = new List<string>() { "Select" };
                        SelectedItemSlot = ItemSlotList[0];
                        break;
                }
            }
        }
        #endregion

        #region filter 3
        private bool itemSlotListEnabled;
        public bool ItemSlotListEnabled
        {
            get { return itemSlotListEnabled; }
            set
            {
                itemSlotListEnabled = value;
                OnPropertyChanged("ItemSlotListEnabled");
            }
        }

        private List<string> armourSlotList = new List<string>();
        private List<string> oneHandTypeList = new List<string>();
        private List<string> twoHandTypeList = new List<string>();
        private List<string> rangedTypeList = new List<string>();
        private List<string> otherTypeList = new List<string>();

        private List<string> itemSlotList = new List<string>();
        public List<string> ItemSlotList
        {
            get { return itemSlotList; }
            set
            {
                itemSlotList = value;
                OnPropertyChanged("ItemSlotList");
            }
        }
        private string selectedItemSlot;
        public string SelectedItemSlot
        {
            get { return selectedItemSlot; }
            set
            {
                selectedItemSlot = value;
                OnPropertyChanged("SelectedItemSlot");
            }
        }
        #endregion

        private string numberOfFilters;
        public string NumberOfFilters
        {
            get { return numberOfFilters; }
            set
            {
                if (value.Length == 1)
                {
                    numberOfFilters = value;
                    int.TryParse(numberOfFilters, out numberOfFiltersInt);
                    //OnPropertyChanged("NumberOfFilters");
                }
            }
        }
        private int numberOfFiltersInt = 0;

        public FilterUI() //this has to be a method, not a constructor, since this method calls SelecteItedmList set method and that throws an event. On construction the event hasn't been subscribed to yet, and so is null
        {
            itemsList.Add("Select");
            itemsList.Add("Weapons");
            itemsList.Add("Armour");
            SelectedItemList = itemsList[0]; //set it to "Select" by default

            armourTypeList.Add("Select");
            armourTypeList.Add("Cloth");
            armourTypeList.Add("Leather");
            armourTypeList.Add("Mail");
            armourTypeList.Add("Plate");
            armourTypeList.Add("Amulet");
            armourTypeList.Add("Ring");
            armourTypeList.Add("Trinket");
            armourTypeList.Add("Shield");
            armourTypeList.Add("Cloak");
            armourTypeList.Add("OffHandFrill");
            armourTypeList.Add("Relic");
            armourTypeList.Add("Miscellaneous");

            weaponTypeList.Add("Select");
            weaponTypeList.Add("One-Handed");
            weaponTypeList.Add("Two-Handed");
            weaponTypeList.Add("Ranged");
            weaponTypeList.Add("Other");
            ItemTypeList = armourTypeList;
            SelectedItemType = itemTypeList[0];
            ItemTypeListEnabled = false;

            oneHandTypeList.Add("Select");
            oneHandTypeList.Add("Daggers");
            oneHandTypeList.Add("Fist Weapons");
            oneHandTypeList.Add("One-Handed Axes");
            oneHandTypeList.Add("One-Handed Maces");
            oneHandTypeList.Add("One-Handed Swords");

            twoHandTypeList.Add("Select");
            twoHandTypeList.Add("Polearms");
            twoHandTypeList.Add("Staves");
            twoHandTypeList.Add("Two-Handed Axes");
            twoHandTypeList.Add("Two-Handed Maces");
            twoHandTypeList.Add("Two-Handed Swords");
            twoHandTypeList.Add("Fishing Poles");

            rangedTypeList.Add("Select");
            rangedTypeList.Add("Bows");
            rangedTypeList.Add("Crossbows");
            rangedTypeList.Add("Guns");
            rangedTypeList.Add("Thrown");
            rangedTypeList.Add("Wands");

            otherTypeList.Add("Select");
            otherTypeList.Add("Other");

            armourSlotList.Add("Select");
            armourSlotList.Add("Chest");
            armourSlotList.Add("Feet");
            armourSlotList.Add("Hands");
            armourSlotList.Add("Head");
            armourSlotList.Add("Legs");
            armourSlotList.Add("Shoulder");
            armourSlotList.Add("Waist");
            armourSlotList.Add("Wrist");
            //itemSlotList = armourSlotList;
            //SelectedItemSlot = armourSlotList[0];
            ItemSlotListEnabled = false;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(
                    this, new PropertyChangedEventArgs(propName));
        }

        public delegate void SelectedItemTypeChanged(object sender, SelectedItemTypeArgs e);

        public class SelectedItemTypeArgs : EventArgs
        {
            public bool? weaponSelected;
        }
    }
}
