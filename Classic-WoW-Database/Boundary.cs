﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Classic_WoW_Database
{
    public class Boundary : IBoundary
    {
        private readonly IStorage db = new Database();

        public ObservableCollection<Item> SearchByName(string _name)
        {
            return db.SearchAllItemsByName(_name);
        }

        public ObservableCollection<Item> SearchByID(int _id)
        {
            return db.SearchAllItemsById(_id);
        }

        public ObservableCollection<Item> SearchByFilter(string _name, string _quality, int _minLevel, int _maxLevel, List<FilterUI> _filters, List<StatFilterUI> _stats, string _orderBy)
        {
            //conduct validation
            
            List<Filter> filters = new List<Filter>();
            List<StatFilter> statFilters = new List<StatFilter>();
            foreach (FilterUI iFilter in _filters)
            {
                bool? isWeapon = true; //get the ite type as nullable bool
                switch (iFilter.SelectedItemList)
                {
                    case "Armour":
                        isWeapon = false;
                        break;
                    case "Select":
                        isWeapon = null;
                        break;
                }//note we start with true so if the selected content is weapon then we're already done
                filters.Add(new Filter()
                {
                    IsWeapon = isWeapon,
                    ItemType = iFilter.SelectedItemType,
                    ItemSlot = iFilter.SelectedItemSlot,
                    MinLevel = _minLevel,
                    MaxLevel = _maxLevel,
                    Quality = _quality
                });
            }

            foreach (StatFilterUI iFilter in _stats)
            {
                statFilters.Add(new StatFilter{ stat = iFilter.SelectedStat, comparisonOperator = iFilter.SelectedOperator, value = iFilter.ComparativeValue });
            }

            return db.SearchByFilter(_name, filters, statFilters, _orderBy);
        }

        public ObservableCollection<Item> MaxStat(List<FilterUI> _filter, StatFilterUI _stat)
        {
            List<Filter> filters = new List<Filter>();

            foreach (var filter in _filter)
            {
                bool? isWeapon = true; //get the ite type as nullable bool
                switch (filter.SelectedItemList)
                {
                    case "Armour":
                        isWeapon = false;
                        break;
                    case "Select":
                        isWeapon = null;
                        break;
                }//note we start with true so if the selected content is weapon then we're already done

                filters.Add(new Filter { IsWeapon = isWeapon, ItemSlot = filter.SelectedItemSlot, ItemType = filter.SelectedItemType });
            }
            //Filter filter = new Filter{ IsWeapon = isWeapon, ItemSlot = _filter.SelectedItemSlot, ItemType = _filter.SelectedItemType };
            StatFilter stat = new StatFilter {stat = _stat.SelectedStat};

            return db.MaxStat(filters, stat);
        }

        public bool AddItemByID(int _id)
        {
            return false;
        }

        public List<Item> Debug()
        {
            return db.Debug();
        }
    }
}
