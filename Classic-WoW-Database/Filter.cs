﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Classic_WoW_Database
{
    public class Filter
    {       
        public bool? IsWeapon { get; set; }
        public string ItemType { get; set; }
        public string ItemSlot { get; set; }

        public string Quality { get; set; }
        public int MinLevel { get; set; }
        public int MaxLevel { get; set; }
    }
}
