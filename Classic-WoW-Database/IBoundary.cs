﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Classic_WoW_Database
{
    public interface IBoundary
    {
        ObservableCollection<Item> SearchByName(string _name);
        ObservableCollection<Item> SearchByID(int _id);

        ObservableCollection<Item> SearchByFilter(string _name, string _quality, int _minLevel, int _maxLevel, List<FilterUI> _filters, List<StatFilterUI> _stats, string _orderBy);
        ObservableCollection<Item> MaxStat(List<FilterUI> _filter, StatFilterUI _stat);

        bool AddItemByID(int _id);

        List<Item> Debug();
    }
}
