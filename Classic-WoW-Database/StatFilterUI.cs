﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Classic_WoW_Database
{
    public class StatFilterUI : INotifyPropertyChanged
    {
        public event StatSelected OnStatSelected;

        private ObservableCollection<string> statList = new ObservableCollection<string>();
        public ObservableCollection<string> StatList
        {
            get { return statList; }
            set
            {
                statList = value;
                OnPropertyChanged("StatList");
            }
        }
        private string selectedStat;
        public string SelectedStat
        {
            get { return selectedStat; }
            set
            {
                string oldSelectedStat = selectedStat;
                selectedStat = value;
                OnPropertyChanged("SelectedStat");

                if (value == "Select") //if the user is changing to select, then remove the last one
                {
                    EnabledOperatorList = false;
                    SelectedOperator = ">";
                    ComparitiveValueTextBoxEnabled = false;
                    ComparativeValue = 0;
                    SelectedEventArgs e = new SelectedEventArgs();
                    e.add = false;
                    OnStatSelected(this, e);
                }
                else if (oldSelectedStat == "Select") //changing from select to a stat, so add a stat filter
                {
                    EnabledOperatorList = true;
                    ComparitiveValueTextBoxEnabled = true;
                    SelectedEventArgs e = new SelectedEventArgs();
                    e.add = true;
                    OnStatSelected(this, e);
                }
                //just chaging a stat filter, do nothing
            }
        }
        //private bool enabledStatList;
        //public bool EnabledStatList
        //{
        //    get { return enabledStatList; }
        //    set
        //    {
        //        enabledStatList = value;
        //        OnPropertyChanged("EnabledStatList");
        //    }
        //}

        private List<string> operatorList = new List<string>();
        public List<string> OperatorList
        {
            get { return operatorList; }
            set { operatorList = value; }
        }
        private string selectedOperator;
        public string SelectedOperator
        {
            get { return selectedOperator; }
            set
            {
                selectedOperator = value;
                OnPropertyChanged("SelectedOperator");
            }
        }
        private bool enabledOperatorList;
        public bool EnabledOperatorList
        {
            get { return enabledOperatorList; }
            set
            {
                enabledOperatorList = value;
                OnPropertyChanged("EnabledOperatorList");
            }
        }

        private int comparativeValue;
        public int ComparativeValue
        {
            get { return comparativeValue; }
            set
            {
                if (value >= 0 && value < 1000)
                {
                    comparativeValue = value;
                    OnPropertyChanged("ComparativeValue");
                }
            }
        }
        private bool comparitiveValueTextBoxEnabled;
        public bool ComparitiveValueTextBoxEnabled
        {
            get { return comparitiveValueTextBoxEnabled; }
            set
            {
                comparitiveValueTextBoxEnabled = value;
                OnPropertyChanged("ComparitiveValueTextBoxEnabled");
            }
        }

        private bool setWeaponSelected;
        public bool SetWeaponSelected
        {
            get { return setWeaponSelected; }
            set
            {
                if (!setWeaponSelected && value)
                    StatList.Insert(1, "DPS");
                else if (setWeaponSelected && !value)
                {
                    StatList.Remove("DPS");

                    if (SelectedStat == null)
                        SelectedStat = "Select";
                }

                setWeaponSelected = value;
            }
        }

        public StatFilterUI()
        {
            operatorList.Add(">");
            operatorList.Add("<");
            operatorList.Add("=");

            statList.Add("Select");
            statList.Add("Agility");
            statList.Add("Intellect");
            statList.Add("Stamina");
            statList.Add("Spirit");
            statList.Add("Strength");
            statList.Add("Mana per 5");
            statList.Add("Health per 5");
            statList.Add("Armour");
            statList.Add("Dodge");
            statList.Add("Parry");
            statList.Add("Defence");
            statList.Add("Block Chance");
            statList.Add("Block Value");
            statList.Add("Increased Axes");
            statList.Add("Increased Daggers");
            statList.Add("Increased Swords");
            statList.Add("Hit");
            statList.Add("Critical");
            statList.Add("Attack Power");
            statList.Add("Ranged Attack Power");
            statList.Add("Feral Attack Power");
            statList.Add("Spell Hit");
            statList.Add("Spell Critical");
            statList.Add("Spell Damage");
            statList.Add("Spell Healing");
            statList.Add("Spell Hit");
            statList.Add("Resistance Decrease");
            statList.Add("Arcane Damage");
            statList.Add("Fire Damage");
            statList.Add("Frost Damage");
            statList.Add("Nature Damage");
            statList.Add("Shadow Damage");
            statList.Add("Arcane Resistance");
            statList.Add("Fire Resistance");
            statList.Add("Frost Resistance");
            statList.Add("Nature Resistance");
            statList.Add("Shadow Resistance");

            SelectedOperator = ">";
            selectedStat = "Select"; //uses the lowercase version to prevent triggeting the set method on initiation of the control
            EnabledOperatorList = false;
            ComparitiveValueTextBoxEnabled = false;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(
                    this, new PropertyChangedEventArgs(propName));
        }
    }

    public delegate void StatSelected(object sender, SelectedEventArgs e);

    public class SelectedEventArgs : EventArgs
    {
        public bool add;
    }
}
