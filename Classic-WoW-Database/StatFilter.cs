﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Classic_WoW_Database
{
    public class StatFilter
    {
        public string comparisonOperator { get; set; }
        public string stat { get; set; }
        public int value { get; set; }
    }
}
