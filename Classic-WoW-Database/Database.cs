﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;

namespace Classic_WoW_Database
{
    public class Database : IStorage
    {
        private const string SELECT_FROM_ITEMS =
            "SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items ";

        private const string SELECT_FROM_WEAPONS =
            "SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, AttackPower, RangedAttackPower, FeralAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, WeaponType, Slot, DPS, DamageLow, DamageHigh, Speed, ExtraDamage, ChanceOnHit, WandDamage FROM Weapons ";
        public List<Item> ListWeapons()
        {
            List<Item> items = new List<Item>();

            SQLiteConnection mySQLConnection = new SQLiteConnection(Classic_WoW_Database.Properties.Settings.Default.ItemsConnectionString);
            try
            {
                mySQLConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            SQLiteCommand mycommand = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, AttackPower, RangedAttackPower, FeralAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, WeaponType, Slot, DPS, DamageLow, DamageHigh, Speed, ExtraDamage, ChanceOnHit, WandDamage FROM Weapons", mySQLConnection);

            SQLiteDataReader myReader = mycommand.ExecuteReader();

            if (myReader.HasRows)
            {
                while (myReader.Read()) //for each row
                {
                    //create item
                    Item theItem = new Item
                        (
                        true,
                        myReader.GetInt32(0),//ItemID
                        myReader.GetString(1),//Name
                        myReader.GetInt32(2),//RequiredLevel
                        myReader.GetString(3),//OtherRequirements
                        myReader.GetString(4),//OtherRequirements2
                        myReader.GetBoolean(5),//BoE
                        myReader.GetBoolean(6),//BoP
                        myReader.GetBoolean(7),//QuestItem
                        myReader.GetBoolean(8),//IsUnique
                        myReader.GetString(9),//Quality
                        myReader.GetInt32(10),//Armour
                        myReader.GetInt32(11),//Durability
                        myReader.GetBoolean(12),//RandomBonuses
                        myReader.GetInt32(13),//Agility
                        myReader.GetInt32(14),//Intellect
                        myReader.GetInt32(15),//Spirit
                        myReader.GetInt32(16),//Stamina
                        myReader.GetInt32(17),//Strength
                        myReader.GetInt32(18),//IncreacedDaggers
                        myReader.GetInt32(19),//IncreacedSwords
                        myReader.GetInt32(20),//IncreacedAxes
                        myReader.GetInt32(21),//ManaPer5
                        myReader.GetInt32(22),//HealthPer5
                        myReader.GetInt32(23),//Hit
                        myReader.GetInt32(24),//SpellHit
                        myReader.GetInt32(25),//Critical
                        myReader.GetInt32(26),//SpellCritical
                        myReader.GetInt32(27),//Dodge
                        myReader.GetInt32(28),//Parry
                        myReader.GetInt32(29),//Defence
                        myReader.GetInt32(30),//AttackPower
                        myReader.GetInt32(31),//RangedAttackPower
                        myReader.GetInt32(32),//FeralAttackPower
                        myReader.GetInt32(33),//SpellDamage
                        myReader.GetInt32(34),//SpellHealing
                        myReader.GetInt32(35),//ResistanceDecreace
                        myReader.GetInt32(36),//ArcaneDamage
                        myReader.GetInt32(37),//ShadowDamage
                        myReader.GetInt32(38),//FireDamage
                        myReader.GetInt32(39),//NatureDamage
                        myReader.GetInt32(40),//FrostDamage
                        myReader.GetInt32(41),//NatureResistance
                        myReader.GetInt32(42),//FireResistance
                        myReader.GetInt32(43),//ShadowResistance
                        myReader.GetInt32(44),//FrostResistance
                        myReader.GetInt32(45),//ArcaneResistance
                        myReader.GetString(46),//MiscEquip
                        myReader.GetString(47),//OnUse
                        myReader.GetString(48),//YellowText
                        myReader.GetString(49),//WeaponType
                        myReader.GetString(50),//Slot
                        (float)myReader.GetDouble(51),//DPS
                        (float)myReader.GetDouble(52),//DamageLow
                        (float)myReader.GetDouble(53),//DamageHigh
                        (float)myReader.GetDouble(54),//Speed
                        myReader.GetString(55),//ExtraDamage
                        myReader.GetString(56),//ChanceOnHit
                        myReader.GetString(57)//WandDamage
                        );
                    items.Add(theItem);
                }
                myReader.Close();
            }

            mySQLConnection.Close();

            return items;
        }

        //remember to include a leading space before the WHERe or the command won't work
        public List<Item> ListAllArmour(string filter)
        {
            //no where clause selecting types so we need to say FROM items WHERE ... not FROM items AND ...
            //string newFilter = filter.Replace("AND", "WHERE");
            return ListArmour("", filter);
        }

        public List<Item> ListClothArmour(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Cloth' AND Slot != 'Back'", filter);
        }

        public List<Item> ListLeatherArmour(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Leather'", filter);
        }

        public List<Item> ListMailArmour(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Mail'", filter);
        }

        public List<Item> ListPlateArmour(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Plate'", filter);
        }

        public List<Item> ListAmuletArmour(string filter)
        {
            return ListArmour(" WHERE Slot = 'Neck'", filter);
        }

        public List<Item> ListRingArmour(string filter)
        {
            return ListArmour(" WHERE Slot = 'Finger'", filter);
        }

        public List<Item> ListTrinketArmour(string filter)
        {
            return ListArmour(" WHERE Slot = 'Trinket'", filter);
        }

        public List<Item> ListCloakArmour(string filter)
        {
            return ListArmour(" WHERE Slot = 'Back'", filter);
        }

        public List<Item> ListOffHandFrillArmour(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'OffHandFrill'", filter);
        }

        public List<Item> ListShieldArmour(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Shield'", filter);
        }

        public List<Item> ListMiscellaneousArmour(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Miscellaneous'", filter);
        }


        #region cloth
        public List<Item> ListClothChest(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Cloth' AND Slot = 'Chest'", filter);
        }

        public List<Item> ListClothFeet(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Cloth' AND Slot = 'Feet'", filter);
        }

        public List<Item> ListClothHands(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Cloth' AND Slot = 'Hands'", filter);
        }

        public List<Item> ListClothHead(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Cloth' AND Slot = 'Head'", filter);
        }

        public List<Item> ListClothLeg(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Cloth' AND Slot = 'Legs'", filter);
        }

        public List<Item> ListClothShoulder(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Cloth' AND Slot = 'Shoulder'", filter);
        }

        public List<Item> ListClothWaist(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Cloth' AND Slot = 'Waist'", filter);
        }

        public List<Item> ListClothWrist(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Cloth' AND Slot = 'Wrist'", filter);
        }
        #endregion

        #region leather
        public List<Item> ListLeatherChest(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Leather' AND Slot = 'Chest'", filter);
        }

        public List<Item> ListLeatherFeet(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Leather' AND Slot = 'Feet'", filter);
        }

        public List<Item> ListLeatherHands(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Leather' AND Slot = 'Hands'", filter);
        }

        public List<Item> ListLeatherHead(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Leather' AND Slot = 'Head'", filter);
        }

        public List<Item> ListLeatherLeg(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Leather' AND Slot = 'Legs'", filter);
        }

        public List<Item> ListLeatherShoulder(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Leather' AND Slot = 'Shoulder'", filter);
        }

        public List<Item> ListLeatherWaist(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Leather' AND Slot = 'Waist'", filter);
        }

        public List<Item> ListLeatherWrist(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Leather' AND Slot = 'Wrist'", filter);
        }
        #endregion

        #region mail
        public List<Item> ListMailChest(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Mail' AND Slot = 'Chest'", filter);
        }

        public List<Item> ListMailFeet(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Mail' AND Slot = 'Feet'", filter);
        }

        public List<Item> ListMailHands(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Mail' AND Slot = 'Hands'", filter);
        }

        public List<Item> ListMailHead(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Mail' AND Slot = 'Head'", filter);
        }

        public List<Item> ListMailLeg(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Mail' AND Slot = 'Legs'", filter);
        }

        public List<Item> ListMailShoulder(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Mail' AND Slot = 'Shoulder'", filter);
        }

        public List<Item> ListMailWaist(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Mail' AND Slot = 'Waist'", filter);
        }

        public List<Item> ListMailWrist(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Mail' AND Slot = 'Wrist'", filter);
        }
        #endregion

        #region plate
        public List<Item> ListPlateChest(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Plate' AND Slot = 'Chest'", filter);
        }

        public List<Item> ListPlateFeet(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Plate' AND Slot = 'Feet'", filter);
        }

        public List<Item> ListPlateHands(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Plate' AND Slot = 'Hands'", filter);
        }

        public List<Item> ListPlateHead(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Plate' AND Slot = 'Head'", filter);
        }

        public List<Item> ListPlateLeg(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Plate' AND Slot = 'Legs'", filter);
        }

        public List<Item> ListPlateShoulder(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Plate' AND Slot = 'Shoulder'", filter);
        }

        public List<Item> ListPlateWaist(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Plate' AND Slot = 'Waist'", filter);
        }

        public List<Item> ListPlateWrist(string filter)
        {
            return ListArmour(" WHERE ArmourType = 'Plate' AND Slot = 'Wrist'", filter);
        }
        #endregion

        #region weapons
        public List<Item> ListAllWeapons(string filter)
        {
            return ListWeapons("", filter);
        }

        public List<Item> ListOneHandedWeapons(string filter)
        {
            return ListWeapons(" WHERE Slot = 'OneHand'", filter);
            //return Debug("", filter);
        }
        public List<Item> ListTwoHandedWeapons(string filter)
        {
            return ListWeapons(" WHERE Slot = 'TwoHand'", filter);
        }
        public List<Item> ListRangedWeapons(string filter)
        {
            return ListWeapons(" WHERE Slot = 'Ranged'", filter);
        }

        public List<Item> ListOneHandedSwords(string filter)
        {
            return ListWeapons(" WHERE Slot = 'OneHand' AND WeaponType='Sword'", filter);
        }
        public List<Item> ListOneHandedAxes(string filter)
        {
            return ListWeapons(" WHERE Slot = 'OneHand' AND WeaponType='Axe'", filter);
        }
        public List<Item> ListOneHandedMaces(string filter)
        {
            return ListWeapons(" WHERE Slot = 'OneHand' AND WeaponType='Mace'", filter);
        }
        public List<Item> ListDaggers(string filter)
        {
            return ListWeapons(" WHERE Slot = 'OneHand' AND WeaponType='Dagger'", filter);
        }
        public List<Item> ListFistWeapons(string filter)
        {
            return ListWeapons(" WHERE Slot = 'OneHand' AND WeaponType='Fist'", filter);
        }
        public List<Item> ListPolearms(string filter)
        {
            return ListWeapons(" WHERE Slot = 'TwoHand' AND WeaponType='Polearm'", filter);
        }
        public List<Item> ListStaffs(string filter)
        {
            return ListWeapons(" WHERE Slot = 'TwoHand' AND WeaponType='Staff'", filter);
        }
        public List<Item> ListTwoHandedSwords(string filter)
        {
            return ListWeapons(" WHERE Slot = 'TwoHand' AND WeaponType='Sword'", filter);
        }
        public List<Item> ListTwoHandedAxes(string filter)
        {
            return ListWeapons(" WHERE Slot = 'TwoHand' AND WeaponType='Axe'", filter);
        }
        public List<Item> ListTwoHandedMaces(string filter)
        {
            return ListWeapons(" WHERE Slot = 'TwoHand' AND WeaponType='Mace'", filter);
        }
        public List<Item> ListFishingPoles(string filter)
        {
            return ListWeapons(" WHERE Slot = 'TwoHand' AND WeaponType='FishingPole'", filter);
        }

        public List<Item> ListCrossbows(string filter)
        {
            return ListWeapons(" WHERE Slot = 'Ranged' AND WeaponType='Crossbow'", filter);
        }
        public List<Item> ListGuns(string filter)
        {
            return ListWeapons(" WHERE Slot = 'Ranged' AND WeaponType='Gun'", filter);
        }
        public List<Item> ListBows(string filter)
        {
            return ListWeapons(" WHERE Slot = 'Ranged' AND WeaponType='Bow'", filter);
        }
        public List<Item> ListWands(string filter)
        {
            return ListWeapons(" WHERE Slot = 'Ranged' AND WeaponType='Wand'", filter);
        }
        public List<Item> ListThrown(string filter)
        {
            return ListWeapons(" WHERE Slot = 'Ranged' AND WeaponType='Thrown'", filter);
        }

        public List<Item> ListMiscellaneous(string filter)
        {
            return ListWeapons(" WHERE WeaponType = 'Miscellaneous'", filter);
        }
        #endregion

        /// <summary>
        /// This is private so that someone cannot pass false string values into it
        /// The other methods must be called which are public and in turn call this method
        /// </summary>
        /// <param name="searchSet"></param>
        /// <returns></returns>
        public List<Item> ListArmour(string search, string filter)
        {
            //check to see if we are trying to do an AND without a WHERE, if so replace
            if (search == string.Empty && Regex.IsMatch(filter, " AND ."))
            {
                filter = filter.Remove(0, 4);
                filter = " WHERE" + filter;
            }

            SQLiteConnection mySQLConnection = new SQLiteConnection(Classic_WoW_Database.Properties.Settings.Default.ItemsConnectionString);
            try
            {
                mySQLConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            SQLiteDataReader myReader = null;
            SQLiteCommand myCommandRead = null;

            myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items" + search + filter, mySQLConnection); //get all columns

            myReader = myCommandRead.ExecuteReader(); //set the reader //error here if invalid item passed in (nul refrence)

            List<Item> items = new List<Item>(); //list of items found

            if (myReader.HasRows) //item search found something
            {
                while (myReader.Read()) //for each row
                {
                    //cerate an item
                    Item theItem = new Item
                        (
                        false,
                        myReader.GetInt32(0),//ItemID
                        myReader.GetString(1),//Name
                        myReader.GetInt32(2),//RequiredLevel
                        myReader.GetString(3),//OtherRequirements
                        myReader.GetString(4),//OtherRequirements2
                        myReader.GetBoolean(5),//BoE
                        myReader.GetBoolean(6),//BoP
                        myReader.GetBoolean(7),//QuestItem
                        myReader.GetBoolean(8),//IsUnique
                        myReader.GetBoolean(9),//BeginQuest
                        myReader.GetString(10),//Quality
                        myReader.GetInt32(11),//Armour
                        myReader.GetInt32(12),//Durability
                        myReader.GetBoolean(13),//RandomBonuses
                        myReader.GetInt32(14),//Agility
                        myReader.GetInt32(15),//Intellect
                        myReader.GetInt32(16),//Spirit
                        myReader.GetInt32(17),//Stamina
                        myReader.GetInt32(18),//Strength
                        myReader.GetInt32(19),//IncreacedDaggers
                        myReader.GetInt32(20),//IncreacedSwords
                        myReader.GetInt32(21),//IncreacedAxes
                        myReader.GetInt32(22),//ManaPer5
                        myReader.GetInt32(23),//HealthPer5
                        myReader.GetInt32(24),//Hit
                        myReader.GetInt32(25),//SpellHit
                        myReader.GetInt32(26),//Critical
                        myReader.GetInt32(27),//SpellCritical
                        myReader.GetInt32(28),//Dodge
                        myReader.GetInt32(29),//Parry
                        myReader.GetInt32(30),//Defence
                        myReader.GetInt32(31),//BaseBlock
                        myReader.GetInt32(32),//BlockValue
                        myReader.GetInt32(33),//BlockPercent
                        myReader.GetInt32(34),//AttackPower
                        myReader.GetInt32(35),//RangedAttackPower
                        myReader.GetInt32(36),//SpellDamage
                        myReader.GetInt32(37),//SpellHealing
                        myReader.GetInt32(38),//ResistanceDecreace
                        myReader.GetInt32(39),//ArcaneDamage
                        myReader.GetInt32(40),//ShadowDamage
                        myReader.GetInt32(41),//FireDamage
                        myReader.GetInt32(42),//NatureDamage
                        myReader.GetInt32(43),//FrostDamage
                        myReader.GetInt32(44),//NatureResistance
                        myReader.GetInt32(45),//FireResistance
                        myReader.GetInt32(46),//ShadowResistance
                        myReader.GetInt32(47),//FrostResistance
                        myReader.GetInt32(48),//ArcaneResistance
                        myReader.GetString(49),//MiscEquip
                        myReader.GetString(50),//OnUse
                        myReader.GetString(51),//YellowText
                        myReader.GetString(52),//ArmourType
                        myReader.GetString(53)//Slot
                        );
                    //Console.WriteLine("\"" + myReader[52].ToString() + "\"");
                    items.Add(theItem); //add to list
                }
            }

            myReader.Close();
            mySQLConnection.Close();

            return items;
        }

        public List<Item> ListWeapons(string search, string filter)
        {
            //check to see if we are trying to do an AND without a WHERE, if so replace
            if (search == string.Empty && Regex.IsMatch(filter, " AND ."))
            {
                filter = filter.Remove(0, 4);
                filter = " WHERE" + filter;
            }

            SQLiteConnection mySQLConnection = new SQLiteConnection(Classic_WoW_Database.Properties.Settings.Default.ItemsConnectionString);
            try
            {
                mySQLConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            SQLiteDataReader myReader = null;
            SQLiteCommand myCommandRead = null;

            myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, AttackPower, RangedAttackPower, FeralAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, WeaponType, Slot, DPS, DamageLow, DamageHigh, Speed, ExtraDamage, ChanceOnHit, WandDamage FROM Weapons" + search + filter, mySQLConnection); //get all columns

            myReader = myCommandRead.ExecuteReader(); //set the reader //error here if invalid item passed in (nul refrence)

            List<Item> items = new List<Item>(); //list of items found

            if (myReader.HasRows) //item search found something
            {
                while (myReader.Read()) //for each row
                {
                    //cerate an item
                    Item theItem = new Item
                        (
                        true,
                        myReader.GetInt32(0),//ItemID
                        myReader.GetString(1),//Name
                        myReader.GetInt32(2),//RequiredLevel
                        myReader.GetString(3),//OtherRequirements
                        myReader.GetString(4),//OtherRequirements2
                        myReader.GetBoolean(5),//BoE
                        myReader.GetBoolean(6),//BoP
                        myReader.GetBoolean(7),//QuestItem
                        myReader.GetBoolean(8),//IsUnique
                        myReader.GetString(9),//Quality
                        myReader.GetInt32(10),//Armour
                        myReader.GetInt32(11),//Durability
                        myReader.GetBoolean(12),//RandomBonuses
                        myReader.GetInt32(13),//Agility
                        myReader.GetInt32(14),//Intellect
                        myReader.GetInt32(15),//Spirit
                        myReader.GetInt32(16),//Stamina
                        myReader.GetInt32(17),//Strength
                        myReader.GetInt32(18),//IncreacedDaggers
                        myReader.GetInt32(19),//IncreacedSwords
                        myReader.GetInt32(20),//IncreacedAxes
                        myReader.GetInt32(21),//ManaPer5
                        myReader.GetInt32(22),//HealthPer5
                        myReader.GetInt32(23),//Hit
                        myReader.GetInt32(24),//SpellHit
                        myReader.GetInt32(25),//Critical
                        myReader.GetInt32(26),//SpellCritical
                        myReader.GetInt32(27),//Dodge
                        myReader.GetInt32(28),//Parry
                        myReader.GetInt32(29),//Defence
                        myReader.GetInt32(30),//AttackPower
                        myReader.GetInt32(31),//RangedAttackPower
                        myReader.GetInt32(32),//FeralAttackPower
                        myReader.GetInt32(33),//SpellDamage
                        myReader.GetInt32(34),//SpellHealing
                        myReader.GetInt32(35),//ResistanceDecreace
                        myReader.GetInt32(36),//ArcaneDamage
                        myReader.GetInt32(37),//ShadowDamage
                        myReader.GetInt32(38),//FireDamage
                        myReader.GetInt32(39),//NatureDamage
                        myReader.GetInt32(40),//FrostDamage
                        myReader.GetInt32(41),//NatureResistance
                        myReader.GetInt32(42),//FireResistance
                        myReader.GetInt32(43),//ShadowResistance
                        myReader.GetInt32(44),//FrostResistance
                        myReader.GetInt32(45),//ArcaneResistance
                        myReader.GetString(46),//MiscEquip
                        myReader.GetString(47),//OnUse
                        myReader.GetString(48),//YellowText
                        myReader.GetString(49),//WeaponType
                        myReader.GetString(50),//Slot
                        (float)myReader.GetDouble(51),//DPS
                        (float)myReader.GetDouble(52),//DamageLow
                        (float)myReader.GetDouble(53),//DamageHigh
                        (float)myReader.GetDouble(54),//Speed
                        myReader.GetString(55),//ExtraDamage
                        myReader.GetString(56),//ChanceOnHit
                        myReader.GetString(57)//WandDamage
                        );
                    items.Add(theItem);
                }
            }

            myReader.Close();
            mySQLConnection.Close();

            return items;
        }

        public List<Item> Debug()
        {
            SQLiteConnection mySQLConnection = new SQLiteConnection(Classic_WoW_Database.Properties.Settings.Default.ItemsConnectionString);
            try
            {
                mySQLConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            SQLiteDataReader myReader = null;
            SQLiteCommand myCommandRead = null;

            myCommandRead = new SQLiteCommand("SELECT Name, ItemID from Items where OnUse != ''", mySQLConnection); //get all columns

            myReader = myCommandRead.ExecuteReader(); //set the reader //error here if invalid item passed in (nul refrence)

            List<Item> items = new List<Item>(); //list of items found

            TextWriter tw = new StreamWriter("C:\\armour.txt");
            
            if (myReader.HasRows) //item search found something
            {
                while (myReader.Read()) //for each row
                {
                    Console.WriteLine(myReader[0] + " " + myReader[1]);
                    tw.WriteLine(myReader[0] + " (" + myReader[1]+ ")");
                }
            }

            tw.Close();
            myReader.Close();
            mySQLConnection.Close();

            return null;
        }

        /// <summary>
        /// Searches both the weapons and items tables for a given name, name does not need to be specific, partials are ok
        /// </summary>
        /// <param name="_searchTerm">partial name to search for</param>
        /// <returns>returns a list of item objects that were found</returns>
        public ObservableCollection<Item> SearchAllItemsByName(string _searchTerm)
        {
            return SearchAllItems("WHERE Name LIKE '%" + _searchTerm + "%'", "WHERE Name LIKE '%" + _searchTerm + "%'");
        }

        public ObservableCollection<Item> SearchAllItemsById(int _id)
        {
            return SearchAllItems("WHERE ItemID = " + _id, "WHERE ItemID = " + _id);
        }


        private static ObservableCollection<Item> SearchAllItems(string _armourQuery, string _weaponQuery)
        {
            if (_armourQuery == " WHERE 1=1 ")
            {
                _armourQuery = "";
            }
            if (_weaponQuery == " WHERE 1=1 ")
            {
                _weaponQuery = "";
            }

            if (_armourQuery != "")
            {
                _armourQuery = SELECT_FROM_ITEMS + _armourQuery;
            }
            if (_weaponQuery != "")
            {
                _weaponQuery = SELECT_FROM_WEAPONS + _weaponQuery;
            }

            Console.WriteLine("\n\n" + _armourQuery);

            SQLiteConnection mySQLConnection = new SQLiteConnection(Properties.Settings.Default.ItemsConnectionString);
            try
            {
                mySQLConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            SQLiteDataReader myReader = null;
            SQLiteCommand myCommandRead;
            ObservableCollection<Item> items = new ObservableCollection<Item>(); //list of items found

            if (_armourQuery != "")
            {
                myCommandRead = new SQLiteCommand(_armourQuery, mySQLConnection); //get all columns where item = name passed in
                myReader = myCommandRead.ExecuteReader(); //set the reader

                if (myReader.HasRows) //item search found something
                {
                    while (myReader.Read()) //for each row
                    {
                        //cerate an item
                        Item theItem = new Item
                            (
                            false,
                            myReader.GetInt32(0), //ItemID
                            myReader.GetString(1), //Name
                            myReader.GetInt32(2), //RequiredLevel
                            myReader.GetString(3), //OtherRequirements
                            myReader.GetString(4), //OtherRequirements2
                            myReader.GetBoolean(5), //BoE
                            myReader.GetBoolean(6), //BoP
                            myReader.GetBoolean(7), //QuestItem
                            myReader.GetBoolean(8), //IsUnique
                            myReader.GetBoolean(9), //BeginQuest
                            myReader.GetString(10), //Quality
                            myReader.GetInt32(11), //Armour
                            myReader.GetInt32(12), //Durability
                            myReader.GetBoolean(13), //RandomBonuses
                            myReader.GetInt32(14), //Agility
                            myReader.GetInt32(15), //Intellect
                            myReader.GetInt32(16), //Spirit
                            myReader.GetInt32(17), //Stamina
                            myReader.GetInt32(18), //Strength
                            myReader.GetInt32(19), //IncreacedDaggers
                            myReader.GetInt32(20), //IncreacedSwords
                            myReader.GetInt32(21), //IncreacedAxes
                            myReader.GetInt32(22), //ManaPer5
                            myReader.GetInt32(23), //HealthPer5
                            myReader.GetInt32(24), //Hit
                            myReader.GetInt32(25), //SpellHit
                            myReader.GetInt32(26), //Critical
                            myReader.GetInt32(27), //SpellCritical
                            myReader.GetInt32(28), //Dodge
                            myReader.GetInt32(29), //Parry
                            myReader.GetInt32(30), //Defence
                            myReader.GetInt32(31), //BaseBlock
                            myReader.GetInt32(32), //BlockValue
                            myReader.GetInt32(33), //BlockPercent
                            myReader.GetInt32(34), //AttackPower
                            myReader.GetInt32(35), //RangedAttackPower
                            myReader.GetInt32(36), //SpellDamage
                            myReader.GetInt32(37), //SpellHealing
                            myReader.GetInt32(38), //ResistanceDecreace
                            myReader.GetInt32(39), //ArcaneDamage
                            myReader.GetInt32(40), //ShadowDamage
                            myReader.GetInt32(41), //FireDamage
                            myReader.GetInt32(42), //NatureDamage
                            myReader.GetInt32(43), //FrostDamage
                            myReader.GetInt32(44), //NatureResistance
                            myReader.GetInt32(45), //FireResistance
                            myReader.GetInt32(46), //ShadowResistance
                            myReader.GetInt32(47), //FrostResistance
                            myReader.GetInt32(48), //ArcaneResistance
                            myReader.GetString(49), //MiscEquip
                            myReader.GetString(50), //OnUse
                            myReader.GetString(51), //YellowText
                            myReader.GetString(52), //ArmourType
                            myReader.GetString(53) //Slot]"
                            );
                        items.Add(theItem); //add to list
                    }
                }

                myReader.Close(); //close reader ready for next search 
            }

            if (_weaponQuery != "")
            {
                //weapon search
                myCommandRead = new SQLiteCommand(_weaponQuery, mySQLConnection); //get all columns where item = name passed in
                myReader = myCommandRead.ExecuteReader(); //set the reader

                if (myReader.HasRows)
                {
                    while (myReader.Read()) //for each row
                    {
                        //create item
                        Item theItem = new Item
                            (
                            true,
                            myReader.GetInt32(0), //ItemID
                            myReader.GetString(1), //Name
                            myReader.GetInt32(2), //RequiredLevel
                            myReader.GetString(3), //OtherRequirements
                            myReader.GetString(4), //OtherRequirements2
                            myReader.GetBoolean(5), //BoE
                            myReader.GetBoolean(6), //BoP
                            myReader.GetBoolean(7), //QuestItem
                            myReader.GetBoolean(8), //IsUnique
                            myReader.GetString(9), //Quality
                            myReader.GetInt32(10), //Armour
                            myReader.GetInt32(11), //Durability
                            myReader.GetBoolean(12), //RandomBonuses
                            myReader.GetInt32(13), //Agility
                            myReader.GetInt32(14), //Intellect
                            myReader.GetInt32(15), //Spirit
                            myReader.GetInt32(16), //Stamina
                            myReader.GetInt32(17), //Strength
                            myReader.GetInt32(18), //IncreacedDaggers
                            myReader.GetInt32(19), //IncreacedSwords
                            myReader.GetInt32(20), //IncreacedAxes
                            myReader.GetInt32(21), //ManaPer5
                            myReader.GetInt32(22), //HealthPer5
                            myReader.GetInt32(23), //Hit
                            myReader.GetInt32(24), //SpellHit
                            myReader.GetInt32(25), //Critical
                            myReader.GetInt32(26), //SpellCritical
                            myReader.GetInt32(27), //Dodge
                            myReader.GetInt32(28), //Parry
                            myReader.GetInt32(29), //Defence
                            myReader.GetInt32(30), //AttackPower
                            myReader.GetInt32(31), //RangedAttackPower
                            myReader.GetInt32(32), //FeralAttackPower
                            myReader.GetInt32(33), //SpellDamage
                            myReader.GetInt32(34), //SpellHealing
                            myReader.GetInt32(35), //ResistanceDecreace
                            myReader.GetInt32(36), //ArcaneDamage
                            myReader.GetInt32(37), //ShadowDamage
                            myReader.GetInt32(38), //FireDamage
                            myReader.GetInt32(39), //NatureDamage
                            myReader.GetInt32(40), //FrostDamage
                            myReader.GetInt32(41), //NatureResistance
                            myReader.GetInt32(42), //FireResistance
                            myReader.GetInt32(43), //ShadowResistance
                            myReader.GetInt32(44), //FrostResistance
                            myReader.GetInt32(45), //ArcaneResistance
                            myReader.GetString(46), //MiscEquip
                            myReader.GetString(47), //OnUse
                            myReader.GetString(48), //YellowText
                            myReader.GetString(49), //WeaponType
                            myReader.GetString(50), //Slot
                            (float)myReader.GetDouble(51), //DPS
                            (float)myReader.GetDouble(52), //DamageLow
                            (float)myReader.GetDouble(53), //DamageHigh
                            (float)myReader.GetDouble(54), //Speed
                            myReader.GetString(55), //ExtraDamage
                            myReader.GetString(56), //ChanceOnHit
                            myReader.GetString(57) //WandDamage
                            );
                        items.Add(theItem);
                    }
                    myReader.Close();
                } //end 
            }

            mySQLConnection.Close();

            return items;
        }

        public ObservableCollection<Item> SearchByFilter(string _name, List<Filter> _filters, List<StatFilter> _stats, string _orderBy)
        {
            //return MaxStat(_filters[0], _stats[0]);
            //construct the query first
            string armourQuery = " WHERE 1=1 ";
            string weaponQuery = " WHERE 1=1 ";

            bool anyWeapon = false;
            bool anyArmour = false;

            List<string> typeAndSlotWeaponList = new List<string>();
            List<string> typeAndSlotArmourList = new List<string>();

            foreach (Filter iFilter in _filters) //go through every filter we have
            {
                //we need to generate this: WHERE 1=1 AND (ArmourType = 'Cloth' AND Slot = 'Feet') OR (ArmourType = 'Plate' AND Slot = 'Waist')
                string armourSlotQuery = ""; //these will store the query for this filter, if we have more than oen at the end with join them with OR
                string weaponSlotQuery = ""; //we use 2 as we'll be executing 2 commands since they'r ein different tables and joins are hard
                switch (iFilter.IsWeapon) //we're building two queries so switch on isWeapon
                {
                    case null:
                        //search weapons and armour
                        //no checking of the other item selections needed
                        break;
                    case false:
                        //search armour
                        anyArmour = true;
                        switch (iFilter.ItemType)//plate, mail etc
                        {
                            case "Cloth":
                                armourSlotQuery += " AND ArmourType = 'Cloth'";
                                break;
                            case "Leather":
                                armourSlotQuery += " AND ArmourType = 'Leather'";
                                break;
                            case "Mail":
                                armourSlotQuery += " AND ArmourType = 'Mail'";
                                break;
                            case "Plate":
                                armourSlotQuery += " AND ArmourType = 'Plate'";
                                break;
                            case "Trinket":
                                armourSlotQuery += " AND Slot = 'Trinket'";
                                break;
                            case "Ring":
                                armourSlotQuery += " AND Slot = 'Finger'";
                                break;
                            case "Amulet":
                                armourSlotQuery += " AND Slot = 'Neck'";
                                break;
                            case "OffHandFrill":
                                armourSlotQuery += " AND ArmourType = 'OffHandFrill'";
                                break;
                            case "Shield":
                                armourSlotQuery += " AND ArmourType = 'Shield' ";
                                break;
                            case "Relic":
                                armourSlotQuery += " AND Slot = 'Relic'";
                                break;
                            case "Cloak":
                                armourSlotQuery += " AND Slot = 'Back'";
                                break;
                        }
                        if (iFilter.ItemSlot != "Select") //if it's select no point switching
                        {
                            switch (iFilter.ItemSlot)
                            {
                                case "Head":
                                    armourSlotQuery += " AND Slot = 'Head'";
                                    break;
                                case "Hands":
                                    armourSlotQuery += " AND Slot = 'Hands'";
                                    break;
                                case "Wrist":
                                    armourSlotQuery += " AND Slot = 'Wrist'";
                                    break;
                                case "Waist":
                                    armourSlotQuery += " AND Slot = 'Waist'";
                                    break;
                                case "Shoulder":
                                    armourSlotQuery += " AND Slot = 'Shoulder'";
                                    break;
                                case "Back":
                                    armourSlotQuery += " AND Slot = 'Back'";
                                    break;
                                case "Chest":
                                    armourSlotQuery += " AND Slot = 'Chest'";
                                    break;
                                case "Feet":
                                    armourSlotQuery += " AND Slot = 'Feet'";
                                    break;
                                case "Legs":
                                    armourSlotQuery += " AND Slot = 'Legs'";
                                    break;
                            }
                        }
                        //here we have the the slot of one item
                        break;
                    default:
                        //search weapons
                        anyWeapon = true;
                        switch (iFilter.ItemType) //this is the slot in the case of weapons
                        {
                            case "One-Handed":
                                weaponSlotQuery += " AND (Slot = 'OneHand' OR Slot = 'MainHand' OR Slot = 'OffHand')"; //thses aren't seperate choices in the ui yet
                                break;
                            case "Two-Handed":
                                weaponSlotQuery += " AND Slot = 'TwoHand'";
                                break;
                            case "Ranged":
                                weaponSlotQuery += " AND Slot = 'Ranged'";
                                break;
                            case "Other":
                                weaponSlotQuery += " AND Slot = 'OneHand'";
                                break;
                        }
                        if (iFilter.ItemSlot != "Select")
                        {
                            switch (iFilter.ItemSlot) //again, no point switching if the user didn't choose anything
                            {
                                case "Daggers":
                                    weaponSlotQuery += " AND WeaponType = 'Dagger'";
                                    break;
                                case "Two-Handed Swords":
                                case "One-Handed Swords":
                                    weaponSlotQuery += " AND WeaponType = 'Sword'";
                                    break;
                                case "Two-Handed Maces":
                                case "One-Handed Maces":
                                    weaponSlotQuery += " AND WeaponType = 'Mace'";
                                    break;
                                case "Fist Weapons":
                                    weaponSlotQuery += " AND WeaponType = 'Fist'";
                                    break;
                                case "Two-Handed Axes":
                                case "One-Handed Axes":
                                    weaponSlotQuery += " AND WeaponType = 'Axe'";
                                    break;
                                case "Staves":
                                    weaponSlotQuery += " AND WeaponType = 'Staff'";
                                    break;
                                case "Polearms":
                                    weaponSlotQuery += " AND WeaponType = 'Polearm'";
                                    break;
                                case "Bows":
                                    weaponSlotQuery += " AND WeaponType = 'Bow'";
                                    break;
                                case "Crossbows":
                                    weaponSlotQuery += " AND WeaponType = 'Crossbow'";
                                    break;
                                case "Guns":
                                    weaponSlotQuery += " AND WeaponType = 'Gun'";
                                    break;
                                case "Throwns":
                                    weaponSlotQuery += " AND WeaponType = 'Thrown'";
                                    break;
                                case "Wands":
                                    weaponSlotQuery += " AND WeaponType = 'Wand'";
                                    break;
                                case "Miscellaneous":
                                    weaponSlotQuery += " AND WeaponType = 'Miscellaneous'";
                                    break;
                                case "Fishing Poles":
                                    weaponSlotQuery += " AND WeaponType = 'FishingPole'";
                                    break;
                            }
                        }
                        break;
                }
                if (iFilter.IsWeapon == true) //if it's a weapon, add the weapon query to the list
                {
                    typeAndSlotWeaponList.Add(weaponSlotQuery);
                }
                else if (iFilter.IsWeapon == false) //not a weapon, add to the armour query list
                {
                    typeAndSlotArmourList.Add(armourSlotQuery);
                }
            }
            if (typeAndSlotArmourList.Count == 1) //only 1 item
            {
                armourQuery += typeAndSlotArmourList[0]; //just append it to the master query list
            }
            else if(typeAndSlotArmourList.Count > 1)//more than one item
            {
                for (int i = 0; i < typeAndSlotArmourList.Count; i++)
                {
                    if(i == 0)
                    {
                        armourQuery += " AND ((" + typeAndSlotArmourList[i].Substring(5) + ") OR "; //strip of the and and then replace it, effectively moving it outside the brackets
                    }
                    else if (i != typeAndSlotArmourList.Count - 1) //If it's not the last item, append OR. Minus 1 since the array starts at 0
                    {
                        armourQuery += "(" + typeAndSlotArmourList[i].Substring(5) + ") OR "; //strip off the first 5 characters which is " AND "
                    }
                    else //last item, as above but no OR at the end
                    {
                        armourQuery += "(" + typeAndSlotArmourList[i].Substring(5) + ")) ";
                    }
                }
            }

            if (typeAndSlotWeaponList.Count == 1) //only 1 item
            {
                weaponQuery += typeAndSlotWeaponList[0]; //just append it to the master query list
            }
            else if(typeAndSlotWeaponList.Count > 1) //more than one item
            {
                for (int i = 0; i < typeAndSlotWeaponList.Count; i++)
                {
                    if (i == 0)
                    {   //we need double brackets here for queries like this select * from weapons  WHERE 1=1  AND ((Slot = 'TwoHand' AND WeaponType = 'Axe') OR (Slot = 'TwoHand' AND WeaponType = 'Mace') OR (Slot = 'TwoHand' AND WeaponType = 'Sword')) AND Stamina > 20
                        weaponQuery += " AND ((" + typeAndSlotWeaponList[i].Substring(5) + ") OR "; //strip of the and and then replace it, effectively moving it outside the brackets
                    }
                    else if (i != typeAndSlotWeaponList.Count - 1) //If it's not the last item, append OR. Minus 1 since the array starts at 0
                    {
                        weaponQuery += "(" + typeAndSlotWeaponList[i].Substring(5) + ") OR "; //strip off the first 5 characters which is " AND "
                    }
                    else //last item, as above but no OR at the end
                    {
                        weaponQuery += "(" + typeAndSlotWeaponList[i].Substring(5) + ")) ";
                    }
                }
            }

            //now ned to add on the stat queries
            for (int i = 0; i < _stats.Count; i++)
            {
                if (i != _stats.Count - 1) //there will always be a stat filter extra at the end, we exclude it else we add AND Select > 0 to the end
                {
                    if (anyArmour || (!anyWeapon && !anyArmour)) //only add it if we have armour to search for OR no top filter has been selected, all items with stats are returned
                    {
                        armourQuery += " AND " + _stats[i].stat.Replace(" ", "") + " " + _stats[i].comparisonOperator + " " + _stats[i].value; //remove any spaces in the text in the combo box to enable it to match mysql column names
                    }
                    if (anyWeapon || (!anyWeapon && !anyArmour)) //only add it if we have weapons to search for OR no top filter has been selected, all items with stats are returned
                    {
                        weaponQuery += " AND " + _stats[i].stat.Replace(" ", "") + " " + _stats[i].comparisonOperator + " " + _stats[i].value;
                    }
                }
            }

            if (anyArmour || (!anyWeapon && !anyArmour)) //only add it if we have armour to search for OR no top filter has been selected, all items with stats are returned
            {
                if (_filters[0].Quality != "Any")
                {
                    armourQuery += " AND Quality = '" + _filters[0].Quality.Split(' ')[0] + "'"; //the split removes the (Purple) bit at the end of the combo box text
                }

                if (_filters[0].MinLevel > 0)
                {
                    armourQuery += " AND RequiredLevel >= " + _filters[0].MinLevel;    
                }
                if (_filters[0].MaxLevel > 0)
                {
                    armourQuery += " AND RequiredLevel <= " + _filters[0].MaxLevel;
                }
                if (!string.IsNullOrEmpty(_name))
                {
                    armourQuery += " AND Name LIKE '%" + _name + "%'";
                }
            }
            if (anyWeapon || (!anyWeapon && !anyArmour)) //only add it if we have weapons to search for OR no top filter has been selected, all items with stats are returned
            {
                if (_filters[0].Quality != "Any")
                {
                    weaponQuery += " AND Quality = '" + _filters[0].Quality.Split(' ')[0] + "'"; 
                }

                if (_filters[0].MinLevel > 0)
                {
                    weaponQuery += " AND RequiredLevel >= " + _filters[0].MinLevel;
                }
                if (_filters[0].MaxLevel > 0)
                {
                    weaponQuery += " AND RequiredLevel <= " + _filters[0].MaxLevel;
                }
                if (!string.IsNullOrEmpty(_name))
                {
                    weaponQuery += " AND Name LIKE '%" + _name + "%'";
                }
            }

            if (_orderBy != "Select")
            {
                if (anyArmour)
                    armourQuery += " ORDER BY " + _orderBy.Replace(" ", "") + " DESC";
                else if(anyWeapon)
                    weaponQuery += " ORDER BY " + _orderBy.Replace(" ", "") + " DESC";
            }

            Console.WriteLine("Armour Query:\n" + armourQuery); //DEBUG
            Console.WriteLine("Weapon Query:\n" + weaponQuery);

            return SearchAllItems(armourQuery, weaponQuery);
        }

        public ObservableCollection<Item> MaxStat(List<Filter> _filter, StatFilter _stat)
        {
            string weaponQuery = " WHERE 1=1 ";

            string armourQuery = " WHERE 1=1 AND " + _stat.stat.Replace(" ", "") + " > 1 AND Slot = 'Finger' ORDER BY " + _stat.stat.Replace(" ", "") + " DESC LIMIT 2;";
            var fingerResults = SearchAllItems(armourQuery, weaponQuery);
            armourQuery = " WHERE 1=1 AND " + _stat.stat.Replace(" ", "") + " > 1 AND Slot = 'Trinket' ORDER BY " + _stat.stat.Replace(" ", "") + " DESC LIMIT 2;";
            var trinketResults = SearchAllItems(armourQuery, weaponQuery);
            armourQuery = " WHERE 1=1 AND " + _stat.stat.Replace(" ", "") + " > 1 AND Slot = 'Neck' ORDER BY " + _stat.stat.Replace(" ", "") + " DESC LIMIT 1;";
            var neckResults = SearchAllItems(armourQuery, weaponQuery);
            armourQuery = " WHERE 1=1 AND " + _stat.stat.Replace(" ", "") + " > 1 AND Slot = 'Back' ORDER BY " + _stat.stat.Replace(" ", "") + " DESC LIMIT 1;";
            var backResults = SearchAllItems(armourQuery, weaponQuery);



            

            foreach (var item in trinketResults)
            {
                fingerResults.Add(item);
            }
            fingerResults.Add(neckResults[0]);
            fingerResults.Add(backResults[0]);

            return fingerResults;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_number">Number of items to return. e.g. 2 for trinkets or rings, 1 otherwise</param>
        /// <param name="_search"></param>
        /// <param name="_filter"></param>
        /// <returns></returns>
        public List<Item> MaxStat(int _number, string _search, string _filter)
        {
            SQLiteConnection mySQLConnection = new SQLiteConnection(Properties.Settings.Default.ItemsConnectionString);
            try
            {
                mySQLConnection.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            SQLiteDataReader myReader = null;
            SQLiteCommand myCommandRead = null;
            /*
            myCommandRead = new SqlCommand("SELECT TOP 2 ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE Slot = 'Trinket'" + filter +
                " SELECT TOP 1 ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE Slot = 'Back'" + filter +
                " SELECT TOP 1 ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE Slot = 'Neck'" + filter +
                " SELECT TOP 2 ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items WHERE Slot = 'Finger'" + filter, mySQLConnection); //get all columns
            */
            myCommandRead = new SQLiteCommand("SELECT ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot FROM Items " + _search + _filter + " LIMIT " + _number, mySQLConnection);

            myReader = myCommandRead.ExecuteReader(); //set the reader //error here if invalid item passed in (nul refrence)

            List<Item> items = new List<Item>(); //list of items found

            if (myReader.HasRows) //item search found something
            {
                while (myReader.Read()) //for each row
                {
                    //cerate an item
                    Item theItem = new Item
                        (
                        false,
                        myReader.GetInt32(0),//ItemID
                        myReader.GetString(1),//Name
                        myReader.GetInt32(2),//RequiredLevel
                        myReader.GetString(3),//OtherRequirements
                        myReader.GetString(4),//OtherRequirements2
                        myReader.GetBoolean(5),//BoE
                        myReader.GetBoolean(6),//BoP
                        myReader.GetBoolean(7),//QuestItem
                        myReader.GetBoolean(8),//IsUnique
                        myReader.GetBoolean(9),//BeginQuest
                        myReader.GetString(10),//Quality
                        myReader.GetInt32(11),//Armour
                        myReader.GetInt32(12),//Durability
                        myReader.GetBoolean(13),//RandomBonuses
                        myReader.GetInt32(14),//Agility
                        myReader.GetInt32(15),//Intellect
                        myReader.GetInt32(16),//Spirit
                        myReader.GetInt32(17),//Stamina
                        myReader.GetInt32(18),//Strength
                        myReader.GetInt32(19),//IncreacedDaggers
                        myReader.GetInt32(20),//IncreacedSwords
                        myReader.GetInt32(21),//IncreacedAxes
                        myReader.GetInt32(22),//ManaPer5
                        myReader.GetInt32(23),//HealthPer5
                        myReader.GetInt32(24),//Hit
                        myReader.GetInt32(25),//SpellHit
                        myReader.GetInt32(26),//Critical
                        myReader.GetInt32(27),//SpellCritical
                        myReader.GetInt32(28),//Dodge
                        myReader.GetInt32(29),//Parry
                        myReader.GetInt32(30),//Defence
                        myReader.GetInt32(31),//BaseBlock
                        myReader.GetInt32(32),//BlockValue
                        myReader.GetInt32(33),//BlockPercent
                        myReader.GetInt32(34),//AttackPower
                        myReader.GetInt32(35),//RangedAttackPower
                        myReader.GetInt32(36),//SpellDamage
                        myReader.GetInt32(37),//SpellHealing
                        myReader.GetInt32(38),//ResistanceDecreace
                        myReader.GetInt32(39),//ArcaneDamage
                        myReader.GetInt32(40),//ShadowDamage
                        myReader.GetInt32(41),//FireDamage
                        myReader.GetInt32(42),//NatureDamage
                        myReader.GetInt32(43),//FrostDamage
                        myReader.GetInt32(44),//NatureResistance
                        myReader.GetInt32(45),//FireResistance
                        myReader.GetInt32(46),//ShadowResistance
                        myReader.GetInt32(47),//FrostResistance
                        myReader.GetInt32(48),//ArcaneResistance
                        myReader.GetString(49),//MiscEquip
                        myReader.GetString(50),//OnUse
                        myReader.GetString(51),//YellowText
                        myReader.GetString(52),//ArmourType
                        myReader.GetString(53)//Slot
                        );
                    //Console.WriteLine("\"" + myReader[52].ToString() + "\"");
                    items.Add(theItem); //add to list
                }
            }

            myReader.Close();
            mySQLConnection.Close();

            return items;
        }
    }
}
