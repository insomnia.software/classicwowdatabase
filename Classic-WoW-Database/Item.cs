﻿using System;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using System.Data.SQLite;
using System.Collections.ObjectModel;

namespace Classic_WoW_Database
{
    public class Item
    {
        #region variables
        private string HTML;
        private const string FILE_LOCATION = @"..\..\..\Database\";

        public bool isWeapon { get; private set; }

        public int itemID { get; private set; }
        public string name { get; private set; }
        public int requiredLevel { get; private set; }
        public string otherRequirements { get; private set; } //engineering for example (includes class requirements)
        public string otherRequirements2 { get; private set; }

        public bool boe { get; private set; }
        public bool bop { get; private set; }
        public bool questItem { get; private set; } //quest item
        public bool beginQuest { get; private set; } //this item begins a quest
        public bool unique { get; private set; }
        public Quality quality { get; private set; }

        public int armour { get; private set; }
        public int durability { get; private set; }

        public bool randomBonuses { get; private set; }  //see item 20683 of the bear type thing
        public int stamina { get; private set; } //tested
        public int agility { get; private set; }
        public int spirit { get; private set; }
        public int intellect { get; private set; } //tested
        public int strength { get; private set; } //tested

        public int increacedDaggers { get; private set; } //tested
        public int increacedSwords { get; private set; }
        public int increacedAxes { get; private set; }

        public int hp5 { get; private set; }
        public int mp5 { get; private set; } //tested
        public int hit { get; private set; }
        public int spellHit { get; private set; }
        public int crit { get; private set; } //tested
        public int spellCrit { get; private set; } //tested
        public int dodge { get; private set; }
        public int parry { get; private set; } //tested
        public int defence { get; private set; } //tested
        public int baseBlock { get; private set; } //on shields
        public int blockValue { get; private set; } //+ block value on shields and items
        public int blockPercent { get; private set; } //+1% chance to block
        public int attackPower { get; private set; } //tested
        public int rangedAttackPower { get; private set; }
        public int feralAttackPower { get; private set; }
        public int spellDamage { get; private set; } //tested
        public int spellHealing { get; private set; }
        public int resistanceDecreace { get; private set; } //old spell pen

        public int arcaneDamage { get; private set; }
        public int shadowDamage { get; private set; }
        public int fireDamage { get; private set; }
        public int natureDamage { get; private set; }
        public int frostDamage { get; private set; }

        public int natureRes { get; private set; } //tested
        public int shadowRes { get; private set; }
        public int fireRes { get; private set; } //tested
        public int frostRes { get; private set; }
        public int arcaneRes { get; private set; }

        public string use { get; private set; } //tested (no cd)
        public string miscEquip { get; private set; } //anything that starts "Equip: " that is not one of the above. So atiesh or ashbringer

        public string yellowText { get; private set; } //tested

        //Armour
        public ArmourType armourType { get; private set; }
        public ArmourSlot armourSlot { get; private set; }

        //Weapon
        public WeaponType weaponType { get; private set; } //tested
        public WeaponSlot weaponSlot { get; private set; } //tested

        public float dps { get; private set; } //tested
        public float damageLow { get; private set; } //tested
        public float damageHigh { get; private set; } //tested
        public float speed { get; private set; } //tested
        public string extraDamage { get; private set; } //tested

        public string chanceOnHit { get; private set; } //tested

        public WandDamage wandDamage { get; private set; }

        #endregion

        private ObservableCollection<string> itemWhiteText = new ObservableCollection<string>();
        public ObservableCollection<string> ItemWhiteText
        {
            get { return itemWhiteText; }
            set
            {
                itemWhiteText = value;
            }
        }

        private ObservableCollection<string> itemGreenText = new ObservableCollection<string>();
        public ObservableCollection<string> ItemGreenText
        {
            get { return itemGreenText; }
            set
            {
                itemGreenText = value;
            }
        }

        private ObservableCollection<string> itemYellowText = new ObservableCollection<string>();
        public ObservableCollection<string> ItemYellowText
        {
            get { return itemYellowText; }
            set
            {
                itemYellowText = value;
            }
        }

        private System.Windows.Media.SolidColorBrush nameColour = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Red);
        public System.Windows.Media.SolidColorBrush NameColour
        {
            get { return nameColour; }
            set
            {
                nameColour = value;
            }
        }

        #region enums
        public enum Quality
        {
            Poor,
            Common,
            Rare,
            Epic,
            Legendary,
            Uncommon
        };

        public enum ArmourType
        {
            Null,
            Cloth,
            Leather,
            Mail,
            Plate,
            Shield,
            OffHandFrill,
            Idol,
            Libram,
            Totem
        };

        public enum ArmourSlot
        {
            Null,
            Head,
            Neck,
            Shoulder,
            Chest,
            Back,
            Wrist,
            Hands,
            Waist,
            Legs,
            Feet,
            Finger,
            Trinket,
            OffHand,
            Relic
        };

        public enum WeaponType
        {
            Null,
            Dagger,
            Sword,
            Mace,
            //Shield,
            Fist,
            Axe,
            Staff,
            Polearm,
            //TwoHandedAxe,
            //TwoHandedSword,
            //TwoHandedMace,
            Bow,
            Crossbow,
            Gun,
            Thrown,
            Wand,
            Miscellaneous,
            FishingPole
        };

        public enum WeaponSlot
        {
            Null,
            MainHand,
            OneHand,
            OffHand,
            TwoHand,
            Ranged,
            Relic
        };

        public enum WandDamage
        {
            Null,
            Holy,
            Fire,
            Shadow,
            Nature,
            Frost,
            Arcane
        };

        #endregion

        //item constructor
        public Item(bool m_isWeapon, int m_itemID, string m_name, int m_requiredLevel, string m_otherRequirements, string m_otherRequirements2, bool m_boe, bool m_bop, bool m_questItem, bool m_unique, bool m_beginQusest, string m_quality, int m_armour, int m_durability, bool m_randomBonuses, int m_agility, int m_intellect, int m_spirit, int m_stamina, int m_strength, int m_increacsedDaggers, int m_increasedSwords, int m_increasedAxes, int m_mp5, int m_hp5, int m_hit, int m_spellHit, int m_critical, int m_spellCritical, int m_dodge, int m_parry, int m_defence, int m_baseBlock, int m_blockValue, int m_blockPercent, int m_attachPower, int m_rangedAttackPower, int m_spellDamage, int m_spellHealing, int m_resistanceDecrease, int m_arcaneDamage, int m_shadowDamage, int m_fireDamage, int m_natureDamage, int m_frostDamage, int m_natureResistance, int m_fireResistance, int m_shadowResistance, int m_frostResistance, int m_arcaneResistance, string m_use, string m_miscEquip, string m_yellowText, string m_armourType, string m_armourSlot)
        {
            isWeapon = m_isWeapon;
            itemID = m_itemID;
            name = m_name;
            requiredLevel = m_requiredLevel;
            otherRequirements = m_otherRequirements;
            otherRequirements2 = m_otherRequirements2;
            boe = m_boe;
            bop = m_bop;
            questItem = m_questItem;
            unique = m_unique;
            beginQuest = m_beginQusest;
            #region quality switch
            switch (m_quality)
            {
                case "Rare":
                    quality = Quality.Rare;
                    break;
                case "Legendary":
                    quality = Quality.Legendary;
                    break;
                case "Uncommon":
                    quality = Quality.Uncommon;
                    break;
                case "Poor":
                    quality = Quality.Poor;
                    break;
                case "Common":
                    quality = Quality.Common;
                    break;
                case "Epic":
                    quality = Quality.Epic;
                    break;
            }
            #endregion
            armour = m_armour;
            durability = m_durability;
            randomBonuses = m_randomBonuses;
            agility = m_agility;
            intellect = m_intellect;
            spirit = m_spirit;
            stamina = m_stamina;
            strength = m_strength;
            increacedDaggers = m_increacsedDaggers;
            increacedSwords = m_increasedSwords;
            increacedAxes = m_increasedAxes;
            mp5 = m_mp5;
            hp5 = m_hp5;
            hit = m_hit;
            spellHit = m_spellHit;
            crit = m_critical;
            spellCrit = m_spellCritical;
            dodge = m_dodge;
            parry = m_parry;
            defence = m_defence;
            baseBlock = m_baseBlock;
            blockValue = m_blockValue;
            blockPercent = m_blockPercent;
            attackPower = m_attachPower;
            rangedAttackPower = m_rangedAttackPower;
            spellDamage = m_spellDamage;
            spellHealing = m_spellHealing;
            resistanceDecreace = m_resistanceDecrease;
            arcaneDamage = m_arcaneDamage;
            shadowDamage = m_shadowDamage;
            fireDamage = m_fireDamage;
            natureDamage = m_natureDamage;
            frostDamage = m_frostDamage;
            natureRes = m_natureResistance;
            fireRes = m_fireResistance;
            shadowRes = m_shadowResistance;
            frostRes = m_frostResistance;
            arcaneRes = m_arcaneResistance;
            use = m_use;
            miscEquip = m_miscEquip;
            yellowText = m_yellowText;
            #region Type switch
            switch (m_armourType)
            {
                case "Plate":
                    armourType = ArmourType.Plate;
                    break;
                case "Cloth":
                    armourType = ArmourType.Cloth;
                    break;
                case "Leather":
                    armourType = ArmourType.Leather;
                    break;
                case "Mail":
                    armourType = ArmourType.Mail;
                    break;
                case "Shield":
                    armourType = ArmourType.Shield;
                    break;
                case "Libram":
                    armourType = ArmourType.Libram;
                    break;
                case "Idol":
                    armourType = ArmourType.Idol;
                    break;
                case "Totem":
                    armourType = ArmourType.Totem;
                    break;
                case "OffHandFrill":
                    armourType = ArmourType.OffHandFrill;
                    break;
                default:
                    armourType = ArmourType.Null;
                    break;
            }
            #endregion
            #region Slot switch
            switch (m_armourSlot)
            {
                case "Trinket":
                    armourSlot = ArmourSlot.Trinket;
                    break;
                case "Finger":
                    armourSlot = ArmourSlot.Finger;
                    break;
                case "Head":
                    armourSlot = ArmourSlot.Head;
                    break;
                case "Hands":
                    armourSlot = ArmourSlot.Hands;
                    break;
                case "Wrist":
                    armourSlot = ArmourSlot.Wrist;
                    break;
                case "Waist":
                    armourSlot = ArmourSlot.Waist;
                    break;
                case "Shoulder":
                    armourSlot = ArmourSlot.Shoulder;
                    break;
                case "Back":
                    armourSlot = ArmourSlot.Back;
                    break;
                case "Chest":
                    armourSlot = ArmourSlot.Chest;
                    break;
                case "Feet":
                    armourSlot = ArmourSlot.Feet;
                    break;
                case "Legs":
                    armourSlot = ArmourSlot.Legs;
                    break;
                case "Neck":
                    armourSlot = ArmourSlot.Neck;
                    break;
                case "OffHand":
                    armourSlot = ArmourSlot.OffHand;
                    break;
                case "Relic":
                    armourSlot = ArmourSlot.Relic;
                    break;
            }
            #endregion

            ItemWhiteText = AddWhiteText();
            ItemGreenText = AddGreenText();
            itemYellowText = AddYellowText();

            switch (quality)
            {
                case Item.Quality.Poor:
                    NameColour = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Gray);
                    break;
                case Item.Quality.Common:
                    NameColour = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.White);
                    break;
                case Item.Quality.Uncommon:
                    NameColour = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(30, 255, 0));
                    break;
                case Item.Quality.Rare:
                    NameColour = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Blue);
                    break;
                case Item.Quality.Epic:
                    NameColour = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(163, 53, 238));
                    break;
                case Item.Quality.Legendary:
                    NameColour = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Orange);
                    break;
            }
        }

        //weapon constructor
        public Item(bool m_isWeapon, int m_itemID, string m_name, int m_requiredLevel, string m_otherRequirements, string m_otherRequirements2, bool m_boe, bool m_bop, bool m_questItem, bool m_unique, string m_quality, int m_armour, int m_durability, bool m_randomBonuses, int m_agility, int m_intellect, int m_spirit, int m_stamina, int m_strength, int m_increacsedDaggers, int m_increasedSwords, int m_increasedAxes, int m_mp5, int m_hp5, int m_hit, int m_spellHit, int m_critical, int m_spellCritical, int m_dodge, int m_parry, int m_defence, int m_attachPower, int m_rangedAttackPower, int m_feralAttackPower, int m_spellDamage, int m_spellHealing, int m_resistanceDecrease, int m_arcaneDamage, int m_shadowDamage, int m_fireDamage, int m_natureDamage, int m_frostDamage, int m_natureResistance, int m_fireResistance, int m_shadowResistance, int m_frostResistance, int m_arcaneResistance, string m_use, string m_miscEquip, string m_yellowText, string m_weaponType, string m_weaponSlot, float m_DPS, float m_damageLow, float m_damageHigh, float m_speed, string m_extraDamage, string m_chanceOnHit, string m_wandDamage)
        {
            isWeapon = m_isWeapon;
            itemID = m_itemID;
            name = m_name;
            requiredLevel = m_requiredLevel;
            otherRequirements = m_otherRequirements;
            otherRequirements2 = m_otherRequirements2;
            boe = m_boe;
            bop = m_bop;
            questItem = m_questItem;
            unique = m_unique;
            #region quality switch
            switch (m_quality)
            {
                case "Rare":
                    quality = Quality.Rare;
                    break;
                case "Legendary":
                    quality = Quality.Legendary;
                    break;
                case "Uncommon":
                    quality = Quality.Uncommon;
                    break;
                case "Poor":
                    quality = Quality.Poor;
                    break;
                case "Common":
                    quality = Quality.Common;
                    break;
                case "Epic":
                    quality = Quality.Epic;
                    break;
            }
            #endregion
            armour = m_armour;
            durability = m_durability;
            randomBonuses = m_randomBonuses;
            agility = m_agility;
            intellect = m_intellect;
            spirit = m_spirit;
            stamina = m_stamina;
            strength = m_strength;
            increacedDaggers = m_increacsedDaggers;
            increacedSwords = m_increasedSwords;
            increacedAxes = m_increasedAxes;
            mp5 = m_mp5;
            hp5 = m_hp5;
            hit = m_hit;
            spellHit = m_spellHit;
            crit = m_critical;
            spellCrit = m_spellCritical;
            dodge = m_dodge;
            parry = m_parry;
            defence = m_defence;
            attackPower = m_attachPower;
            rangedAttackPower = m_rangedAttackPower;
            feralAttackPower = m_feralAttackPower;
            spellDamage = m_spellDamage;
            spellHealing = m_spellHealing;
            resistanceDecreace = m_resistanceDecrease;
            arcaneDamage = m_arcaneDamage;
            shadowDamage = m_shadowDamage;
            fireDamage = m_fireDamage;
            natureDamage = m_natureDamage;
            frostDamage = m_frostDamage;
            natureRes = m_natureResistance;
            fireRes = m_fireResistance;
            shadowRes = m_shadowResistance;
            frostRes = m_frostResistance;
            arcaneRes = m_arcaneResistance;
            use = m_use;
            miscEquip = m_miscEquip;
            yellowText = m_yellowText;
            #region Type switch
            switch (m_weaponType)
            {
                case "Dagger":
                    weaponType = WeaponType.Dagger;
                    break;
                case "Sword":
                    weaponType = WeaponType.Sword;
                    break;
                case "Mace":
                    weaponType = WeaponType.Mace;
                    break;
                case "Axe":
                    weaponType = WeaponType.Axe;
                    break;
                case "Staff":
                    weaponType = WeaponType.Staff;
                    break;
                case "Polearm":
                    weaponType = WeaponType.Polearm;
                    break;
                case "Fist":
                    weaponType = WeaponType.Fist;
                    break;
                case "Bow":
                    weaponType = WeaponType.Bow;
                    break;
                case "Gun":
                    weaponType = WeaponType.Gun;
                    break;
                case "Crossbow":
                    weaponType = WeaponType.Crossbow;
                    break;
                case "Thrown":
                    weaponType = WeaponType.Thrown;
                    break;
                case "Wand":
                    weaponType = WeaponType.Wand;
                    break;
                case "Miscellaneous":
                    weaponType = WeaponType.Miscellaneous;
                    break;
                case "FishingPole":
                    weaponType = WeaponType.FishingPole;
                    break;
                default:
                    weaponType = WeaponType.Null;
                    break;
            }
            #endregion
            #region Slot switch
            switch (m_weaponSlot)
            {
                case "MainHand":
                    weaponSlot = WeaponSlot.MainHand;
                    break;
                case "OneHand":
                    weaponSlot = WeaponSlot.OneHand;
                    break;
                case "TwoHand":
                    weaponSlot = WeaponSlot.TwoHand;
                    break;
                case "Ranged":
                    weaponSlot = WeaponSlot.Ranged;
                    break;
                case "OffHand":
                    weaponSlot = WeaponSlot.OffHand;
                    break;
                case "Relic":
                    weaponSlot = WeaponSlot.Relic;
                    break;
            }
            #endregion
            dps = m_DPS;
            damageLow = m_damageLow;
            damageHigh = m_damageHigh;
            speed = m_speed;
            extraDamage = m_extraDamage;
            chanceOnHit = m_chanceOnHit;
            #region wand switch
            switch (m_wandDamage)
            {
                case "Fire":
                    wandDamage = WandDamage.Fire;
                    break;
                case "Shadow":
                    wandDamage = WandDamage.Shadow;
                    break;
                case "Nature":
                    wandDamage = WandDamage.Nature;
                    break;
                case "Frost":
                    wandDamage = WandDamage.Frost;
                    break;
                case "Arcane":
                    wandDamage = WandDamage.Arcane;
                    break;
                case "Holy":
                    wandDamage = WandDamage.Holy;
                    break;
            }
            #endregion

            ItemWhiteText = AddWhiteText();
            ItemGreenText = AddGreenText();
            itemYellowText = AddYellowText();

            switch (quality)
            {
                case Item.Quality.Poor:
                    NameColour = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Gray);
                    break;
                case Item.Quality.Common:
                    NameColour = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.White);
                    break;
                case Item.Quality.Uncommon:
                    NameColour = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(30, 255, 0));
                    break;
                case Item.Quality.Rare:
                    NameColour = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Blue);
                    break;
                case Item.Quality.Epic:
                    NameColour = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(163, 53, 238));
                    break;
                case Item.Quality.Legendary:
                    NameColour = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Orange);
                    break;
            }
        }

        public Item() //this is a hack to print no results found in the list box
        {
            name = "No results found";
        }

        #region GetHTML
        public bool GetHTML(int itemNumber)
        {
            string URL = "http://db.valkyrie-wow.com/?item=" + itemNumber.ToString();
            using (WebClient client = new WebClient())
            {
                try
                {
                    HTML = client.DownloadString(URL);
                    itemID = itemNumber;
                    return true;
                }
                catch (WebException) //catch 404
                {
                    return false;
                }
            }
        }
        #endregion

        #region SaveHTML
        public bool SaveHTML()
        {
            TextWriter tw = new StreamWriter(FILE_LOCATION + itemID + ".html");
            tw.Write(HTML);
            tw.Close();
            if (File.Exists(FILE_LOCATION + itemID + ".html"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Set HTML from file
        public bool SetHTML(int itemNumber)
        {
            TextReader tr = null;
            try
            {
                tr = new StreamReader(FILE_LOCATION + itemNumber + ".html");
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("HTML file for item " + itemNumber + " not found");
            }

            if (tr != null)
            {
                HTML = tr.ReadToEnd();
                itemID = itemNumber;
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region parseHTML
        public bool ParseHTML()
        {
            Match match = Regex.Match(HTML, "<table><tr><td><table><tr><td><b class=[ -~\\s]+?</div>");
            string goodHTML = match.ToString();
            goodHTML = goodHTML.Substring(39);
            string qualityString = Regex.Match(goodHTML, "[q0-9]+").ToString();

            switch (qualityString)
            {
                case "q0":
                    quality = Quality.Poor;
                    break;
                case "q1":
                    quality = Quality.Common;
                    break;
                case "q2":
                    quality = Quality.Uncommon;
                    break;
                case "q3":
                    quality = Quality.Rare;
                    break;
                case "q4":
                    quality = Quality.Epic;
                    break;
                case "q5":
                    quality = Quality.Legendary;
                    break;
                case "q":
                    return false;
            }

            name = Regex.Match(goodHTML, ">[A-Za-z0-9&()/:. ,\"'-]+?<").ToString(); //& added for item 18675, " and . added for item 18365
            name = name.Substring(1, name.Length - 2);

            if (Regex.Match(goodHTML, "Binds when [a-z ]+").ToString() == "Binds when picked up")
            {
                boe = false;
                bop = true;
            }
            else if (Regex.Match(goodHTML, "Binds when [a-z ]+").ToString() == "Binds when equipped")
            {
                bop = false;
                boe = true;
            }
            else
            {
                bop = false;
                boe = false;
            }

            match = Regex.Match(goodHTML, "Quest Item");
            if (match.ToString() != "")
            {
                questItem = true;
            }
            else
            {
                questItem = false;
            }

            match = Regex.Match(goodHTML, "This Item Begins a Quest");
            if (match.ToString() != "")
            {
                beginQuest = true;
            }
            else
            {
                beginQuest = false;
            }

            match = Regex.Match(goodHTML, "Unique");
            if (match.ToString() != "")
            {
                unique = true;
            }
            else
            {
                unique = false;
            }

            string temp = "Unique";
            string oldTemp;

            match = Regex.Match(goodHTML, ">[a-zA-Z0-9 '!,\\.:&;\\+%\\(\\)\\r/\"-]+<"); //needs ! in the regex
            do
            {
                oldTemp = temp;
                match = match.NextMatch();
                temp = match.ToString();
                if (temp != "")
                {
                    temp = temp.Substring(1, temp.Length - 2);
                }

                switch (temp)
                {
                    case "One-hand":
                        weaponSlot = WeaponSlot.OneHand;
                        armourType = ArmourType.Null;
                        armourSlot = ArmourSlot.Null;
                        isWeapon = true;
                        break;
                    case "Off Hand":
                        weaponSlot = WeaponSlot.OffHand;
                        armourType = ArmourType.Null;
                        armourSlot = ArmourSlot.Null;
                        isWeapon = true;
                        break;
                    case "Two-hand":
                        weaponSlot = WeaponSlot.TwoHand;
                        armourType = ArmourType.Null;
                        armourSlot = ArmourSlot.Null;
                        isWeapon = true;
                        break;
                    case "Main Hand":
                        weaponSlot = WeaponSlot.MainHand;
                        armourType = ArmourType.Null;
                        armourSlot = ArmourSlot.Null;
                        isWeapon = true;
                        break;
                    case "Ranged":
                        weaponSlot = WeaponSlot.Ranged;
                        armourType = ArmourType.Null;
                        armourSlot = ArmourSlot.Null;
                        isWeapon = true;
                        break;
                    case "Sword":
                        weaponType = WeaponType.Sword;
                        break;
                    case "Staff":
                        weaponType = WeaponType.Staff;
                        break;
                    case "Dagger":
                        weaponType = WeaponType.Dagger;
                        break;
                    case "Fist Weapon":
                        weaponType = WeaponType.Fist;
                        break;
                    case "Axe":
                        weaponType = WeaponType.Axe;
                        break;
                    case "Polearm":
                        weaponType = WeaponType.Polearm;
                        break;
                    case "Mace":
                        weaponType = WeaponType.Mace;
                        break;
                    case "Crossbow":
                        weaponType = WeaponType.Crossbow;
                        break;
                    case "Bow":
                        weaponType = WeaponType.Bow;
                        break;
                    case "Gun":
                        weaponType = WeaponType.Gun;
                        break;
                    case "Thrown":
                        weaponType = WeaponType.Thrown;
                        weaponSlot = WeaponSlot.Ranged;
                        isWeapon = true;
                        break;
                    case "Wand":
                        weaponType = WeaponType.Wand;
                        break;
                    case "Miscellaneous":
                        weaponType = WeaponType.Miscellaneous;
                        break;
                    case "Fishing Pole":
                        weaponType = WeaponType.FishingPole;
                        break;
                    case "Held In Off-Hand":
                        armourSlot = ArmourSlot.OffHand;
                        armourType = ArmourType.OffHandFrill;
                        break;
                    case "Shield":
                        armourType = ArmourType.Shield;
                        armourSlot = ArmourSlot.OffHand;
                        isWeapon = false;
                        break;
                    case "Trinket":
                        armourSlot = ArmourSlot.Trinket;
                        isWeapon = false;
                        break;
                    case "Finger":
                        armourSlot = ArmourSlot.Finger;
                        isWeapon = false;
                        break;
                    case "Neck":
                        armourSlot = ArmourSlot.Neck;
                        isWeapon = false;
                        break;
                    case "Head":
                        armourSlot = ArmourSlot.Head;
                        isWeapon = false;
                        break;
                    case "Chest":
                        armourSlot = ArmourSlot.Chest;
                        isWeapon = false;
                        break;
                    case "Shoulder":
                        armourSlot = ArmourSlot.Shoulder;
                        isWeapon = false;
                        break;
                    case "Back":
                        armourSlot = ArmourSlot.Back;
                        isWeapon = false;
                        break;
                    case "Wrist":
                        armourSlot = ArmourSlot.Wrist;
                        isWeapon = false;
                        break;
                    case "Hands":
                        armourSlot = ArmourSlot.Hands;
                        isWeapon = false;
                        break;
                    case "Waist":
                        armourSlot = ArmourSlot.Waist;
                        isWeapon = false;
                        break;
                    case "Legs":
                        armourSlot = ArmourSlot.Legs;
                        isWeapon = false;
                        break;
                    case "Feet":
                        armourSlot = ArmourSlot.Feet;
                        isWeapon = false;
                        break;
                    case "Relic":
                        armourSlot = ArmourSlot.Relic;
                        isWeapon = false;
                        break;
                }

                if (isWeapon)
                {
                    if (Regex.IsMatch(temp, "Speed [0-9\\.]+"))
                    {
                        speed = float.Parse(Regex.Match(temp, "[0-9\\.]+").ToString());
                    }
                    else if (Regex.IsMatch(temp, "^[0-9a-zA-Z -]+Damage"))
                    {
                        if (Regex.IsMatch(temp, "^[0-9 -]+Damage"))
                        {
                            Match damage = Regex.Match(temp, "[0-9]+");
                            damageLow = float.Parse(damage.ToString());
                            damage = damage.NextMatch();
                            damageHigh = float.Parse(damage.ToString());
                        }
                        else if (Regex.IsMatch(temp, "Frost"))
                        {
                            //wand damage is frost
                            wandDamage = WandDamage.Frost;
                            Match damage = Regex.Match(temp, "[0-9]+");
                            damageLow = float.Parse(damage.ToString());
                            damage = damage.NextMatch();
                            damageHigh = float.Parse(damage.ToString());
                        }
                        else if (Regex.IsMatch(temp, "Nature"))
                        {
                            //wand damage is nature
                            wandDamage = WandDamage.Nature;
                            Match damage = Regex.Match(temp, "[0-9]+");
                            damageLow = float.Parse(damage.ToString());
                            damage = damage.NextMatch();
                            damageHigh = float.Parse(damage.ToString());
                        }
                        else if (Regex.IsMatch(temp, "Fire"))
                        {
                            //wand damage is fire
                            wandDamage = WandDamage.Fire;
                            Match damage = Regex.Match(temp, "[0-9]+");
                            damageLow = float.Parse(damage.ToString());
                            damage = damage.NextMatch();
                            damageHigh = float.Parse(damage.ToString());
                        }
                        else if (Regex.IsMatch(temp, "Arcane"))
                        {
                            //wand damage is arcane
                            wandDamage = WandDamage.Arcane;
                            Match damage = Regex.Match(temp, "[0-9]+");
                            damageLow = float.Parse(damage.ToString());
                            damage = damage.NextMatch();
                            damageHigh = float.Parse(damage.ToString());
                        }
                        else if (Regex.IsMatch(temp, "Shadow"))
                        {
                            //wand damage is shadow
                            wandDamage = WandDamage.Shadow;
                            Match damage = Regex.Match(temp, "[0-9]+");
                            damageLow = float.Parse(damage.ToString());
                            damage = damage.NextMatch();
                            damageHigh = float.Parse(damage.ToString());
                        }
                        else if (Regex.IsMatch(temp, "Holy"))
                        {
                            //wand damage is holy
                            wandDamage = WandDamage.Holy;
                            Match damage = Regex.Match(temp, "[0-9]+");
                            damageLow = float.Parse(damage.ToString());
                            damage = damage.NextMatch();
                            damageHigh = float.Parse(damage.ToString());
                        }
                    }
                    else if (Regex.IsMatch(temp, "damage per second"))
                    {
                        dps = float.Parse(Regex.Match(temp, "[0-9\\.]+").ToString());
                    }
                    else if (Regex.IsMatch(temp, "Chance on hit:"))
                    {
                        match = match.NextMatch();
                        temp = match.ToString();
                        temp = temp.Substring(1, temp.Length - 2);
                        chanceOnHit = temp;
                    }
                    else if (Regex.IsMatch(temp, "[0-9a-zA-Z\\+ ]+ Damage")) //rare, should be at the end
                    {
                        extraDamage = temp;
                    }
                }
                else //armour
                {
                    switch (temp)
                    {
                        case "Cloth":
                            armourType = ArmourType.Cloth;
                            break;
                        case "Mail":
                            armourType = ArmourType.Mail;
                            break;
                        case "Plate":
                            armourType = ArmourType.Plate;
                            break;
                        case "Leather":
                            armourType = ArmourType.Leather;
                            break;
                        case "Idol":
                            armourType = ArmourType.Idol;
                            break;
                        case "Libram":
                            armourType = ArmourType.Libram;
                            break;
                        case "Totem":
                            armourType = ArmourType.Totem;
                            break;
                    }
                }

                if (Regex.IsMatch(temp, "Use:"))
                {
                    match = match.NextMatch();
                    temp = match.ToString();
                    temp = temp.Substring(1, temp.Length - 2);
                    use = temp;
                    if (use.Contains("&lt;"))
                    {
                        use = use.Replace("&lt;", "");
                        use = use.Replace("&gt;", "");
                    }
                }
                else if (Regex.IsMatch(temp, "Stamina"))
                {
                    if (Regex.IsMatch(temp, "-[0-9]+"))
                    {
                        stamina = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                        stamina = (stamina * -1);
                    }
                    else
                    {
                        stamina = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                    }
                }
                else if (Regex.IsMatch(temp, "Strength"))
                {
                    try
                    {
                        strength = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                    }
                    catch (FormatException)
                    {
                        //nothing needs to be done, this is hit by item 21394
                        //the set "Battlegear of strength" is picked up as a strength atribute
                    }
                }
                else if (Regex.IsMatch(temp, "Agility"))
                {
                    agility = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }
                else if (Regex.IsMatch(temp, "Spirit$")) //the $ here is to prevent an exception on item 19956
                {
                    spirit = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }
                else if (Regex.IsMatch(temp, "Intellect"))
                {
                    intellect = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }

                else if (Regex.IsMatch(temp, "Attack Power"))
                {
                    if (Regex.IsMatch(temp, "ranged"))
                    {
                        rangedAttackPower = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                    }
                    else if (Regex.IsMatch(temp, "Cat"))
                    {
                        feralAttackPower = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                    }
                    else if (Regex.IsMatch(temp, "\\+[0-9]+ Attack Power.$")) //chexk if nothing follows "Attack Power."
                    {
                        attackPower = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                    }
                    else //something follows it, so it will be "Attack Power against Undead" or similar
                    {
                        miscEquip = temp;
                    }
                }
                else if (Regex.IsMatch(temp, "critical strike by"))
                {
                    crit = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }
                else if (Regex.IsMatch(temp, "hit by"))
                {
                    hit = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }
                else if (Regex.IsMatch(temp, "Restores [0-9]+ health"))
                {
                    hp5 = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }

                else if (Regex.IsMatch(temp, "^Increases[a-zA-Z ]+parry"))
                {
                    parry = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }
                else if (Regex.IsMatch(temp, "^Increases[a-zA-Z ]+dodge"))
                {
                    dodge = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }
                else if (Regex.IsMatch(temp, "^Increased Defense"))
                {
                    defence = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }
                else if (Regex.IsMatch(temp, "[0-9]+ Armor"))
                {
                    armour = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }
                else if (Regex.IsMatch(temp, "block value"))
                {
                    blockValue = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }
                else if (Regex.IsMatch(temp, "^Increases your chance to block"))
                {
                    blockPercent = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }
                else if (Regex.IsMatch(temp, "[0-9]+ Block"))
                {
                    baseBlock = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }

                else if (Regex.IsMatch(temp, "^Increased Daggers"))
                {
                    increacedDaggers = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }
                else if (Regex.IsMatch(temp, "^Increased Swords"))
                {
                    increacedSwords = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }
                else if (Regex.IsMatch(temp, "^Increased Axes"))
                {
                    increacedAxes = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }

                else if (Regex.IsMatch(temp, "^Increases damage[ a-zA-Z]+ spells and effects")) //to avoid item 23199
                {
                    //if contains frost
                    if (Regex.IsMatch(temp, "Frost"))
                    {
                        frostDamage = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                    }
                    else if (Regex.IsMatch(temp, "Fire"))
                    {
                        fireDamage = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                    }
                    else if (Regex.IsMatch(temp, "Nature"))
                    {
                        natureDamage = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                    }
                    else if (Regex.IsMatch(temp, "Arcane"))
                    {
                        arcaneDamage = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                    }
                    else if (Regex.IsMatch(temp, "Shadow"))
                    {
                        shadowDamage = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                    }
                    else
                    {
                        spellDamage = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                    }
                }
                else if (Regex.IsMatch(temp, "^Increases healing done by spells"))
                {
                    spellHealing = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }
                else if (Regex.IsMatch(temp, "critical[ a-zA-Z]+ spells"))
                {
                    spellCrit = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }
                else if (Regex.IsMatch(temp, "Restores [0-9]+ mana"))
                {
                    mp5 = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }
                else if (Regex.IsMatch(temp, "hit[ a-zA-Z]+ spells"))
                {
                    spellHit = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }


                else if (Regex.IsMatch(temp, "Fire Resistance"))
                {
                    fireRes = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }
                else if (Regex.IsMatch(temp, "Frost Resistance"))
                {
                    frostRes = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }
                else if (Regex.IsMatch(temp, "Shadow Resistance"))
                {
                    shadowRes = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }
                else if (Regex.IsMatch(temp, "Nature Resistance"))
                {
                    natureRes = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }
                else if (Regex.IsMatch(temp, "Arcane Resistance"))
                {
                    arcaneRes = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }

                else if (Regex.IsMatch(temp, "Requires Level [0-9]+"))
                {
                    requiredLevel = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }
                else if (Regex.IsMatch(temp, "^Requires [A-Z a-z()0-9]+"))
                {
                    if (otherRequirements == null)
                    {
                        otherRequirements = temp;
                        otherRequirements = otherRequirements.Replace('\r', ' ');
                    }
                    else
                    {
                        otherRequirements2 = temp;
                        otherRequirements2 = otherRequirements2.Replace('\r', ' ');
                    }
                }
                else if (Regex.IsMatch(temp, "^Classes: [A-Za-z]+"))
                {
                    otherRequirements = temp;
                }

                else if (Regex.IsMatch(temp, "Durability"))
                {
                    durability = int.Parse(Regex.Match(temp, "[0-9]+").ToString());
                }

                else if (Regex.IsMatch(temp, "^\"[a-zA-Z0-9%' \\.-]"))
                {
                    yellowText = Regex.Match(temp, "[a-zA-Z0-9%'\" \\.-]+").ToString();
                }
                else if (oldTemp == "Equip: ")
                {
                    miscEquip = temp;
                }

                else if (Regex.IsMatch(temp, "^Random Bonuses"))
                {
                    randomBonuses = true;
                }
            }
            while (match.ToString() != "");

            return true;
        }
        #endregion

        #region printing
        public void PrintItem()
        {
            Console.WriteLine(); //space line

            switch (quality)
            {
                case Quality.Common:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case Quality.Epic:
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    break;
                case Quality.Legendary:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case Quality.Poor:
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    break;
                case Quality.Uncommon:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case Quality.Rare:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    break;
            }

            Console.WriteLine(name);

            Console.ResetColor();

            if (boe)
            {
                Console.WriteLine("Binds when Equiped");
            }
            else if (bop)
            {
                Console.WriteLine("Binds when Picked Up");
            }

            if (questItem)
            {
                Console.WriteLine("Quest Item");
            }

            if (unique)
            {
                Console.WriteLine("Unique");
            }

            if (beginQuest)
            {
                Console.WriteLine("This Item Begins a Quest");
            }

            if (!isWeapon) //if it's armour
            {
                switch (armourSlot) //find slot
                {
                    case ArmourSlot.Trinket:
                        Console.WriteLine("Trinket");
                        break;
                    case ArmourSlot.Back:
                        Console.WriteLine("Back");
                        break;
                    case ArmourSlot.Chest:
                        Console.WriteLine("Chest");
                        break;
                    case ArmourSlot.Feet:
                        Console.WriteLine("Feet");
                        break;
                    case ArmourSlot.Hands:
                        Console.WriteLine("Hands");
                        break;
                    case ArmourSlot.Head:
                        Console.WriteLine("Head");
                        break;
                    case ArmourSlot.Legs:
                        Console.WriteLine("Legs");
                        break;
                    case ArmourSlot.Neck:
                        Console.WriteLine("Neck");
                        break;
                    case ArmourSlot.Finger:
                        Console.WriteLine("Finger");
                        break;
                    case ArmourSlot.Shoulder:
                        Console.WriteLine("Shoulder");
                        break;
                    case ArmourSlot.Waist:
                        Console.WriteLine("Waist");
                        break;
                    case ArmourSlot.Wrist:
                        Console.WriteLine("Wrist");
                        break;
                    case ArmourSlot.Relic:
                        Console.WriteLine("Relic");
                        break;
                }

                if (armourType != ArmourType.Null) //it has an armour type
                {
                    switch (armourType)
                    {
                        case ArmourType.Cloth:
                            Console.WriteLine("Cloth");
                            break;
                        case ArmourType.Leather:
                            Console.WriteLine("Leather");
                            break;
                        case ArmourType.Mail:
                            Console.WriteLine("Mail");
                            break;
                        case ArmourType.Plate:
                            Console.WriteLine("Plate");
                            break;
                        case ArmourType.Shield:
                            Console.WriteLine("Shield");
                            break;
                        case ArmourType.OffHandFrill:
                            Console.WriteLine("Held in Off-hand");
                            break;
                        case ArmourType.Idol:
                            Console.WriteLine("Idol");
                            break;
                        case ArmourType.Libram:
                            Console.WriteLine("Libram");
                            break;
                        case ArmourType.Totem:
                            Console.WriteLine("Totem");
                            break;
                    }
                }
            }
            else //it's a weapon
            {
                switch (weaponSlot)
                {
                    case WeaponSlot.MainHand:
                        Console.WriteLine("Main Hand");
                        break;
                    case WeaponSlot.OffHand:
                        Console.WriteLine("Off Hand");
                        break;
                    case WeaponSlot.OneHand:
                        Console.WriteLine("One Hand");
                        break;
                    case WeaponSlot.Ranged:
                        Console.WriteLine("Ranged");
                        break;
                    case WeaponSlot.Relic:
                        Console.WriteLine("Relic");
                        break;
                    case WeaponSlot.TwoHand:
                        Console.WriteLine("Two-hand");
                        break;
                }

                switch (weaponType)
                {
                    case WeaponType.Axe:
                        Console.WriteLine("Axe");
                        break;
                    case WeaponType.Bow:
                        Console.WriteLine("Bow");
                        break;
                    case WeaponType.Crossbow:
                        Console.WriteLine("Crossbow");
                        break;
                    case WeaponType.Dagger:
                        Console.WriteLine("Dagger");
                        break;
                    case WeaponType.FishingPole:
                        Console.WriteLine("Fishing Pole");
                        break;
                    case WeaponType.Fist:
                        Console.WriteLine("Fist Weapon");
                        break;
                    case WeaponType.Gun:
                        Console.WriteLine("Gun");
                        break;
                    case WeaponType.Mace:
                        Console.WriteLine("Mace");
                        break;
                    case WeaponType.Miscellaneous:
                        Console.WriteLine("Miscellaneous");
                        break;
                    case WeaponType.Polearm:
                        Console.WriteLine("Polearm");
                        break;
                    case WeaponType.Staff:
                        Console.WriteLine("Staff");
                        break;
                    case WeaponType.Sword:
                        Console.WriteLine("Sword");
                        break;
                    case WeaponType.Thrown:
                        Console.WriteLine("Thrown");
                        break;
                    //case WeaponType.TwoHandedAxe:
                    //    Console.WriteLine("Two-Handed Axe");
                    //    break;
                    //case WeaponType.TwoHandedMace:
                    //    Console.WriteLine("Two-Handed Mace");
                    //    break;
                    //case WeaponType.TwoHandedSword:
                    //    Console.WriteLine("Two-Handed Sword");
                    //    break;
                    case WeaponType.Wand:
                        Console.WriteLine("Wand");
                        break;
                }

                //damage
                if (wandDamage == WandDamage.Null) //not a wand
                {
                    Console.WriteLine(damageLow + " - " + damageHigh + " Damage");
                }
                else if (wandDamage == WandDamage.Shadow)
                {
                    Console.WriteLine(damageLow + " - " + damageHigh + " Shadow Damage");
                }
                else if (wandDamage == WandDamage.Fire)
                {
                    Console.WriteLine(damageLow + " - " + damageHigh + " Fire Damage");
                }
                else if (wandDamage == WandDamage.Frost)
                {
                    Console.WriteLine(damageLow + " - " + damageHigh + " Frost Damage");
                }
                else if (wandDamage == WandDamage.Arcane)
                {
                    Console.WriteLine(damageLow + " - " + damageHigh + " Arcane Damage");
                }
                else if (wandDamage == WandDamage.Nature)
                {
                    Console.WriteLine(damageLow + " - " + damageHigh + " Nature Damage");
                }
                else if (wandDamage == WandDamage.Holy)
                {
                    Console.WriteLine(damageLow + " - " + damageHigh + " Holy Damage");
                }
                //speed
                Console.WriteLine("Speed " + speed);
                //extra damage
                if (extraDamage != null && extraDamage != "")
                {
                    Console.WriteLine(extraDamage);
                }
                //dps
                Console.WriteLine("(" + dps + ") damage per second");
                //hit
                //crit
                //ap
            }

            if (armour != 0)
            {
                Console.WriteLine(armour + " armor");
            }

            if (baseBlock != 0)
            {
                Console.WriteLine(baseBlock + " Block");
            }

            //base stats in order str, agi, sta, int, spi
            if (strength != 0)
            {
                Console.WriteLine("+" + strength + " Strength");
            }
            if (agility != 0)
            {
                Console.WriteLine("+" + agility + " Agility");
            }
            if (stamina != 0)
            {
                if (stamina > 0)
                {
                    Console.WriteLine("+" + stamina + " Stamina");
                }
                else
                {
                    Console.WriteLine(stamina + " Stamina");
                }
            }
            if (intellect != 0)
            {
                Console.WriteLine("+" + intellect + " Intellect");
            }
            if (spirit != 0)
            {
                Console.WriteLine("+" + spirit + " Spirit");
            }

            if (fireRes != 0)
            {
                Console.WriteLine("+" + fireRes + " Fire Resistance");
            }
            if (natureRes != 0)
            {
                Console.WriteLine("+" + natureRes + " Nature Resistance");
            }
            if (shadowRes != 0)
            {
                Console.WriteLine("+" + shadowRes + " Shadow Resistance");
            }
            if (arcaneRes != 0)
            {
                Console.WriteLine("+" + arcaneRes + " Arcane Resistance");
            }
            if (frostRes != 0)
            {
                Console.WriteLine("+" + frostRes + " Frost Resistance");
            }

            if (durability != 0)
            {
                Console.WriteLine("Durability " + durability + "/" + durability);
            }

            if (otherRequirements != null && otherRequirements != "") //note that class requirements go BEFORE level requirements, others go after
            {
                if (otherRequirements.Substring(0, 7) == "Classes")
                {
                    Console.WriteLine(otherRequirements);
                }
            }

            if (requiredLevel != 0) //Level requirements here
            {
                Console.WriteLine("Requires level " + requiredLevel);
            }

            if (otherRequirements != null && otherRequirements != "") //if other requirements not null
            {
                if (otherRequirements.Substring(0, 7) != "Classes") //if it does not contain class info (already printed)
                {
                    Console.WriteLine(otherRequirements); //print requirements
                    if (otherRequirements2 != null && otherRequirements2 != "") //check for aditional requirements and print
                    {
                        Console.WriteLine(otherRequirements2);
                    }
                }
                else //if otherrequirements does contain class info (already printed)
                {
                    if (otherRequirements2 != null && otherRequirements2 != "") //check for 2nd requirements and print
                    {
                        Console.WriteLine(otherRequirements2);
                    }
                }
            }


            Console.ForegroundColor = ConsoleColor.Green;

            if (randomBonuses)
            {
                Console.WriteLine("Random Bonuses");
            }

            if (miscEquip != null && miscEquip != "")
            {
                Console.WriteLine("Equip: " + miscEquip);
            }

            if (hp5 != 0)
            {
                Console.WriteLine("Equip: Restores " + hp5 + " health every 5 sec.");
            }
            if (mp5 != 0)
            {
                Console.WriteLine("Equip: Restores " + mp5 + " mana every 5 sec.");
            }

            if (hit != 0)
            {
                Console.WriteLine("Equip: Improves your chance to hit by " + hit + "%.");
            }
            if (crit != 0)
            {
                Console.WriteLine("Equip: Improves your chance to get a critical strike by " + crit + "%.");
            }

            if (attackPower != 0)
            {
                Console.WriteLine("Equip: +" + attackPower + " Attack Power.");
            }
            if (rangedAttackPower != 0)
            {
                Console.WriteLine("Equip: +" + rangedAttackPower + " ranged Attack Power.");
            }
            if (feralAttackPower != 0)
            {
                Console.WriteLine("Equip: +" + feralAttackPower + " Attack Power in Cat, Bear, and Dire Bear forms only.");
            }

            if (spellHealing != 0)
            {
                Console.WriteLine("Equip: Increases healing done by spells and effects by up to " + spellHealing + ".");
            }
            if (spellDamage != 0)
            {
                Console.WriteLine("Equip: Increases damage and healing done by magical spells and effects by up to " + spellDamage + ".");
            }
            if (spellHit != 0)
            {
                Console.WriteLine("Equip: Improves your chance to hit with spells by " + spellHit + "%.");
            }
            if (spellCrit != 0)
            {
                Console.WriteLine("Equip: Improves your chance to get a critical strike with spells by " + spellCrit + "%.");
            }

            if (frostDamage != 0)
            {
                Console.WriteLine("Equip: Increases damage done by frost spells and effects by up to " + frostDamage + ".");
            }
            if (shadowDamage != 0)
            {
                Console.WriteLine("Equip: Increases damage done by shadow spells and effects by up to " + shadowDamage + ".");
            }
            if (fireDamage != 0)
            {
                Console.WriteLine("Equip: Increases damage done by fire spells and effects by up to " + fireDamage + ".");
            }
            if (natureDamage != 0)
            {
                Console.WriteLine("Equip: Increases damage done by nature spells and effects by up to " + natureDamage + ".");
            }
            if (arcaneDamage != 0)
            {
                Console.WriteLine("Equip: Increases damage done by arcane spells and effects by up to " + arcaneDamage + ".");
            }

            if (blockPercent != 0)
            {
                Console.WriteLine("Equip: Increases your chance to block attacks with a shield by " + blockPercent + "%.");
            }
            if (blockValue != 0)
            {
                Console.WriteLine("Equip: Increases the block value of your shield by " + blockValue + ".");
            }
            if (defence != 0)
            {
                Console.WriteLine("Equip: Increaced Defence +" + defence + ".");
            }
            if (parry != 0)
            {
                Console.WriteLine("Equip: Increases your chance to parry an attack by " + parry + "%.");
            }
            if (dodge != 0)
            {
                Console.WriteLine("Equip: Increaces your chance to dodge an attack by " + dodge + "%.");
            }

            if (increacedDaggers != 0)
            {
                Console.WriteLine("Equip: Increaced Daggers +" + increacedDaggers);
            }
            if (increacedAxes != 0)
            {
                Console.WriteLine("Equip: Increaced Axes +" + increacedAxes);
            }
            if (increacedSwords != 0)
            {
                Console.WriteLine("Equip: Increaced Swords +" + increacedSwords);
            }

            if (chanceOnHit != null && chanceOnHit != "")
            {
                Console.WriteLine("Chance on Hit: " + chanceOnHit);
            }

            if (use != null && use != "")
            {
                Console.WriteLine("Use: " + use);
            }
            Console.ResetColor();

            if (yellowText != null && yellowText != "")
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(yellowText);
                Console.ResetColor();
            }
        }
        #endregion

        public bool InsertIntoDatabase()
        {
            try
            {
                SQLiteConnection mySQLConnection = new SQLiteConnection(Classic_WoW_Database.Properties.Settings.Default.ItemsConnectionString);

                mySQLConnection.Open();

                int boeInt = 0;
                int bopInt = 0;
                int uniqueInt = 0;
                int questItemInt = 0;
                int beginQuestInt = 0;
                int randombonusesInt = 0;

                //convert bools from true/false to 1/0 so they can be inserted into sql
                if (boe)
                {
                    boeInt = 1;
                }
                if (bop)
                {
                    bopInt = 1;
                }
                if (unique)
                {
                    uniqueInt = 1;
                }
                if (questItem)
                {
                    questItemInt = 1;
                }
                if (beginQuest)
                {
                    beginQuestInt = 1;
                }
                if (randomBonuses)
                {
                    randombonusesInt = 1;
                }

                if (!isWeapon)
                {
                    if (use != null)
                    {
                        use = use.Replace("'", "''"); //keep SQL happy with apostrophe's
                    }
                    if (miscEquip != null)
                    {
                        miscEquip = miscEquip.Replace("'", "''");
                    }
                    if (yellowText != null)
                    {
                        yellowText = yellowText.Replace("'", "''");
                        //yellowText = yellowText.Replace("\"", "");
                    }
                    name = name.Replace("'", "''");//name is never null, no if

                    SQLiteCommand myCommand = new SQLiteCommand(
                        "INSERT INTO Items (ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, BeginQuest, Quality, " +
                        "Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, " +
                        "SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, BaseBlock, BlockValue, BlockPercent, AttackPower, RangedAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, " +
                        "ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, ArmourType, Slot) VALUES (" +
                        itemID + ", '" + name + "', " + requiredLevel + ", '" + otherRequirements + "', '" + otherRequirements2 + "', " + boeInt + ", " + bopInt + ", " + questItemInt + ", " + uniqueInt + ", " + beginQuestInt + ", '" + quality.ToString("f") + "', " +
                        armour + ", " + durability + ", " + randombonusesInt + ", " + agility + ", " + intellect + ", " + spirit + ", " + stamina + ", " + strength + ", " + increacedDaggers + ", " + increacedSwords + ", " + increacedAxes + ", " + mp5 + ", " + hp5 + ", " + hit + ", " +
                        spellHit + ", " + crit + ", " + spellCrit + ", " + dodge + ", " + parry + ", " + defence + ", " + baseBlock + ", " + blockValue + ", " + blockPercent + ", " + attackPower + ", " + rangedAttackPower + ", " + spellDamage + ", " + spellHealing + ", " + resistanceDecreace + ", " +
                        arcaneDamage + ", " + shadowDamage + ", " + fireDamage + ", " + natureDamage + ", " + frostDamage + ", " + natureRes + ", " + fireRes + ", " + shadowRes + ", " + frostRes + ", " + arcaneRes + ", '" + miscEquip + "', '" + use + "', '" + yellowText + "', '" + armourType.ToString("f") + "', '" + armourSlot.ToString("f") + "' " + ");", mySQLConnection);
                    myCommand.ExecuteNonQuery();
                }
                else //is weapon
                {
                    if (use != null)
                    {
                        use = use.Replace("'", "''"); //keep SQL happy with apostrophe's
                    }
                    if (miscEquip != null)
                    {
                        miscEquip = miscEquip.Replace("'", "''");
                    }
                    if (yellowText != null)
                    {
                        yellowText = yellowText.Replace("'", "''");
                        //yellowText = yellowText.Replace("\"", "");
                    }
                    if (chanceOnHit != null)
                    {
                        chanceOnHit = chanceOnHit.Replace("'", "''");
                    }
                    name = name.Replace("'", "''"); //name is never null, no if

                    SQLiteCommand myCommand = new SQLiteCommand(
                        "INSERT INTO Weapons (ItemID, Name, RequiredLevel, OtherRequirements, OtherRequirements2, BoE, BoP, QuestItem, IsUnique, Quality, " +
                        "Armour, Durability, RandomBonuses, Agility, Intellect, Spirit, Stamina, Strength, IncreacedDaggers, IncreacedSwords, IncreacedAxes, ManaPer5, HealthPer5, Hit, " +
                        "SpellHit, Critical, SpellCritical, Dodge, Parry, Defence, AttackPower, RangedAttackPower, FeralAttackPower, SpellDamage, SpellHealing, ResistanceDecreace, " +
                        "ArcaneDamage, ShadowDamage, FireDamage, NatureDamage, FrostDamage, NatureResistance, FireResistance, ShadowResistance, FrostResistance, ArcaneResistance, MiscEquip, OnUse, YellowText, " +
                        "WeaponType, Slot, DPS, DamageLow, DamageHigh, Speed, ExtraDamage, ChanceOnHit, WandDamage) VALUES (" +
                        itemID + ", '" + name + "', " + requiredLevel + ", '" + otherRequirements + "', '" + otherRequirements2 + "', " + boeInt + ", " + bopInt + ", " + questItemInt + ", " + uniqueInt + ", '" + quality.ToString("f") + "', " +
                        armour + ", " + durability + ", " + randombonusesInt + ", " + agility + ", " + intellect + ", " + spirit + ", " + stamina + ", " + strength + ", " + increacedDaggers + ", " + increacedSwords + ", " + increacedAxes + ", " + mp5 + ", " + hp5 + ", " + hit + ", " +
                        spellHit + ", " + crit + ", " + spellCrit + ", " + dodge + ", " + parry + ", " + defence + ", " + attackPower + ", " + rangedAttackPower + ", " + feralAttackPower + "," + spellDamage + ", " + spellHealing + ", " + resistanceDecreace + ", " +
                        arcaneDamage + ", " + shadowDamage + ", " + fireDamage + ", " + natureDamage + ", " + frostDamage + ", " + natureRes + ", " + fireRes + ", " + shadowRes + ", " + frostRes + ", " + arcaneRes + ", '" + miscEquip + "', '" + use + "', '" + yellowText + "', '" +
                        weaponType.ToString("f") + "', '" + weaponSlot.ToString("f") + "', " + dps + ", " + damageLow + ", " + damageHigh + ", " + speed + ", '" + extraDamage + "', '" + chanceOnHit + "', '" + wandDamage.ToString("f") + "'" + ");", mySQLConnection);
                    myCommand.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception e)
            {
                if (e is SQLiteException)
                {
                    Console.WriteLine(e.Message);
                    return false;
                }
                else
                {
                    throw e; //throw any c# exceptions for debugging
                }
            }
        }

        public void BuildFromDatabase(SQLiteDataReader reader, bool weapon)
        {
            //check if it's a weapon first, could also be passed in based on the database it's pulled from
            if (!weapon)
            {
                while (reader.Read())
                {
                    isWeapon = false;
                    #region unused printing
                    //Console.WriteLine(reader["ItemID"].ToString());
                    //Console.Write(reader["Name"].ToString());
                    //Console.WriteLine(" (" + reader["Quality"].ToString() + ")");
                    //if (reader["BoP"].ToString() == "True")
                    //{
                    //    Console.WriteLine("Binds when picked up");
                    //}
                    //if (reader["BoE"].ToString() == "True")
                    //{
                    //    Console.WriteLine("Binds when equiped");
                    //}
                    //if (reader["IsUnique"].ToString() == "True")
                    //{
                    //    Console.WriteLine("Unique");
                    //}
                    //Console.WriteLine(reader["Slot"].ToString());
                    //Console.Write("Requires level ");
                    //Console.WriteLine(reader["RequiredLevel"].ToString());
                    //Console.WriteLine("Equip: Restores " + reader["ManaPer5"].ToString() + " mana per 5 sec.");
                    //object myobject = reader["ManaPer5"];
                    //int tmp = (int)myobject;
                    //Console.WriteLine("Equip: Restores " + reader["HealthPer5"].ToString() + " health per 5 sec.");
                    //Console.WriteLine("Use: " + reader["OnUse"].ToString());
                    //Console.WriteLine(reader["MiscEquip"].ToString());
                    #endregion

                    //ItemID        0
                    //Name          1
                    //RequiredLevel 2
                    //OtherRequirements     3
                    //OtherRequirements2    4
                    //BoE           5
                    //BoP           6
                    //QuestItem     7
                    //IsUnique      8
                    //BeginQuest    9
                    //Quality       10
                    //Armour        11
                    //Durability    12
                    //RandomBonuses 13
                    //Agility       14
                    //Intellect     15
                    //Spirit        16
                    //Stamina       17  
                    //Strength      18
                    //IncreacedDaggers      19
                    //IncreacedSwords       20
                    //IncreacedAxes         21
                    //ManaPer5      22
                    //HealthPer5    23
                    //Hit           24
                    //SpellHit      25
                    //Critical      26
                    //SpellCrit     27
                    //Dodge         28
                    //Parry         29
                    //Defence       30
                    //BaseBlock     31
                    //BlockValue    32
                    //BlockPercent  33
                    //AttackPower   34
                    //RangedattackPower     35
                    //SpellDamage   36
                    //SpellHealing  37
                    //ResistanceDeceace     38
                    //ArcaneDamage  39
                    //ShadowDamage  40
                    //FireDamage    41
                    //NatureDamage  42
                    //FrostDamage   43
                    //NatureRes     44
                    //FireRes       45
                    //ShadowRes     46
                    //FrostRes      47
                    //ArcaneRes     48
                    //MiscEquip     49
                    //OnUse         50
                    //YellowText    51
                    //ArmourType    52
                    //Slot          53

                    int i = 0;
                    itemID = reader.GetInt32(i);
                    i++;
                    name = reader.GetString(i);
                    i++;
                    requiredLevel = reader.GetInt32(i);
                    i++;
                    otherRequirements = reader.GetString(i);
                    i++;
                    otherRequirements2 = reader.GetString(i);
                    i++;
                    boe = reader.GetBoolean(i);
                    i++;
                    bop = reader.GetBoolean(i);
                    i++;
                    questItem = reader.GetBoolean(i);
                    i++;
                    unique = reader.GetBoolean(i);
                    i++;
                    beginQuest = reader.GetBoolean(i);
                    i++;
                    string qualityString = reader.GetString(i);
                    #region quality switch
                    switch (qualityString)
                    {
                        case "Rare":
                            quality = Quality.Rare;
                            break;
                        case "Legendary":
                            quality = Quality.Legendary;
                            break;
                        case "Uncommon":
                            quality = Quality.Uncommon;
                            break;
                        case "Poor":
                            quality = Quality.Poor;
                            break;
                        case "Common":
                            quality = Quality.Common;
                            break;
                        case "Epic":
                            quality = Quality.Epic;
                            break;
                    }
                    #endregion
                    i++;
                    armour = reader.GetInt32(i);
                    i++;
                    durability = reader.GetInt32(i);
                    i++;
                    randomBonuses = reader.GetBoolean(i);
                    i++;
                    agility = reader.GetInt32(i);
                    i++;
                    intellect = reader.GetInt32(i);
                    i++;
                    spirit = reader.GetInt32(i);
                    i++;
                    stamina = reader.GetInt32(i);
                    i++;
                    strength = reader.GetInt32(i);
                    i++;
                    increacedDaggers = reader.GetInt32(i);
                    i++;
                    increacedSwords = reader.GetInt32(i);
                    i++;
                    increacedAxes = reader.GetInt32(i);
                    i++;
                    mp5 = reader.GetInt32(i);
                    i++;
                    hp5 = reader.GetInt32(i);
                    i++;
                    hit = reader.GetInt32(i);
                    i++;
                    spellHit = reader.GetInt32(i);
                    i++;
                    crit = reader.GetInt32(i);
                    i++;
                    spellCrit = reader.GetInt32(i);
                    i++;
                    dodge = reader.GetInt32(i);
                    i++;
                    parry = reader.GetInt32(i);
                    i++;
                    defence = reader.GetInt32(i);
                    i++;
                    baseBlock = reader.GetInt32(i);
                    i++;
                    blockValue = reader.GetInt32(i);
                    i++;
                    blockPercent = reader.GetInt32(i);
                    i++;
                    attackPower = reader.GetInt32(i);
                    i++;
                    rangedAttackPower = reader.GetInt32(i);
                    i++;
                    spellDamage = reader.GetInt32(i);
                    i++;
                    spellHealing = reader.GetInt32(i);
                    i++;
                    resistanceDecreace = reader.GetInt32(i);
                    i++;
                    arcaneDamage = reader.GetInt32(i);
                    i++;
                    shadowDamage = reader.GetInt32(i);
                    i++;
                    fireDamage = reader.GetInt32(i);
                    i++;
                    natureDamage = reader.GetInt32(i);
                    i++;
                    frostDamage = reader.GetInt32(i);
                    i++;
                    natureRes = reader.GetInt32(i);
                    i++;
                    fireRes = reader.GetInt32(i);
                    i++;
                    shadowRes = reader.GetInt32(i);
                    i++;
                    frostRes = reader.GetInt32(i);
                    i++;
                    arcaneRes = reader.GetInt32(i);
                    i++;
                    miscEquip = reader.GetString(i);
                    i++;
                    use = reader.GetString(i);
                    i++;
                    yellowText = reader.GetString(i);
                    i++;
                    string typeString = reader.GetString(i);
                    #region Type switch

                    typeString = typeString.TrimEnd(' '); //for some reason the output of the SQL contains a space on the end, no idea why, trim it off to hit the switch

                    switch (typeString)
                    {
                        case "Plate":
                            armourType = ArmourType.Plate;
                            break;
                        case "Cloth":
                            armourType = ArmourType.Cloth;
                            break;
                        case "Leather":
                            armourType = ArmourType.Leather;
                            break;
                        case "Mail":
                            armourType = ArmourType.Mail;
                            break;
                        case "Shield":
                            armourType = ArmourType.Shield;
                            break;
                        case "Libram":
                            armourType = ArmourType.Libram;
                            break;
                        case "Idol":
                            armourType = ArmourType.Idol;
                            break;
                        case "Totem":
                            armourType = ArmourType.Totem;
                            break;
                        case "OffHandFrill":
                            armourType = ArmourType.OffHandFrill;
                            break;
                        default:
                            armourType = ArmourType.Null;
                            break;
                    }
                    #endregion
                    i++;
                    string slotString = reader.GetString(i);
                    #region Slot switch
                    switch (slotString)
                    {
                        case "Trinket":
                            armourSlot = ArmourSlot.Trinket;
                            break;
                        case "Finger":
                            armourSlot = ArmourSlot.Finger;
                            break;
                        case "Head":
                            armourSlot = ArmourSlot.Head;
                            break;
                        case "Hands":
                            armourSlot = ArmourSlot.Hands;
                            break;
                        case "Wrist":
                            armourSlot = ArmourSlot.Wrist;
                            break;
                        case "Waist":
                            armourSlot = ArmourSlot.Waist;
                            break;
                        case "Shoulder":
                            armourSlot = ArmourSlot.Shoulder;
                            break;
                        case "Back":
                            armourSlot = ArmourSlot.Back;
                            break;
                        case "Chest":
                            armourSlot = ArmourSlot.Chest;
                            break;
                        case "Feet":
                            armourSlot = ArmourSlot.Feet;
                            break;
                        case "Legs":
                            armourSlot = ArmourSlot.Legs;
                            break;
                        case "Neck":
                            armourSlot = ArmourSlot.Neck;
                            break;
                        case "OffHand":
                            armourSlot = ArmourSlot.OffHand;
                            break;
                        case "Relic":
                            armourSlot = ArmourSlot.Relic;
                            break;
                    }
                    #endregion
                    i++;
                }
            }
            else //is weapon
            {
                isWeapon = true;
                //ItemID        0
                //Name          1
                //RequiredLevel 2
                //OtherRequirements     3
                //OtherRequirements2    4
                //BoE           5
                //BoP           6
                //QuestItem     7
                //IsUnique      8

                //Quality       10
                //Armour        11
                //Durability    12
                //RandomBonuses 13
                //Agility       14
                //Intellect     15
                //Spirit        16
                //Stamina       17  
                //Strength      18
                //IncreacedDaggers      19
                //IncreacedSwords       20
                //IncreacedAxes         21
                //ManaPer5      22
                //HealthPer5    23
                //Hit           24
                //SpellHit      25
                //Critical      26
                //SpellCrit     27
                //Dodge         28
                //Parry         29
                //Defence       30

                //AttackPower   34
                //RangedattackPower     35
                //FeralAttackPower
                //SpellDamage   36
                //SpellHealing  37
                //ResistanceDeceace     38
                //ArcaneDamage  39
                //ShadowDamage  40
                //FireDamage    41
                //NatureDamage  42
                //FrostDamage   43
                //NatureRes     44
                //FireRes       45
                //ShadowRes     46
                //FrostRes      47
                //ArcaneRes     48
                //MiscEquip     49
                //OnUse         50
                //YellowText    51
                //WeaponType    52
                //WeaponSlot          53

                while (reader.Read())
                {
                    int i = 0;
                    itemID = reader.GetInt32(i);
                    i++;
                    name = reader.GetString(i);
                    i++;
                    requiredLevel = reader.GetInt32(i);
                    i++;
                    otherRequirements = reader.GetString(i);
                    i++;
                    otherRequirements2 = reader.GetString(i);
                    i++;
                    boe = reader.GetBoolean(i);
                    i++;
                    bop = reader.GetBoolean(i);
                    i++;
                    questItem = reader.GetBoolean(i);
                    i++;
                    unique = reader.GetBoolean(i);
                    i++;
                    string qualityString = reader.GetString(i);
                    #region quality switch
                    switch (qualityString)
                    {
                        case "Rare":
                            quality = Quality.Rare;
                            break;
                        case "Legendary":
                            quality = Quality.Legendary;
                            break;
                        case "Uncommon":
                            quality = Quality.Uncommon;
                            break;
                        case "Poor":
                            quality = Quality.Poor;
                            break;
                        case "Common":
                            quality = Quality.Common;
                            break;
                        case "Epic":
                            quality = Quality.Epic;
                            break;
                    }
                    #endregion
                    i++;
                    armour = reader.GetInt32(i);
                    i++;
                    durability = reader.GetInt32(i);
                    i++;
                    randomBonuses = reader.GetBoolean(i);
                    i++;
                    agility = reader.GetInt32(i);
                    i++;
                    intellect = reader.GetInt32(i);
                    i++;
                    spirit = reader.GetInt32(i);
                    i++;
                    stamina = reader.GetInt32(i);
                    i++;
                    strength = reader.GetInt32(i);
                    i++;
                    increacedDaggers = reader.GetInt32(i);
                    i++;
                    increacedSwords = reader.GetInt32(i);
                    i++;
                    increacedAxes = reader.GetInt32(i);
                    i++;
                    mp5 = reader.GetInt32(i);
                    i++;
                    hp5 = reader.GetInt32(i);
                    i++;
                    hit = reader.GetInt32(i);
                    i++;
                    spellHit = reader.GetInt32(i);
                    i++;
                    crit = reader.GetInt32(i);
                    i++;
                    spellCrit = reader.GetInt32(i);
                    i++;
                    dodge = reader.GetInt32(i);
                    i++;
                    parry = reader.GetInt32(i);
                    i++;
                    defence = reader.GetInt32(i);
                    i++;
                    attackPower = reader.GetInt32(i);
                    i++;
                    rangedAttackPower = reader.GetInt32(i);
                    i++;
                    feralAttackPower = reader.GetInt32(i);
                    i++;
                    spellDamage = reader.GetInt32(i);
                    i++;
                    spellHealing = reader.GetInt32(i);
                    i++;
                    resistanceDecreace = reader.GetInt32(i);
                    i++;
                    arcaneDamage = reader.GetInt32(i);
                    i++;
                    shadowDamage = reader.GetInt32(i);
                    i++;
                    fireDamage = reader.GetInt32(i);
                    i++;
                    natureDamage = reader.GetInt32(i);
                    i++;
                    frostDamage = reader.GetInt32(i);
                    i++;
                    natureRes = reader.GetInt32(i);
                    i++;
                    fireRes = reader.GetInt32(i);
                    i++;
                    shadowRes = reader.GetInt32(i);
                    i++;
                    frostRes = reader.GetInt32(i);
                    i++;
                    arcaneRes = reader.GetInt32(i);
                    i++;
                    miscEquip = reader.GetString(i);
                    i++;
                    use = reader.GetString(i);
                    i++;
                    yellowText = reader.GetString(i);
                    i++;
                    string typeString = reader.GetString(i);
                    #region Type switch

                    typeString = typeString.TrimEnd(' '); //for some reason the output of the SQL contains a space on the end, no idea why, trim it off to hit the switch

                    switch (typeString)
                    {
                        case "Dagger":
                            weaponType = WeaponType.Dagger;
                            break;
                        case "Sword":
                            weaponType = WeaponType.Sword;
                            break;
                        case "Mace":
                            weaponType = WeaponType.Mace;
                            break;
                        case "Axe":
                            weaponType = WeaponType.Axe;
                            break;
                        case "Staff":
                            weaponType = WeaponType.Staff;
                            break;
                        case "Polearm":
                            weaponType = WeaponType.Polearm;
                            break;
                        case "Fist":
                            weaponType = WeaponType.Fist;
                            break;
                        case "Bow":
                            weaponType = WeaponType.Bow;
                            break;
                        case "Gun":
                            weaponType = WeaponType.Gun;
                            break;
                        case "Crossbow":
                            weaponType = WeaponType.Crossbow;
                            break;
                        case "Thrown":
                            weaponType = WeaponType.Thrown;
                            break;
                        case "Wand":
                            weaponType = WeaponType.Wand;
                            break;
                        case "Miscellaneous":
                            weaponType = WeaponType.Miscellaneous;
                            break;
                        case "FishingPole":
                            weaponType = WeaponType.FishingPole;
                            break;
                        default:
                            weaponType = WeaponType.Null;
                            break;
                    }
                    #endregion
                    i++;
                    string slotString = reader.GetString(i);
                    #region Slot switch
                    switch (slotString)
                    {
                        case "MainHand":
                            weaponSlot = WeaponSlot.MainHand;
                            break;
                        case "OneHand":
                            weaponSlot = WeaponSlot.OneHand;
                            break;
                        case "TwoHand":
                            weaponSlot = WeaponSlot.TwoHand;
                            break;
                        case "Ranged":
                            weaponSlot = WeaponSlot.Ranged;
                            break;
                        case "OffHand":
                            weaponSlot = WeaponSlot.OffHand;
                            break;
                        case "Relic":
                            weaponSlot = WeaponSlot.Relic;
                            break;
                    }
                    #endregion
                    i++;
                    dps = (float)reader.GetDouble(i); //getfloat gives cast error this is the fix
                    i++;
                    damageLow = (float)reader.GetDouble(i);
                    i++;
                    damageHigh = (float)reader.GetDouble(i);
                    i++;
                    speed = (float)reader.GetDouble(i);
                    i++;
                    extraDamage = reader.GetString(i);
                    i++;
                    chanceOnHit = reader.GetString(i);
                    i++;
                    string wandDamageString = reader.GetString(i);
                    #region Wand switch
                    switch (wandDamageString)
                    {
                        case "Fire":
                            wandDamage = WandDamage.Fire;
                            break;
                        case "Shadow":
                            wandDamage = WandDamage.Shadow;
                            break;
                        case "Nature":
                            wandDamage = WandDamage.Nature;
                            break;
                        case "Frost":
                            wandDamage = WandDamage.Frost;
                            break;
                        case "Arcane":
                            wandDamage = WandDamage.Arcane;
                            break;
                        case "Holy":
                            wandDamage = WandDamage.Holy;
                            break;
                    }
                    #endregion
                    i++;
                }
            }
        }

        public ObservableCollection<string> ListAttributes()
        {
            ObservableCollection<string> attributes = new ObservableCollection<string>();

            attributes.Add(name);

            //AddWhiteText(attributes);
            foreach (var attribute in AddWhiteText())
            {
                attributes.Add(attribute);   
            }

            //AddGreenText(attributes);
            foreach (var attribute in AddGreenText())
            {
                attributes.Add(attribute);
            }

            //AddYellowText(attributes);
            foreach (var attribute in AddYellowText())
            {
                attributes.Add(attribute);
            }

            return attributes;
        }

        public ObservableCollection<string> AddYellowText()
        {
            ObservableCollection<string> attributes = new ObservableCollection<string>();

            if (yellowText != null && yellowText != "")
            {
                attributes.Add(yellowText);
            }

            return attributes;
        }

        public ObservableCollection<string> AddGreenText()
        {
            ObservableCollection<string> attributes = new ObservableCollection<string>();

            if (randomBonuses)
            {
                attributes.Add("Random Bonuses");
            }

            if (miscEquip != null && miscEquip != "")
            {
                attributes.Add("Equip: " + miscEquip);
            }

            if (hp5 != 0)
            {
                attributes.Add("Equip: Restores " + hp5 + " health every 5 sec.");
            }
            if (mp5 != 0)
            {
                attributes.Add("Equip: Restores " + mp5 + " mana every 5 sec.");
            }

            if (hit != 0)
            {
                attributes.Add("Equip: Improves your chance to hit by " + hit + "%.");
            }
            if (crit != 0)
            {
                attributes.Add("Equip: Improves your chance to get a critical strike by " + crit + "%.");
            }

            if (attackPower != 0)
            {
                attributes.Add("Equip: +" + attackPower + " Attack Power.");
            }
            if (rangedAttackPower != 0)
            {
                attributes.Add("Equip: +" + rangedAttackPower + " ranged Attack Power.");
            }
            if (feralAttackPower != 0)
            {
                attributes.Add("Equip: +" + feralAttackPower + " Attack Power in Cat, Bear, and Dire Bear forms only.");
            }

            if (spellHealing != 0)
            {
                attributes.Add("Equip: Increases healing done by spells and effects by up to " + spellHealing + ".");
            }
            if (spellDamage != 0)
            {
                attributes.Add("Equip: Increases damage and healing done by magical spells and effects by up to " + spellDamage + ".");
            }
            if (spellHit != 0)
            {
                attributes.Add("Equip: Improves your chance to hit with spells by " + spellHit + "%.");
            }
            if (spellCrit != 0)
            {
                attributes.Add("Equip: Improves your chance to get a critical strike with spells by " + spellCrit + "%.");
            }

            if (frostDamage != 0)
            {
                attributes.Add("Equip: Increases damage done by frost spells and effects by up to " + frostDamage + ".");
            }
            if (shadowDamage != 0)
            {
                attributes.Add("Equip: Increases damage done by shadow spells and effects by up to " + shadowDamage + ".");
            }
            if (fireDamage != 0)
            {
                attributes.Add("Equip: Increases damage done by fire spells and effects by up to " + fireDamage + ".");
            }
            if (natureDamage != 0)
            {
                attributes.Add("Equip: Increases damage done by nature spells and effects by up to " + natureDamage + ".");
            }
            if (arcaneDamage != 0)
            {
                attributes.Add("Equip: Increases damage done by arcane spells and effects by up to " + arcaneDamage + ".");
            }

            if (blockPercent != 0)
            {
                attributes.Add("Equip: Increases your chance to block attacks with a shield by " + blockPercent + "%.");
            }
            if (blockValue != 0)
            {
                attributes.Add("Equip: Increases the block value of your shield by " + blockValue + ".");
            }
            if (defence != 0)
            {
                attributes.Add("Equip: Increaced Defence +" + defence + ".");
            }
            if (parry != 0)
            {
                attributes.Add("Equip: Increases your chance to parry an attack by " + parry + "%.");
            }
            if (dodge != 0)
            {
                attributes.Add("Equip: Increaces your chance to dodge an attack by " + dodge + "%.");
            }

            if (increacedDaggers != 0)
            {
                attributes.Add("Equip: Increaced Daggers +" + increacedDaggers);
            }
            if (increacedAxes != 0)
            {
                attributes.Add("Equip: Increaced Axes +" + increacedAxes);
            }
            if (increacedSwords != 0)
            {
                attributes.Add("Equip: Increaced Swords +" + increacedSwords);
            }

            if (chanceOnHit != null && chanceOnHit != "")
            {
                attributes.Add("Chance on Hit: " + chanceOnHit);
            }

            if (use != null && use != "")
            {
                attributes.Add("Use: " + use);
            }

            return attributes;
        }

        public ObservableCollection<string> AddWhiteText()
        {
            ObservableCollection<string> attributes = new ObservableCollection<string>();

            if (boe)
            {
                attributes.Add("Binds when Equiped");
            }
            else if (bop)
            {
                attributes.Add("Binds when Picked Up");
            }

            if (questItem)
            {
                attributes.Add("Quest Item");
            }

            if (unique)
            {
                attributes.Add("Unique");
            }

            if (beginQuest)
            {
                attributes.Add("This Item Begins a Quest");
            }

            if (!isWeapon) //if it's armour
            {
                switch (armourSlot) //find slot
                {
                    case ArmourSlot.Trinket:
                        attributes.Add("Trinket");
                        break;
                    case ArmourSlot.Back:
                        attributes.Add("Back");
                        break;
                    case ArmourSlot.Chest:
                        attributes.Add("Chest");
                        break;
                    case ArmourSlot.Feet:
                        attributes.Add("Feet");
                        break;
                    case ArmourSlot.Hands:
                        attributes.Add("Hands");
                        break;
                    case ArmourSlot.Head:
                        attributes.Add("Head");
                        break;
                    case ArmourSlot.Legs:
                        attributes.Add("Legs");
                        break;
                    case ArmourSlot.Neck:
                        attributes.Add("Neck");
                        break;
                    case ArmourSlot.Finger:
                        attributes.Add("Finger");
                        break;
                    case ArmourSlot.Shoulder:
                        attributes.Add("Shoulder");
                        break;
                    case ArmourSlot.Waist:
                        attributes.Add("Waist");
                        break;
                    case ArmourSlot.Wrist:
                        attributes.Add("Wrist");
                        break;
                    case ArmourSlot.Relic:
                        attributes.Add("Relic");
                        break;
                }

                if (armourType != ArmourType.Null) //it has an armour type
                {
                    switch (armourType)
                    {
                        case ArmourType.Cloth:
                            attributes.Add("Cloth");
                            break;
                        case ArmourType.Leather:
                            attributes.Add("Leather");
                            break;
                        case ArmourType.Mail:
                            attributes.Add("Mail");
                            break;
                        case ArmourType.Plate:
                            attributes.Add("Plate");
                            break;
                        case ArmourType.Shield:
                            attributes.Add("Shield");
                            break;
                        case ArmourType.OffHandFrill:
                            attributes.Add("Held in Off-hand");
                            break;
                        case ArmourType.Idol:
                            attributes.Add("Idol");
                            break;
                        case ArmourType.Libram:
                            attributes.Add("Libram");
                            break;
                        case ArmourType.Totem:
                            attributes.Add("Totem");
                            break;
                    }
                }
            }
            else //it's a weapon
            {
                switch (weaponSlot)
                {
                    case WeaponSlot.MainHand:
                        attributes.Add("Main Hand");
                        break;
                    case WeaponSlot.OffHand:
                        attributes.Add("Off Hand");
                        break;
                    case WeaponSlot.OneHand:
                        attributes.Add("One Hand");
                        break;
                    case WeaponSlot.Ranged:
                        attributes.Add("Ranged");
                        break;
                    case WeaponSlot.Relic:
                        attributes.Add("Relic");
                        break;
                    case WeaponSlot.TwoHand:
                        attributes.Add("Two-hand");
                        break;
                }

                switch (weaponType)
                {
                    case WeaponType.Axe:
                        attributes.Add("Axe");
                        break;
                    case WeaponType.Bow:
                        attributes.Add("Bow");
                        break;
                    case WeaponType.Crossbow:
                        attributes.Add("Crossbow");
                        break;
                    case WeaponType.Dagger:
                        attributes.Add("Dagger");
                        break;
                    case WeaponType.FishingPole:
                        attributes.Add("Fishing Pole");
                        break;
                    case WeaponType.Fist:
                        attributes.Add("Fist Weapon");
                        break;
                    case WeaponType.Gun:
                        attributes.Add("Gun");
                        break;
                    case WeaponType.Mace:
                        attributes.Add("Mace");
                        break;
                    case WeaponType.Miscellaneous:
                        attributes.Add("Miscellaneous");
                        break;
                    case WeaponType.Polearm:
                        attributes.Add("Polearm");
                        break;
                    case WeaponType.Staff:
                        attributes.Add("Staff");
                        break;
                    case WeaponType.Sword:
                        attributes.Add("Sword");
                        break;
                    case WeaponType.Thrown:
                        attributes.Add("Thrown");
                        break;
                    //case WeaponType.TwoHandedAxe:
                    //    attributes.Add("Two-Handed Axe");
                    //    break;
                    //case WeaponType.TwoHandedMace:
                    //    attributes.Add("Two-Handed Mace");
                    //    break;
                    //case WeaponType.TwoHandedSword:
                    //    attributes.Add("Two-Handed Sword");
                    //    break;
                    case WeaponType.Wand:
                        attributes.Add("Wand");
                        break;
                }

                //damage
                if (wandDamage == WandDamage.Null) //not a wand
                {
                    attributes.Add(damageLow + " - " + damageHigh + " Damage");
                }
                else if (wandDamage == WandDamage.Shadow)
                {
                    attributes.Add(damageLow + " - " + damageHigh + " Shadow Damage");
                }
                else if (wandDamage == WandDamage.Fire)
                {
                    attributes.Add(damageLow + " - " + damageHigh + " Fire Damage");
                }
                else if (wandDamage == WandDamage.Frost)
                {
                    attributes.Add(damageLow + " - " + damageHigh + " Frost Damage");
                }
                else if (wandDamage == WandDamage.Arcane)
                {
                    attributes.Add(damageLow + " - " + damageHigh + " Arcane Damage");
                }
                else if (wandDamage == WandDamage.Nature)
                {
                    attributes.Add(damageLow + " - " + damageHigh + " Nature Damage");
                }
                else if (wandDamage == WandDamage.Holy)
                {
                    attributes.Add(damageLow + " - " + damageHigh + " Holy Damage");
                }
                //speed
                attributes.Add("Speed " + speed);
                //extra damage
                if (extraDamage != null && extraDamage != "")
                {
                    attributes.Add(extraDamage);
                }
                //dps
                attributes.Add("(" + dps + ") damage per second");
                //hit
                //crit
                //ap
            }

            if (armour != 0)
            {
                attributes.Add(armour + " armor");
            }

            if (baseBlock != 0)
            {
                attributes.Add(baseBlock + " Block");
            }

            //base stats in order str, agi, sta, int, spi
            if (strength != 0)
            {
                attributes.Add("+" + strength + " Strength");
            }
            if (agility != 0)
            {
                attributes.Add("+" + agility + " Agility");
            }
            if (stamina != 0)
            {
                if (stamina > 0)
                {
                    attributes.Add("+" + stamina + " Stamina");
                }
                else
                {
                    attributes.Add(stamina + " Stamina");
                }
            }
            if (intellect != 0)
            {
                attributes.Add("+" + intellect + " Intellect");
            }
            if (spirit != 0)
            {
                attributes.Add("+" + spirit + " Spirit");
            }

            if (fireRes != 0)
            {
                attributes.Add("+" + fireRes + " Fire Resistance");
            }
            if (natureRes != 0)
            {
                attributes.Add("+" + natureRes + " Nature Resistance");
            }
            if (shadowRes != 0)
            {
                attributes.Add("+" + shadowRes + " Shadow Resistance");
            }
            if (arcaneRes != 0)
            {
                attributes.Add("+" + arcaneRes + " Arcane Resistance");
            }
            if (frostRes != 0)
            {
                attributes.Add("+" + frostRes + " Frost Resistance");
            }

            if (durability != 0)
            {
                attributes.Add("Durability " + durability + "/" + durability);
            }

            if (otherRequirements != null && otherRequirements != "") //note that class requirements go BEFORE level requirements, others go after
            {
                if (otherRequirements.Substring(0, 7) == "Classes")
                {
                    attributes.Add(otherRequirements);
                }
            }

            if (requiredLevel != 0) //Level requirements here
            {
                attributes.Add("Requires level " + requiredLevel);
            }

            if (otherRequirements != null && otherRequirements != "") //if other requirements not null
            {
                if (otherRequirements.Substring(0, 7) != "Classes") //if it does not contain class info (already printed)
                {
                    attributes.Add(otherRequirements); //print requirements
                    if (otherRequirements2 != null && otherRequirements2 != "") //check for aditional requirements and print
                    {
                        attributes.Add(otherRequirements2);
                    }
                }
                else //if otherrequirements does contain class info (already printed)
                {
                    if (otherRequirements2 != null && otherRequirements2 != "") //check for 2nd requirements and print
                    {
                        attributes.Add(otherRequirements2);
                    }
                }
            }

            return attributes;
        }

        public override string ToString()
        {
            string returnString = "";

            returnString += name + "\n";

            if (boe)
            {
                returnString += "Binds when Equiped" + "\n";
            }
            else
            {
                returnString += "Binds when Picked Up" + "\n";
            }

            if (unique)
            {
                returnString += "Unique" + "\n";
            }

            if (!isWeapon) //if it's armour
            {
                switch (armourSlot) //find slot
                {
                    //add slot to return string
                    case ArmourSlot.Trinket:
                        returnString += "Trinket" + "\n";
                        break;
                    case ArmourSlot.Back:
                        returnString += "Back" + "\n";
                        break;
                    case ArmourSlot.Chest:
                        returnString += "Chest" + "\n";
                        break;
                    case ArmourSlot.Feet:
                        returnString += "Feet" + "\n";
                        break;
                    case ArmourSlot.Hands:
                        returnString += "Hands" + "\n";
                        break;
                    case ArmourSlot.Head:
                        returnString += "Head" + "\n";
                        break;
                    case ArmourSlot.Legs:
                        returnString += "Legs" + "\n";
                        break;
                    case ArmourSlot.Neck:
                        returnString += "Neck" + "\n";
                        break;
                    case ArmourSlot.Finger:
                        returnString += "Ring" + "\n";
                        break;
                    case ArmourSlot.Shoulder:
                        returnString += "Shoulder" + "\n";
                        break;
                    case ArmourSlot.Waist:
                        returnString += "Waist" + "\n";
                        break;
                    case ArmourSlot.Wrist:
                        returnString += "Wrist" + "\n";
                        break;
                }

                if (armourType != ArmourType.Null) //it has an armour type
                {
                    switch (armourType)
                    {
                        case ArmourType.Cloth:
                            returnString += "Cloth\n";
                            break;
                        case ArmourType.Leather:
                            returnString += "Leather\n";
                            break;
                        case ArmourType.Mail:
                            returnString += "Mail\n";
                            break;
                        case ArmourType.Plate:
                            returnString += "Cloth\n";
                            break;
                    }
                }
            }
            else //it's a weapon
            {
            }

            //base stats in order str, agi, sta, int, spi
            if (strength != 0)
            {
                returnString += "Strength +" + strength + "\n";
            }
            if (agility != 0)
            {
                returnString += "Agility +" + agility + "\n";
            }
            if (stamina != 0)
            {
                returnString += "Stamina +" + stamina + "\n";
            }
            if (intellect != 0)
            {
                returnString += "Intellect +" + intellect + "\n";
            }
            if (spirit != 0)
            {
                returnString += "Spirit +" + spirit + "\n";
            }

            if (hp5 != 0)
            {
                returnString += "Equip: Restores " + hp5 + " health every 5 sec." + "\n";
            }
            if (mp5 != 0)
            {
                returnString += "Equip: Restores " + mp5 + " mana every 5 sec." + "\n";
            }

            returnString += "Requires level " + requiredLevel + "\n";

            if (use != null)
            {
                returnString += "Use: " + use + "\n";
            }

            return returnString;
        }

    }
}
