﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using System.Diagnostics;
using System.Windows.Media;

namespace Classic_WoW_Database
{
    public class WindowViewModel : INotifyPropertyChanged
    {
        private readonly IBoundary boundary = new Boundary();       

        private string searchText;
        public string SearchText
        {
            get { return searchText; }
            set
            {
                searchText = value;
                OnPropertyChanged("SearchText");
            }
        }

        private ObservableCollection<Item> searchResults = new ObservableCollection<Item>();
        public ObservableCollection<Item> SearchResults
        {
            get { return searchResults; }
            set
            {
                searchResults = value;
                OnPropertyChanged("SearchResults");
            }
        }

        private Item selectedItemSearchResults;
        public Item SelectedItemSearchResults
        {
            get { return selectedItemSearchResults; }
            set
            {
                selectedItemSearchResults = value;
                OnPropertyChanged("SelectedItemSearchResults");

                SelectedItemAttributes = selectedItemSearchResults.ListAttributes();

                switch(selectedItemSearchResults.quality)
                {
                    case Item.Quality.Poor:
                        NameColour = new SolidColorBrush(Colors.Gray);
                        break;
                    case Item.Quality.Common:
                        NameColour = new SolidColorBrush(Colors.White);
                        break;
                    case Item.Quality.Uncommon:
                        NameColour = new SolidColorBrush(Color.FromRgb(30, 255, 0));
                        break;
                    case Item.Quality.Rare:
                        NameColour = new SolidColorBrush(Colors.Blue);
                        break;
                    case Item.Quality.Epic:
                        NameColour = new SolidColorBrush(Color.FromRgb(163, 53, 238));
                        break;
                    case Item.Quality.Legendary:
                        NameColour = new SolidColorBrush(Colors.Orange);
                        break;
                }

                ItemName = selectedItemSearchResults.name;
                ItemWhiteText = selectedItemSearchResults.AddWhiteText();
                ItemGreenText = selectedItemSearchResults.AddGreenText();
                ItemYellowText = selectedItemSearchResults.AddYellowText();
            }
        }

        private int selectedIndexSearchResults;
        public int SelectedIndexSearchResults
        {
            get { return selectedIndexSearchResults; }
            set
            {
                selectedIndexSearchResults = value;
                OnPropertyChanged("SelectedIndexSearchResults");
            }
        }

        private ObservableCollection<string> selectedItemAttributes = new ObservableCollection<string>();
        public ObservableCollection<string> SelectedItemAttributes
        {
            get { return selectedItemAttributes; }
            set
            {
                selectedItemAttributes = value;
                OnPropertyChanged("SelectedItemAttributes");
            }
        }

        private System.Windows.Media.SolidColorBrush nameColour = new System.Windows.Media.SolidColorBrush(Colors.Red);
        public System.Windows.Media.SolidColorBrush NameColour
        {
            get { return nameColour; }
            set
            {
                nameColour = value;
                OnPropertyChanged("NameColour");
            }
        }

        private string itemName = "";
        public string ItemName
        {
            get { return itemName; }
            set
            {
                itemName = value;
                OnPropertyChanged("ItemName");
            }
        }

        private ObservableCollection<string> itemWhiteText = new ObservableCollection<string>();
        public ObservableCollection<string> ItemWhiteText
        {
            get { return itemWhiteText; }
            set
            {
                itemWhiteText = value;
                OnPropertyChanged("ItemWhiteText");
            }
        }

        private ObservableCollection<string> itemGreenText = new ObservableCollection<string>();
        public ObservableCollection<string> ItemGreenText
        {
            get { return itemGreenText; }
            set
            {
                itemGreenText = value;
                OnPropertyChanged("ItemGreenText");
            }
        }

        private ObservableCollection<string> itemYellowText = new ObservableCollection<string>();
        public ObservableCollection<string> ItemYellowText
        {
            get { return itemYellowText; }
            set
            {
                itemYellowText = value;
                OnPropertyChanged("ItemYellowText");
            }
        }

        private ObservableCollection<FilterUI> filters = new ObservableCollection<FilterUI>();
        public ObservableCollection<FilterUI> Filters
        {
            get { return filters; }
            set
            {
                filters = value;
                OnPropertyChanged("Filters");
            }
        }

        private bool? weaponSelected = null;
        private FilterUI topFilter = new FilterUI();
        public FilterUI TopFilter
        {
            get { return topFilter; }
            set { topFilter = value; }
        }

        private int oldNumberOfFilters;
        private int numberOfFilters;
        public int NumberOfFilters
        {
            get { return numberOfFilters; }
            set
            {
                if (value >= 0 && value < 10)
                {
                    oldNumberOfFilters = numberOfFilters;
                    numberOfFilters = value;
                    int difference = numberOfFilters - oldNumberOfFilters;
                    if (difference > 0)
                    {
                        //add
                        for (int i = 0; i < difference; i++)
                        {
                            Filters.Add(new FilterUI());
                        }
                    }
                    else
                    {
                        //subtract
                        for (int i = 0; i > difference; i--)
                        {
                            Filters.Remove(Filters[Filters.Count - 1]);
                        }
                    }
                    OnPropertyChanged("NumberOfFilters");
                }
            }
        }

        private ObservableCollection<StatFilterUI> statFilters = new ObservableCollection<StatFilterUI>();
        public ObservableCollection<StatFilterUI> StatFilters
        {
            get { return statFilters; }
            set
            {
                statFilters = value;
                OnPropertyChanged("StatFilters");
            }
        }

        private ObservableCollection<string> qualityList = new ObservableCollection<string> { "Any", "Legendary (Orange)", "Epic (Purple)", "Rare (Blue)", "Uncommon (Green)", "Common (White)", "Poor (Gray)" };
        public ObservableCollection<string> QualityList
        {
            get { return qualityList; }
            set { qualityList = value; }
        }
        public string SelectedQuality { get; set; }

        private int minLevel;
        public int MinLevel
        {
            get { return minLevel; }
            set
            {
                if (value >= 0 && value <= 60)
                {
                    minLevel = value;
                    OnPropertyChanged("MinLevel");
                }
            }
        }
        private int maxLevel;
        public int MaxLevel
        {
            get { return maxLevel; }
            set
            {
                if (value >= 0 && value <= 60)
                {
                    maxLevel = value;
                    OnPropertyChanged("MaxValue");
                }
            }
        }

        public WindowViewModel()
        {
            StatFilterUI sf = new StatFilterUI();
            sf.OnStatSelected += sf_OnStatSelected;
            StatFilters.Add(sf);

            TopFilter.OnSelectedItemType += new FilterUI.SelectedItemTypeChanged(OnStatSelected);

            SelectedQuality = "Any";
        }

        void sf_OnStatSelected(object sender, SelectedEventArgs e)
        {
            if (e.add)
            {
                StatFilterUI sf = new StatFilterUI();
                sf.SetWeaponSelected = weaponSelected ?? false;
                sf.OnStatSelected += new StatSelected(sf_OnStatSelected);
                StatFilters.Add(sf);
            }
            else
            {
                StatFilters.Remove(StatFilters[StatFilters.Count - 1]); //remove the last one
            }
        }

        void OnStatSelected(object sender, FilterUI.SelectedItemTypeArgs e)
        {
            weaponSelected = e.weaponSelected;

            foreach (FilterUI filter in Filters)
            {
                if (e.weaponSelected == null)
                    filter.SelectedItemList = "Select";
                else if (!(bool)e.weaponSelected)
                    filter.SelectedItemList = "Armour";
                else
                    filter.SelectedItemList = "Weapons";
            }

            //var v = StatFilters.ToList().RemoveAll(x => x.StatList.Contains("DPS"));
            StatFilters.RemoveAll(x => x.SelectedStat == "DPS");

            foreach (StatFilterUI statFilter in StatFilters)
            {
                if (e.weaponSelected == null || e.weaponSelected == false)
                    statFilter.SetWeaponSelected = false;
                else
                    statFilter.SetWeaponSelected = true;
            }

            if (e.weaponSelected == null || e.weaponSelected == false)
            {
                OrderBy.Remove("DPS");
                if (SelectedOrderBy == null)//DPs was selected, but now we're removing it
                    SelectedOrderBy = "Select";
            }
            else
                OrderBy.Insert(1, "DPS");
        }
        //MaxStatCommand

        private ObservableCollection<string> orderBy = new ObservableCollection<string> { "Select", "Agility", "Intellect", "Stamina", "Spirit", "Strength", "Mana per 5", "Health per 5", "Armour", "Dodge", "Parry", "Defence", "Block Chance", "Block Value", "Increased Axes", "Increased Daggers", "Increased Swords", "Hit", "Critical", "Attack Power", "Ranged Attack Power", "Feral Attack Power", "Spell Hit", "Spell Critical", "Spell Damage", "Spell Healing", "Spell Hit", "Resistance Decrease", "Arcane Damage", "Fire Damage", "Frost Damage", "Nature Damage", "Shadow Damage", "Arcane Resistance", "Fire Resistance", "Frost Resistance", "Nature Resistance", "Shadow Resistance" };
        public ObservableCollection<string> OrderBy
        {
            get { return orderBy; }
            set
            {
                orderBy = value;
                OnPropertyChanged("OrderBy");
            }
        }

        private string selectedOrderBy = "Select";
        public string SelectedOrderBy
        {
            get { return selectedOrderBy; }
            set
            {
                selectedOrderBy = value;
                OnPropertyChanged("SelectedOrderBy");
            }
        }

        private ICommand _maxStatCommand;
        public ICommand MaxStatCommand
        {
            get { return _maxStatCommand ?? (_maxStatCommand = new RelayCommand(param => MaxStat())); }
        }

        private void MaxStat()
        {
            Console.WriteLine("Max button hit");
            if (statFilters[0].SelectedStat != "Select")
            {
                Console.WriteLine(statFilters.Count.ToString());
                Console.WriteLine(statFilters[0].SelectedStat);

                //put filterUIs into a new list so that the top filter can be combined with the other filters
                List<FilterUI> allFilters = new List<FilterUI>();
                allFilters.Add(topFilter);
                foreach (FilterUI filter in filters)
                {
                    allFilters.Add(filter);
                }

                ObservableCollection<Item> items = boundary.MaxStat(allFilters, statFilters[0]);

                if (items.Count == 0)
                {
                    SearchResults = new ObservableCollection<Item> { new Item() };
                }
                else
                {
                    SearchResults = items;
                }
            }
        }

        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        param => this.SaveObject(),
                        param => this.CanSave()
                    );
                }
                return _saveCommand;
            }
        }

        private bool CanSave()
        {
            // Verify command can be executed here
            return true;
        }

        private void SaveObject() //called on search button hit
        {
            //boundary.Debug();
            //put filterUIs into a new list so that the top filter can be combined with the other filters
            List<FilterUI> allFilters = new List<FilterUI>();
            List<StatFilterUI> allStatFilters = new List<StatFilterUI>();
            allFilters.Add(topFilter);
            foreach (FilterUI filter in filters)
            {
                allFilters.Add(filter);
            }

            foreach (StatFilterUI iStatFilterUI in statFilters)
            {
                allStatFilters.Add(iStatFilterUI);
            }

            ObservableCollection<Item> items = boundary.SearchByFilter(SearchText, SelectedQuality, minLevel, maxLevel, allFilters, allStatFilters, selectedOrderBy);

            if (items.Count == 0)
            {
                SearchResults = new ObservableCollection<Item> { new Item() };
            }
            else
            {
                SearchResults = items;   
            }

            // Save command execution logic
            foreach (var item in SearchResults)
            {
                Console.WriteLine(item.itemID + " " + item.name);
            }
        }

        public delegate void TextBox_LostFocus();

        public event TextBox_LostFocus lostFocus;
        private void TextBox_LostFocus1()
        {
            Console.WriteLine("Focus Lost");
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(
                    this, new PropertyChangedEventArgs(propName));
        }
    }

    public class LevelValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            string input = value as string;

            if (input.Length > 2)
            {
                return new ValidationResult(false, "Level must be two digits in length");
            }

            if (input != "")
            {
                int number;
                if (!int.TryParse(input, out number))
                {
                    return new ValidationResult(false, "Level must be a number");
                }

                if (number < 0)
                {
                    return new ValidationResult(false, "Level must be positive");
                }

                if (number > 60)
                {
                    return new ValidationResult(false, "Level must be below 60");
                } 
            }

            return new ValidationResult(true, null);
        }
    }

    public static class ExtensionMethods
    {
        public static int RemoveAll<T>(
            this ObservableCollection<T> coll, Func<T, bool> condition)
        {
            var itemsToRemove = coll.Where(condition).ToList();

            foreach (var itemToRemove in itemsToRemove)
            {
                coll.Remove(itemToRemove);
            }

            return itemsToRemove.Count;
        }
    }

    /// <summary>
    /// A command whose sole purpose is to 
    /// relay its functionality to other
    /// objects by invoking delegates. The
    /// default return value for the CanExecute
    /// method is 'true'.
    /// </summary>
    public class RelayCommand : ICommand
    {
        #region Fields

        readonly Action<object> _execute;
        readonly Predicate<object> _canExecute;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        public RelayCommand(Action<object> execute)
            : this(execute, null)
        {
        }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            _execute = execute;
            _canExecute = canExecute;
        }

        #endregion // Constructors

        #region ICommand Members

        [DebuggerStepThrough]
        public bool CanExecute(object parameters)
        {
            return _canExecute == null || _canExecute(parameters);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameters)
        {
            _execute(parameters);
        }

        #endregion // ICommand Members
    }
}
